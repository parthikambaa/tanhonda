package customs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.TextView;
import com.tanhoda.R;


@SuppressLint("AppCompatCustomView")
public class CustomCheckBox extends CheckBox {

    private static final int DEFAULT_TYPE_FACE = 0;

    private boolean justify;
    private float textAreaWidth;
    private float spaceCharSize;
    private float lineY;
    private boolean text;

    private float strokeWidth;
    private Integer strokeColor;
    private Paint.Join strokeJoin;
    private float strokeMiter;

    public CustomCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public CustomCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        /*if (isInEditMode()) {
            return;
        }*/
        Typeface typeface;

        if (attrs == null) {
            typeface = getMatchingTypeface(DEFAULT_TYPE_FACE);
        } else {
            //get the attributes specified in attrs.xml using the name we included
            TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs,
                    R.styleable.CustomTypeface, 0, 0);

            try {
                int type = typedArray.getInt(R.styleable.CustomTypeface_typeface, DEFAULT_TYPE_FACE);
                typeface = getMatchingTypeface(type);
            } finally {
                typedArray.recycle();
            }
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomTypeface);
            if (a.hasValue(R.styleable.CustomTypeface_strokeColor)) {
                float strokeWidth = a.getDimensionPixelSize(R.styleable.CustomTypeface_strokeWidth, 1);
                int strokeColor = a.getColor(R.styleable.CustomTypeface_strokeColor, 0xff000000);
                float strokeMiter = a.getDimensionPixelSize(R.styleable.CustomTypeface_strokeMiter, 10);
                Paint.Join strokeJoin = null;
                switch (a.getInt(R.styleable.CustomTypeface_strokeJoinStyle, 0)) {
                    case (0):
                        strokeJoin = Paint.Join.MITER;
                        break;
                    case (1):
                        strokeJoin = Paint.Join.BEVEL;
                        break;
                    case (2):
                        strokeJoin = Paint.Join.ROUND;
                        break;
                }
                this.setStroke(strokeWidth, strokeColor, strokeJoin, strokeMiter);
            }
        }
        setTypeface(typeface);
    }

    private Typeface getMatchingTypeface(int type) {
        Context context = getContext();
        switch (type) {
            case 0:
            default:
                return CustomFonts.getInstance().getRegularTypeFace(context);
            case 1:
                return CustomFonts.getInstance().getBoldTypeface(context);
            case 2:
                return CustomFonts.getInstance().getLightTypeFace(context);
            case 3:
                return CustomFonts.getInstance().getMediumTypeFace(context);
        }
    }

    public void setText(boolean text) {
        this.text = text;
    }

    public void setStroke(float width, int color, Paint.Join join, float miter) {
        strokeWidth = width;
        strokeColor = color;
        strokeJoin = join;
        strokeMiter = miter;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int restoreColor = this.getCurrentTextColor();
        if (strokeColor != null) {
            TextPaint paint = this.getPaint();
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(strokeJoin);
            paint.setStrokeMiter(strokeMiter);
            this.setTextColor(strokeColor);
            paint.setStrokeWidth(strokeWidth);
            super.onDraw(canvas);
            paint.setStyle(Paint.Style.FILL);
            this.setTextColor(restoreColor);
        }
    }

/*
    @Override
    protected void onDraw(Canvas canvas) {
        drawText(canvas);
    }

    @SuppressLint("NewApi")
    private void drawText(Canvas canvas) {
        TextPaint paint = getPaint();
        paint.setColor(getCurrentTextColor());
        paint.drawableState = getDrawableState();
        textAreaWidth = getMeasuredWidth() - (getPaddingLeft() + getPaddingRight());

        spaceCharSize = paint.measureText(" ");

        String text = getText().toString();
        lineY = getTextSize();

        Layout textLayout = getLayout();

        if (textLayout == null)
            return;

        Paint.FontMetrics fm = paint.getFontMetrics();
        int textHeight = (int) Math.ceil(fm.descent - fm.ascent);
        textHeight = (int) (textHeight * getLineSpacingMultiplier() + textLayout.getSpacingAdd());
        try {
            for (int i = 0; i < textLayout.getLineCount(); i++) {

                int lineStart = textLayout.getLineStart(i);
                int lineEnd = textLayout.getLineEnd(i);

                float lineWidth = StaticLayout.getDesiredWidth(text, lineStart, lineEnd, paint);
                String line = text.substring(lineStart, lineEnd);
                if (line.length() > 0) {
                    if (line.charAt(line.length() - 1) == ' ') {
                        line = line.substring(0, line.length() - 1);
                    }
                }
                if (justify && i < textLayout.getLineCount() - 1) {
                    drawLineJustified(canvas, line, lineWidth);
                } else {
                    canvas.drawText(line, 0, lineY, paint);
                }

                lineY += textHeight;
            }
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    private void drawLineJustified(Canvas canvas, String line, float lineWidth) {
        TextPaint paint = getPaint();

        float emptySpace = textAreaWidth - lineWidth;
        int spaces = line.split(" ").length - 1;
        float newSpaceSize = (emptySpace / spaces) + spaceCharSize;

        float charX = 0;

        for (int i = 0; i < line.length(); i++) {
            String character = String.valueOf(line.charAt(i));
            float charWidth = StaticLayout.getDesiredWidth(character, paint);
            if (!character.equals(" ")) {
                canvas.drawText(character, charX, lineY, paint);
            }

            if (character.equals(" ") && i != line.length() - 1)
                charX += newSpaceSize;
            else
                charX += charWidth;
        }

    }*/
}