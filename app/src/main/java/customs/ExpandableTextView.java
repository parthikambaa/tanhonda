package customs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.tanhoda.R;


/**
 * Created by Android on 01-06-2018.
 */

@SuppressLint("AppCompatCustomView")
public class ExpandableTextView extends TextView implements View.OnClickListener {
    private static final int MAX_LINES = 10000000;
    private int currentMaxLines = Integer.MAX_VALUE;
    private static final int DEFAULT_TYPE_FACE = 0;


    private void init(Context context, AttributeSet attrs) {
        /*if (isInEditMode()) {
            return;
        }*/
        Typeface typeface;

        if (attrs == null) {
            typeface = getMatchingTypeface(DEFAULT_TYPE_FACE);
        } else {
            //get the attributes specified in attrs.xml using the name we included
            TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs,
                    R.styleable.CustomTypeface, 0, 0);
            try {
                int type = typedArray.getInt(R.styleable.CustomTypeface_typeface, DEFAULT_TYPE_FACE);
                typeface = getMatchingTypeface(type);
            } finally {
                typedArray.recycle();
            }
        }
        setTypeface(typeface);
    }

    private Typeface getMatchingTypeface(int type) {
        Context context = getContext();
        switch (type) {
            case 0:
            default:
                return CustomFonts.getInstance().getRegularTypeFace(context);
            case 1:
                return CustomFonts.getInstance().getBoldTypeface(context);
            case 2:
                return CustomFonts.getInstance().getLightTypeFace(context);
            case 3:
                return CustomFonts.getInstance().getMediumTypeFace(context);
        }
    }

    public ExpandableTextView(Context context) {
        super(context);
        setOnClickListener(this);

    }

    public ExpandableTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setOnClickListener(this);
        init(context, attrs);
    }

    public ExpandableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnClickListener(this);
        init(context, attrs);
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        /* If text longer than MAX_LINES set DrawableBottom - I'm using '...' icon */
        post(new Runnable() {
            public void run() {
                if (getLineCount() > MAX_LINES)
                    setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.ic_more);
                else {
                    setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }


                setMaxLines(MAX_LINES);
            }
        });
    }


    @Override
    public void setMaxLines(int maxLines) {
        currentMaxLines = maxLines;
        super.setMaxLines(maxLines);
    }

    /* Custom method because standard getMaxLines() requires API > 16 */
    public int getMyMaxLines() {
        return currentMaxLines;
    }

    int i = 0;

    @Override
    public void onClick(View v) {
        /* Toggle between expanded collapsed states */
        if (getMyMaxLines() == Integer.MAX_VALUE)
            setMaxLines(MAX_LINES);
        else
            setMaxLines(Integer.MAX_VALUE);
        if (i == 0) {
            i = 1;
        } else {
            i = 0;
            /*try {
                if (productsAdapter != null) {
                    productsAdapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }*/

        }

    }

}
