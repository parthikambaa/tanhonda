package customs;


public interface Fonts {
    String REGULAR = "Cairo-Regular.ttf";
    String MEDIUM = "Cairo-Regular.ttf";
    String BOLD = "Cairo-Bold.ttf";
    String LIGHT = "Cairo-Light.ttf";
}
