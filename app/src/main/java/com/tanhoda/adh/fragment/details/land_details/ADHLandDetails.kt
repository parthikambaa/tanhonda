package com.tanhoda.adh.fragment.details.land_details


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tanhoda.R
import com.tanhoda.adh.fragment.ApplicationList.ADHApplicationList.Companion.adhapplistRESADH
import com.tanhoda.adh.fragment.ApplicationList.ADHApplicationList.Companion.adhcurrentposition
import com.tanhoda.adh.fragment.details.AdhAppDetailsHome


import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import kotlinx.android.synthetic.main.fragment_adhland_details.*


class ADHLandDetails : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_adhland_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



    }

    override fun onResume() {
        super.onResume()
        try{
        settoForm()
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun settoForm() {
        try{
            val data = AdhAppDetailsHome.adhappdetRES?.data?.applications?.appland !!
            val appitem = AdhAppDetailsHome.adhappdetRES?.data?.applications!!

            if(data.serveyNo!=null){
                custxtviw_adh_land_survey_number?.setText(data.serveyNo)
            }
            if(data.sourceOfIrrigation!=null){
                custxtviw_adh_land_survey_irragtion?.setText(data.sourceOfIrrigation)
            }
            if(data.totalArea!=null){
                custxtviw_adh_land_total_area?.setText(data.totalArea)
            }
            if(appitem.village!=null){
                custxtviw_adh_land_village?.setText(appitem.villagename?.village)
            }
            if(appitem.appblock!=null){
                custxtviw_adh_land_block?.setText(appitem.appblock.block)
            }
            if(appitem.appdistrict!=null){
                custxtviw_adh_land_district?.setText(appitem.appdistrict.district)
            }
            if(appitem.appstate!=null){
                custxtviw_adh_land_state?.setText(appitem.appstate.state)
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
}
