package com.tanhoda.adh.fragment.Dashboard.adapter

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.tanhoda.R
import com.tanhoda.adh.fragment.Dashboard.pojo.ADHappStarusPojo

import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.utils.ImageUtils
import com.tanhoda.utils.Utils

class ADHPridashboardAdapter(val context: Context, val statusArrayList: ArrayList<ADHappStarusPojo>, val onclickPostionEvent: OnclickPostionEvent)
    : RecyclerView.Adapter<ADHPridashboardAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ADHPridashboardAdapter.ViewHolder {
        return   ViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_adh_pri_dashboard,parent,false))
    }

    override fun getItemCount(): Int { return  statusArrayList.size   }

    override fun onBindViewHolder(holder: ADHPridashboardAdapter.ViewHolder, position: Int) {
        try{
//            holder.image?.setImageResource(statusArrayList[position].image)
            ImageUtils.displayImageFromUrl(context,Utils.IMG_URL+statusArrayList[position].image,holder.image)
            holder.statusName?.text = statusArrayList[position].statusName
            holder.statusno?.text = statusArrayList[position].status_no
            holder.master?.setOnClickListener {
                try{
                    onclickPostionEvent.OnclickPostionEvent(statusArrayList[position].statusName,position.toString())
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val  image =itemView?.findViewById<ImageView>(R.id.imgv_app_pri_dashboard_icon)
        val  statusName =itemView?.findViewById<customs.CustomTextView>(R.id.txt_app_pri_dashboard_name)
        val  statusno =itemView?.findViewById<customs.CustomTextView>(R.id.txt_app_pri_dashboard_no)
        val  master =itemView?.findViewById<ConstraintLayout>(R.id.adh_pri_dashboard_master_layout)
    }

}