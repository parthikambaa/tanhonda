package com.tanhoda.adh.fragment.schemes.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.tanhoda.R
import com.tanhoda.adh.fragment.schemes.adhschemespojo.SchemesItem
import com.tanhoda.interfaces.OnclickPostionEvent
import customs.CustomTextView

class ADHschemesAdapter(
    val context: Context,
    val schemeArrayList: ArrayList<SchemesItem?>?,
    val onclickPostionEvent: OnclickPostionEvent
)
    : RecyclerView.Adapter<ADHschemesAdapter.Viewholder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ADHschemesAdapter.Viewholder {
        return Viewholder(LayoutInflater.from(context).inflate(R.layout.adapter_adh_scheme_list,parent,false))
    }

    override fun getItemCount(): Int { return  schemeArrayList?.size!!   }

    override fun onBindViewHolder(holder: ADHschemesAdapter.Viewholder, position: Int) {
        try {
            holder.siNo.text = (position+1).toString()
            holder.shortname.text = schemeArrayList!![position]?.shortName
            holder.scheme_name.text =schemeArrayList[position]?.schemeName
            holder.linear.setOnClickListener {
                try {
                    onclickPostionEvent.OnclickPostionEvent("ADHscheme", position.toString())
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    class Viewholder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val siNo = itemView?.findViewById<CustomTextView>(R.id.adh_scheme_si_no_adp)!!
        val shortname = itemView?.findViewById<CustomTextView>(R.id.adh_scheme_short_name_adp)!!
        val scheme_name = itemView?.findViewById<CustomTextView>(R.id.adh_scheme_scheme_name_adp)!!
        val linear = itemView?.findViewById<LinearLayout>(R.id.adh_scheme_list_title_linear_adp)!!

    }
}