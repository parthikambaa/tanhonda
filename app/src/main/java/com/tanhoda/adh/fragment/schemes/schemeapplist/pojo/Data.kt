package com.tanhoda.adh.fragment.schemes.schemeapplist.pojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("applications")
	val applications: ArrayList<ApplicationsItem?>? = null
)