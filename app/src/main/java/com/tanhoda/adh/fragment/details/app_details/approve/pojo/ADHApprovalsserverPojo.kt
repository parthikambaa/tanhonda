package com.tanhoda.adh.fragment.details.app_details.approve.pojo

import com.google.gson.annotations.SerializedName

class ADHApprovalsserverPojo(
    @field:SerializedName("approval_value")
    var approval_value: String? = null,
    @field: SerializedName("approval_name")
    var approval_name: String? = null
)