package com.tanhoda.adh.fragment.ApplicationList.pojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Appblock(

	@field:SerializedName("block_tamil")
	val blockTamil: Any? = null,

	@field:SerializedName("office_address")
	val officeAddress: Any? = null,

	@field:SerializedName("updated_at")
	val updatedAt: Any? = null,

	@field:SerializedName("block_code")
	val blockCode: Any? = null,

	@field:SerializedName("active")
	val active: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: Any? = null,

	@field:SerializedName("block")
	val block: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("state_id")
	val stateId: Int? = null,

	@field:SerializedName("district_id")
	val districtId: Int? = null
)