package com.tanhoda.adh.fragment.schemes.schemeapplist.viewshemestatus.farmer_details


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tanhoda.R


class ADHSchemeFarmerDet : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_adhscheme_farmer_det, container, false)
    }


}
