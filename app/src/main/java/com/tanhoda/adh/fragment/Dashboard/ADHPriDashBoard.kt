package com.tanhoda.adh.fragment.Dashboard


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import com.google.gson.Gson

import com.tanhoda.R
import com.tanhoda.adh.ADHActivity
import com.tanhoda.adh.fragment.Dashboard.adapter.ADHPridashboardAdapter
import com.tanhoda.adh.fragment.Dashboard.pojo.ADHappStarusPojo
import com.tanhoda.adh.fragment.Dashboard.pojo.adhpridashboardpojo.ADHPridashRES
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.login_splash_screen.SplashActivity
import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_adhpri_dash_board.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ADHPriDashBoard : Fragment(),OnclickPostionEvent {

    var dialog: Dialog? = null
    var priDashboradArrayList=ArrayList<ADHappStarusPojo>()
    companion object {
        var currentclickpostionaname=""
        var dashboradRES: ADHPridashRES?=null
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as ADHActivity).enableToogle(getString(R.string.dashboard))
        return inflater.inflate(R.layout.fragment_adhpri_dash_board, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when{
            Utils.haveNetworkConnection(activity)->{
                getDashboardRes()
            }
            else ->{
                adh_pri_dashboard_empty_List?.visibility =View.VISIBLE
                adh_pri_dashboard_empty_List?.text =getString(R.string.check_internet)
                MessageUtils.showSnackBar(activity,adh_pridash_snackView,getString(R.string.check_internet))
            }
        }
    }
    private fun getDashboardRes() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["user_id"] = SessionManager.getUserId(activity)
            map["block"] = SessionManager.getBlockId(activity)
            map["role"] = SessionManager.getuserSopeID(activity)
            map["district"] = SessionManager.getdistrictID(activity)
            val calinterface = Utils.getInstance(activity)
            val callback = calinterface.call_post("officerprofile", "Bearer " + SessionManager.getToken(activity!!), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    MessageUtils.showSnackBar(activity, adh_pridash_snackView, msg)
                    adh_pri_dashboard_empty_List?.visibility =View.VISIBLE
                    adh_pri_dashboard_empty_List?.text =msg
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                           dashboradRES = Gson().fromJson(response.body()?.string(), ADHPridashRES::class.java)
                            if(dashboradRES !=null){
                                if (dashboradRES?.success!!){
                                    if(dashboradRES?.data!=null){
                                        priDashboradArrayList.clear()
                                        setApplicationStatus()
                                        adh_pri_dashboard_empty_List?.visibility =View.GONE
                                    }else{
                                        adh_pri_dashboard_empty_List?.visibility =View.VISIBLE
                                        adh_pri_dashboard_empty_List?.text =dashboradRES?.message
                                        MessageUtils.showSnackBar(activity,adh_pridash_snackView,
                                           dashboradRES?.message)
                                    }

                                }else{
                                    MessageUtils.showSnackBar(activity,adh_pridash_snackView,
                                       dashboradRES?.message)
                                }

                            }else{
                                MessageUtils.showSnackBar(activity,adh_pridash_snackView,getString(R.string.checkjsonformat))
                            }
                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            MessageUtils.showSnackBar(activity, adh_pridash_snackView, msg)
                            adh_pri_dashboard_empty_List?.visibility =View.VISIBLE
                            adh_pri_dashboard_empty_List?.text =msg
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setApplicationStatus() {
        try {
            val newapp = ADHappStarusPojo(dashboradRES?.data?.block?.img!!,dashboradRES?.data?.block?.status!!,dashboradRES?.data?.block?.blockcount.toString())
            priDashboradArrayList.add(newapp)
            val preinspection = ADHappStarusPojo(dashboradRES?.data?.district?.img!!,dashboradRES?.data?.district?.status!!,
               dashboradRES?.data?.district?.districtcount.toString())
            priDashboradArrayList.add(preinspection)
            val hoapprovel = ADHappStarusPojo(dashboradRES?.data?.head?.img!!,dashboradRES?.data?.head?.status!!,dashboradRES?.data?.head?.headcount.toString())
            priDashboradArrayList.add(hoapprovel)

            val affidavit = ADHappStarusPojo(dashboradRES?.data?.work?.img!!,dashboradRES?.data?.work?.status!!,dashboradRES?.data?.work?.workcount.toString())
            priDashboradArrayList.add(affidavit)

            val adhapproval = ADHappStarusPojo(dashboradRES?.data?.payment?.img!!,dashboradRES?.data?.payment?.status!!,dashboradRES?.data?.payment?.paymentcount.toString())
            priDashboradArrayList.add(adhapproval)


            recy_adh_pri_dashboard_list?.layoutManager = GridLayoutManager(activity, 2, GridLayout.VERTICAL, false)
            val statsusAdapter = ADHPridashboardAdapter(activity!!, priDashboradArrayList, this)
            recy_adh_pri_dashboard_list?.adapter = statsusAdapter
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {
        try {
           currentclickpostionaname =name
            FragmentCallUtils.passFragment(activity, ADHAppDashboard(), R.id.aho_container_body)
//            FragmentCallUtils.passFragment(activity, AHOAppDetails(), R.id.aho_container_body)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        MessageUtils.dismissSnackBar(SplashActivity.snackBar)
    }
}


