package com.tanhoda.adh.fragment.schemes.schemeapplist.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.tanhoda.R
import com.tanhoda.adh.fragment.schemes.schemeapplist.pojo.ApplicationsItem

import com.tanhoda.interfaces.OnclickPostionEvent
import customs.CustomTextView

class ADHscemeAppListADapter(
    val context: Context,
    val schemeArrayList: ArrayList<ApplicationsItem?>?,
    val onclickPostionEvent: OnclickPostionEvent
)
    : RecyclerView.Adapter<ADHscemeAppListADapter.Viewholder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ADHscemeAppListADapter.Viewholder {
        return Viewholder(LayoutInflater.from(context).inflate(R.layout.adapter_adh_schemeapp_list,parent,false))
    }

    override fun getItemCount(): Int { return  schemeArrayList?.size!!   }

    override fun onBindViewHolder(holder: ADHscemeAppListADapter.Viewholder, position: Int) {
        try {
            holder.siNo.text = (position+1).toString()
            holder.category.text = schemeArrayList!![position]?.appcategory?.category
            holder.component.text =schemeArrayList[position]?.appcomponent?.componentName
            holder.linear.setOnClickListener {
                try {
                    onclickPostionEvent.OnclickPostionEvent("ADHscheme", position.toString())
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    class Viewholder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val siNo = itemView?.findViewById<CustomTextView>(R.id.adh_schemeapp_si_no_adp)!!
        val category = itemView?.findViewById<CustomTextView>(R.id.adh_schemeapp_cat_name_adp)!!
        val component = itemView?.findViewById<CustomTextView>(R.id.adh_schemeapp_compnent_name_adp)!!
        val linear = itemView?.findViewById<LinearLayout>(R.id.adh_schemeapp_list_title_linear_adp)!!

    }
}