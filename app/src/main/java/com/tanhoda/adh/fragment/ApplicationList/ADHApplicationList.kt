package com.tanhoda.adh.fragment.ApplicationList


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.google.gson.Gson
import com.tanhoda.R
import com.tanhoda.adh.ADHActivity
import com.tanhoda.adh.fragment.ApplicationList.adapter.ADHapplicationListAdpter
import com.tanhoda.adh.fragment.ApplicationList.pojo.ADHApplicationlistRES
import com.tanhoda.adh.fragment.Dashboard.ADHAppDashboard
import com.tanhoda.adh.fragment.details.AdhAppDetailsHome
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.login_splash_screen.SplashActivity.Companion.snackBar
import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_adhapplication_list.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ADHApplicationList : Fragment(), OnclickPostionEvent {
    var adhApplicationadapter: ADHapplicationListAdpter? = null
    val TAG = "AHOAPPLICATIONLIST"
    var dialog: Dialog? = null
    var snackViewADH: RelativeLayout? = null

    companion object {
        var adhapplistRESADH: ADHApplicationlistRES? = null
        var adhcurrentposition: String? = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as ADHActivity).disableNavigationInAdoptionHome(getString(R.string.applicationlist))
        return inflater.inflate(R.layout.fragment_adhapplication_list, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)
        snackViewADH = view.findViewById(R.id.rel_lay_adh_app_list_snackview)


        /*
         * get AHO Application List*/

        if (Utils.haveNetworkConnection(activity)) {
            getApplicationList()
        } else {
            adh_app_empty_list?.visibility = View.VISIBLE
            adh_app_empty_list?.text = getString(R.string.check_internet)
            MessageUtils.showSnackBar(activity, snackViewADH, getString(R.string.check_internet))
        }


    }

    private fun getApplicationList() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["userid"] = SessionManager.getUserId(activity)
            map["role"] = SessionManager.getuserSopeID(activity)
            map["status"] = ADHAppDashboard.currentstatusname
            map["block"] = SessionManager.getBlockId(activity)
            map["district"] = SessionManager.getdistrictID(activity)

            val callback = Utils.getInstance(activity)
                .call_post("applicationlist", "Bearer " + SessionManager.getToken(activity), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    snackBar = MessageUtils.showSnackBar(activity, snackViewADH, msg)
                    adh_app_empty_list?.visibility = View.VISIBLE
                    adh_app_empty_list?.text = msg
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            adhapplistRESADH =
                                Gson().fromJson(response.body()?.string(), ADHApplicationlistRES::class.java)
                            if (adhapplistRESADH != null) {
                                if (adhapplistRESADH?.success!!) {
                                    if (adhapplistRESADH?.data != null) {
                                        if (adhapplistRESADH?.data?.applications != null && adhapplistRESADH?.data?.applications?.size != 0) {
                                            setToADapter()
                                            adh_app_empty_list?.visibility = View.GONE
                                            recy_adh_app_list?.visibility = View.VISIBLE
                                            adh_app_list_title_linear?.visibility = View.VISIBLE
                                        } else {
                                            adh_app_list_title_linear?.visibility = View.GONE
                                            recy_adh_app_list?.visibility = View.GONE
                                            adh_app_empty_list?.visibility = View.VISIBLE
                                            adh_app_empty_list?.text = adhapplistRESADH?.message
                                        }

                                    } else {
                                        MessageUtils.showSnackBar(
                                            activity,
                                            snackViewADH,
                                            adhapplistRESADH?.message
                                        )
                                    }
                                } else {
                                    MessageUtils.showSnackBar(
                                        activity,
                                        snackViewADH,
                                        adhapplistRESADH?.message
                                    )
                                }
                            } else {
                                MessageUtils.showSnackBar(
                                    activity,
                                    snackViewADH,
                                    getString(R.string.checkjsonformat)
                                )
                            }

                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            MessageUtils.showSnackBar(activity, snackViewADH, msg)
                            adh_app_empty_list?.visibility = View.VISIBLE
                            adh_app_empty_list?.text = msg
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }

                }

            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setToADapter() {
        try {

            recy_adh_app_list?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            adhApplicationadapter = ADHapplicationListAdpter(activity!!, adhapplistRESADH?.data?.applications, this)
            recy_adh_app_list?.adapter = adhApplicationadapter
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {
        try {
            adhcurrentposition = postion
            FragmentCallUtils.passFragment(activity, AdhAppDetailsHome(), R.id.aho_container_body)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        MessageUtils.dismissSnackBar(snackBar)
    }
}
