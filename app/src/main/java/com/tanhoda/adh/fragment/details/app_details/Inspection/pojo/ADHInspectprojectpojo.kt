package com.tanhoda.adh.fragment.details.app_details.Inspection.pojo

import com.google.gson.annotations.SerializedName

class ADHInspectprojectpojo(
    @field:SerializedName("particulars")
    var projectparticulars: String? = null,
    @field:SerializedName("projectspec")
    var projectspec: String? = null,
    @field:SerializedName("projectcapacity")
    var projectcapacity: String? = null,
    @field:SerializedName("projectqty")
    var projectqty: String? = null,
    @field:SerializedName("projectamount")
    var projectamount: String? = null,

    @field:SerializedName("inspectionspec")
    var inspectionspec: String? = null,
    @field:SerializedName("inspectioncapacity")
    var inspectioncapacity: String? = null,
    @field:SerializedName("inspectionqty")
    var inspectionqty: String? = null,
    @field:SerializedName("inspectionamount")
    var inspectionamount: String? = null,
    @field:SerializedName("inspectionremarks")
    var inspectionremarks: String? = null
)