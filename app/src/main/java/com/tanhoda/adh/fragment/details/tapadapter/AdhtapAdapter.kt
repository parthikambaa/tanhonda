package com.tanhoda.adh.fragment.details.tapadapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import java.util.ArrayList

class AdhtapAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {
    private val AhoDetailsList = ArrayList<Fragment>()
    private val InventoryTitileList = ArrayList<String>()


    fun addFragment(fragment: Fragment, title: String) {
        AhoDetailsList.add(fragment)
        InventoryTitileList.add(title)
    }

    override fun getItem(position: Int): android.support.v4.app.Fragment? {
        return AhoDetailsList[position]
    }


    override fun getCount(): Int {
        return AhoDetailsList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return InventoryTitileList[position]
    }

}