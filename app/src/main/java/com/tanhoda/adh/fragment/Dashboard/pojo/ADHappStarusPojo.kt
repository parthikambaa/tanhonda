package com.tanhoda.adh.fragment.Dashboard.pojo

import com.google.gson.annotations.SerializedName

class ADHappStarusPojo(
    @field:SerializedName("image")
    var image:String,
    @field:SerializedName("status_name")
    var statusName:String,
    @field:SerializedName("status_no")
    var status_no:String

)