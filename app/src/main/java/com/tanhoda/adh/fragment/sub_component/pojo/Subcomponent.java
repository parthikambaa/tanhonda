
package com.tanhoda.adh.fragment.sub_component.pojo;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Subcomponent {

    @SerializedName("active")
    private Long mActive;
    @SerializedName("available_in")
    private String mAvailableIn;
    @SerializedName("category_id")
    private Long mCategoryId;
    @SerializedName("component_name")
    private String mComponentName;
    @SerializedName("componentcategory")
    private Componentcategory mComponentcategory;
    @SerializedName("componentscheme")
    private Componentscheme mComponentscheme;
    @SerializedName("create_date")
    private String mCreateDate;
    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("id")
    private Long mId;
    @SerializedName("image")
    private String mImage;
    @SerializedName("max_limit")
    private String mMaxLimit;
    @SerializedName("min_limit")
    private String mMinLimit;
    @SerializedName("scheme_id")
    private Long mSchemeId;
    @SerializedName("short_description")
    private String mShortDescription;
    @SerializedName("stage")
    private String mStage;
    @SerializedName("subsidy_amount")
    private String mSubsidyAmount;
    @SerializedName("subsidy_percentage")
    private String mSubsidyPercentage;
    @SerializedName("type")
    private String mType;
    @SerializedName("unit")
    private String mUnit;
    @SerializedName("updated_at")
    private String mUpdatedAt;

    public Long getActive() {
        return mActive;
    }

    public void setActive(Long active) {
        mActive = active;
    }

    public String getAvailableIn() {
        return mAvailableIn;
    }

    public void setAvailableIn(String availableIn) {
        mAvailableIn = availableIn;
    }

    public Long getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(Long categoryId) {
        mCategoryId = categoryId;
    }

    public String getComponentName() {
        return mComponentName;
    }

    public void setComponentName(String componentName) {
        mComponentName = componentName;
    }

    public Componentcategory getComponentcategory() {
        return mComponentcategory;
    }

    public void setComponentcategory(Componentcategory componentcategory) {
        mComponentcategory = componentcategory;
    }

    public Componentscheme getComponentscheme() {
        return mComponentscheme;
    }

    public void setComponentscheme(Componentscheme componentscheme) {
        mComponentscheme = componentscheme;
    }

    public String getCreateDate() {
        return mCreateDate;
    }

    public void setCreateDate(String createDate) {
        mCreateDate = createDate;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getMaxLimit() {
        return mMaxLimit;
    }

    public void setMaxLimit(String maxLimit) {
        mMaxLimit = maxLimit;
    }

    public String getMinLimit() {
        return mMinLimit;
    }

    public void setMinLimit(String minLimit) {
        mMinLimit = minLimit;
    }

    public Long getSchemeId() {
        return mSchemeId;
    }

    public void setSchemeId(Long schemeId) {
        mSchemeId = schemeId;
    }

    public String getShortDescription() {
        return mShortDescription;
    }

    public void setShortDescription(String shortDescription) {
        mShortDescription = shortDescription;
    }

    public String getStage() {
        return mStage;
    }

    public void setStage(String stage) {
        mStage = stage;
    }

    public String getSubsidyAmount() {
        return mSubsidyAmount;
    }

    public void setSubsidyAmount(String subsidyAmount) {
        mSubsidyAmount = subsidyAmount;
    }

    public String getSubsidyPercentage() {
        return mSubsidyPercentage;
    }

    public void setSubsidyPercentage(String subsidyPercentage) {
        mSubsidyPercentage = subsidyPercentage;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getUnit() {
        return mUnit;
    }

    public void setUnit(String unit) {
        mUnit = unit;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

}
