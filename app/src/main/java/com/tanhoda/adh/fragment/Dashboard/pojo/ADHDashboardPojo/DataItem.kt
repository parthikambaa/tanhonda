package com.tanhoda.adh.fragment.Dashboard.pojo.ADHDashboardPojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class DataItem(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("stage_name")
	val stageName: String? = null,

	@field:SerializedName("count")
	val count: Int? = null
)