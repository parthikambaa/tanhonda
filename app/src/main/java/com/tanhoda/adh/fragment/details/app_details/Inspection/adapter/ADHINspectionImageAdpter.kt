package com.tanhoda.adh.fragment.details.app_details.Inspection.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.tanhoda.R
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.utils.ImageUtils
import customs.CustomTextView
import java.io.File

class ADHINspectionImageAdpter (
    val  activity: FragmentActivity,
    val fileArrayList:ArrayList<File>,
    val onclickPostionEvent: OnclickPostionEvent
): RecyclerView.Adapter<ADHINspectionImageAdpter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ADHINspectionImageAdpter.ViewHolder {
        return ViewHolder(LayoutInflater.from(activity).inflate(R.layout.adapter_adh_inspectionimage,parent,false))
    }

    override fun getItemCount(): Int {
        return  fileArrayList.size

    }

    override fun onBindViewHolder(holder: ADHINspectionImageAdpter.ViewHolder, position: Int) {
        try{

            holder.filename?.text = fileArrayList[position].name
            ImageUtils.setImage(holder.image,fileArrayList[position].absolutePath,activity)


        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        val filename = itemView?.findViewById<CustomTextView>(R.id.image_name_textview)
        val image = itemView?.findViewById<ImageView>(R.id.image_view)

    }
}