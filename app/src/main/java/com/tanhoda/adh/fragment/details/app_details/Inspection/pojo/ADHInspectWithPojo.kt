package com.tanhoda.adh.fragment.details.app_details.Inspection.pojo
import com.google.gson.annotations.SerializedName

class ADHInspectWithPojo (
    @field:SerializedName("name")
    var name:String?=null,
    @field:SerializedName("designation")
    var designation:String?=null,
    @field:SerializedName("remarks")
    var remarks:String?=null
)