package com.tanhoda.adh.fragment.details.app_details.approve


import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

import com.tanhoda.R
import com.tanhoda.adh.ADHActivity
import com.tanhoda.adh.fragment.Dashboard.ADHPriDashBoard
import com.tanhoda.adh.fragment.details.AdhAppDetailsHome
import com.tanhoda.adh.fragment.details.AdhAppDetailsHome.Companion.adhappdetRES
import com.tanhoda.adh.fragment.details.app_details.approve.adapter.ADHappDetViewDocumetnAdaper
import com.tanhoda.adh.fragment.details.app_details.approve.adapter.ADHapprovedynamicAdapter
import com.tanhoda.adh.fragment.details.app_details.approve.pojo.ADHAppDetDocumentServerPojo
import com.tanhoda.adh.fragment.details.app_details.approve.pojo.ADHApprovalsserverPojo
import com.tanhoda.adh.fragment.details.app_details.approve.pojo.ADHdynamicofficer
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.utils.*
import com.tanhoda.utils.Utils.log
import kotlinx.android.synthetic.main.dialog_dismiss_btn.*
import kotlinx.android.synthetic.main.dialog_permissions.*
import kotlinx.android.synthetic.main.dialog_pic_img.*
import kotlinx.android.synthetic.main.fragment_adh_app_approve.*
import kotlinx.android.synthetic.main.item_dynamic.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class AdhAppApprove : Fragment() , View.OnClickListener, OnclickPostionEvent {


    var GALLERY: Int = 0
    var CAMERA: Int = 1
    var IMAGE_DIRECTORY: String = "/TanHoda/Land"
    var path: String = ""
    var TAG = "AHO_FRAGMENT"
    var extraofficerArrayList = ArrayList<ADHdynamicofficer>()
    var extraofficeradapter: ADHapprovedynamicAdapter? = null
    var dialog: Dialog? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as ADHActivity).disableNavigationInAdoptionHome(getString(R.string.approve))
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_adh_app_approve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog =Dialog(activity)
        adh_app_approve_cancel.setOnClickListener(this)
        adh_app_approve_submit.setOnClickListener(this)
        adh_app_img_select.setOnClickListener(this)





        ins_add_more?.setOnClickListener {
            try {
                val officername = edt_comer_name?.text.toString()
                val department = edt_dept_name?.text.toString()
                val designation = edt_comer_designation?.text.toString()
                val remarks = edt_comer_remark?.text.toString()
                if (validateextraofficerdata(officername, department, designation, remarks)) {
                    val officeritem = ADHdynamicofficer(officername, department, designation, remarks)
                    extraofficerArrayList.add(officeritem)
                    adh_approve_dynamic_view?.layoutManager =
                            LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
                    extraofficeradapter = ADHapprovedynamicAdapter(activity!!, extraofficerArrayList, this)
                    adh_approve_dynamic_view?.adapter =extraofficeradapter
                    setCardViewempty()
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }


        /**
         * getDynamicApprovelDocuments
         *
         */
        val statusID= AdhAppDetailsHome?.adhappdetRES?.data?.approval?.statusId!!
        when(statusID){
            1,2,3 -> seToADpter()

        }

        if (AdhAppDetailsHome.adhappdetRES?.data?.inspectionimage == 0) {
            adh_app_land_img_layout?.visibility = View.VISIBLE
        } else {
            adh_app_land_img_layout?.visibility = View.GONE
        }
    }

    private fun seToADpter() {
        try{
            val adapter = ADHappDetViewDocumetnAdaper(activity!!,adhappdetRES?.data?.documents,this)
            recyl_adh_app_det_approvelist?.layoutManager = LinearLayoutManager(activity,LinearLayout.VERTICAL,false)
            recyl_adh_app_det_approvelist?.adapter = adapter


        }catch (ex :Exception){
            ex.printStackTrace()
        }
    }


    private fun setCardViewempty() {
        try {
            edt_comer_name?.setText("")
            edt_comer_designation?.setText("")
            edt_comer_remark?.setText("")
            edt_dept_name?.setText("")
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }


    private fun validateextraofficerdata(
        officername: String,
        department: String,
        designation: String,
        remarks: String
    ): Boolean {
        try {
            if (officername.isEmpty()) {
                MessageUtils.showSnackBar(activity, adh_app_approv_snackview, getString(R.string.enter_ooficername))
                return false
            } else if (department.isEmpty()) {
                MessageUtils.showSnackBar(activity, adh_app_approv_snackview, getString(R.string.enter_department))
                return false
            } else if (designation.isEmpty()) {
                MessageUtils.showSnackBar(activity, adh_app_approv_snackview, getString(R.string.enter_designation))
                return false
            } else if (remarks.isEmpty()) {
                MessageUtils.showSnackBar(activity, adh_app_approv_snackview, getString(R.string.enter_remark))
                return false
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return true
    }



    override fun onClick(v: View?) {
        try {
            if (v != null) {
                when (v.id) {
                    R.id.adh_app_approve_cancel -> {
                        activity!!.onBackPressed()
                    }

                    R.id.adh_app_approve_submit -> {
                      dialog =MessageUtils.showDialog(activity)
                        val strRemarks = adh_edt_remarks.text.toString().trim()
                        val documentList = ArrayList<ADHAppDetDocumentServerPojo>()
                        val approvalsList = ArrayList<ADHApprovalsserverPojo>()
                        val approvalsmap = HashMap<String, Any>()
                        val documentsmap = HashMap<String, Any>()

                        for (item in AdhAppDetailsHome.adhappdetRES?.data?.documents!!) {
                            if (item?.check!!) {
                                log("check", item?.documentName)
                                val documentserveritem =
                                    ADHAppDetDocumentServerPojo(item.id.toString(), item.documentName)
                                documentList.add(documentserveritem)
                            }
                        }
                        documentsmap.put("document", documentList)

                        for (item in AdhAppDetailsHome.adhappdetRES?.data?.approvals!!) {
                            log("check", item?.applicationStatus)
                            val approvals = ADHApprovalsserverPojo(item?.id.toString(), item?.applicationStatus)
                            approvalsList.add(approvals)
                        }

                        approvalsmap.put("approval", approvalsList)

                        val hashMap = HashMap<String, Any>()
                        hashMap["user_id"] = SessionManager.getUserId(activity).toInt()
                        hashMap["role"] = SessionManager.getuserSopeID(activity).toInt()
                        hashMap["block"] = SessionManager.getBlockId(activity).toInt()
                        hashMap["district"] = SessionManager.getdistrictID(activity).toInt()
                        hashMap["application_id"] = AdhAppDetailsHome.adhappdetRES?.data?.applications?.id!!
                        /*         hashMap["documents"] =documentsmap
                                 hashMap["approvals"] =approvalsmap*/
                        hashMap["documents"] = documentList
                        hashMap["approvals"] = approvalsList

                        val file: File
                        val landimagePart: MultipartBody.Part?
                        val callback: Call<ResponseBody>
                        if (!path.isEmpty()) {
                            file = CompressFile.getCompressedImageFile(File(path), activity)

                            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)

                            landimagePart = MultipartBody.Part.createFormData("land_image", file.getName(), requestFile)



                        }
                        else {

                            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "")

                            landimagePart = MultipartBody.Part.createFormData("land_image", "", requestFile)


                        }
                        hashMap["remarks"] = strRemarks

                        val service = Utils.getInstance(activity)
                        callback= service.call_post(
                            "Ahoapproval",
                            "Bearer " + SessionManager.getToken(activity),
                            hashMap,
                            landimagePart
                        )


                        callback.enqueue(object : Callback<ResponseBody> {
                            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                MessageUtils.dismissDialog(dialog)
                                val msg = MessageUtils.setFailurerMessage(activity, t.message)
                                MessageUtils.showSnackBar(activity, adh_app_img_select, msg)
                            }

                            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                                MessageUtils.dismissDialog(dialog)
                                try {
                                    if (response.isSuccessful) {

                                        val obj = JSONObject(response.body()?.string())
                                        val success = obj.getBoolean("success")
                                        val message = obj.getString("message")
                                        if (success) {

                                            MessageUtils.showToastMessage(activity, message)
                                            FragmentCallUtils.passFragmentWithoutBackStatck(
                                                activity,
                                                ADHPriDashBoard(),
                                                R.id.aho_container_body
                                            )

                                        } else {

                                            MessageUtils.showSnackBar(activity, adh_app_img_select, message)
                                        }

                                    } else {
                                        val msg = MessageUtils.setErrorMessage(response.code())
                                        MessageUtils.showSnackBar(activity, adh_app_img_select, msg)
                                    }
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                            }

                        })
                        log("hashMapsd", "" + hashMap)


                    }
                    R.id.adh_app_img_select -> {
                        if (
                            ContextCompat.checkSelfPermission(
                                activity!!,
                                android.Manifest.permission.CAMERA
                            ) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(
                                activity!!,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE
                            ) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(
                                activity!!,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ) != PackageManager.PERMISSION_GRANTED

                        ) {

                            val dialog = Dialog(activity)
                            dialog.setContentView(R.layout.dialog_permissions)
                            dialog.setCancelable(false)
                            dialog.window.setBackgroundDrawableResource(R.color.dialog_tans)
                            dialog.window.setGravity(Gravity.BOTTOM)
                            dialog.show()
                            dialog.window.setWindowAnimations(R.style.UpDownDialogAnim)
                            dialog.btn_grand.visibility = View.VISIBLE
                            dialog.permission_title.text = "Permission Required"
                            dialog.permission_message.text = "CAMERA\n" +
                                    "The App needs access to the camera to click your land Information\n" +
                                    "\n" +
                                    "STORAGE\n" +
                                    "The App accesses to your document verification"

                            dialog.btn_dismiss.setOnClickListener {
                                dialog.dismiss()
//                        token?.cancelPermissionRequest()
                            }
                            dialog.btn_grand.setOnClickListener {
                                dialog.dismiss()
                                validatePermissions(1)
                            }

                        } else {
                            showImagePickDialog()
                        }

                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {

    }


    private fun showImagePickDialog() {

        val dialog = Dialog(activity)
        dialog.setContentView(R.layout.dialog_pic_img)
        dialog.setCancelable(false)
        dialog.window.setBackgroundDrawableResource(R.color.dialog_tans)
        dialog.window.setGravity(Gravity.BOTTOM)
        dialog.show()
        dialog.window.setWindowAnimations(R.style.UpDownDialogAnim)
        dialog.btn_dismiss.setOnClickListener {
            dialogDismiss(dialog)
        }

        dialog.gallery.setOnClickListener {
            dialogDismiss(dialog)

            choosePhotoFromGallary()
        }
        dialog.camera.setOnClickListener {
            dialogDismiss(dialog)
            takePhotoFromCamera()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_CANCELED) {
            adh_app_lay_file_name.visibility = View.GONE
            return
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data.data
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(activity!!.getContentResolver(), contentURI)
                    path = saveImage(bitmap)
                    log("pathsdsd", "" + path)
                    adh_app_txt_img_file_name.text = File(path).name
                    Toast.makeText(activity, "Image Saved!", Toast.LENGTH_SHORT).show()
                    adh_app_img_land.setImageBitmap(bitmap)
                    adh_app_lay_file_name.visibility = View.VISIBLE
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(activity, "Failed!", Toast.LENGTH_SHORT).show()
                }

            }

        } else if (requestCode == CAMERA) {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            adh_app_img_land.setImageBitmap(thumbnail)

            path = saveImage(thumbnail)
            log("pathCamera", path)
            adh_app_txt_img_file_name.text = File(path).name
            adh_app_lay_file_name.visibility = View.VISIBLE
            Toast.makeText(activity, "Image Saved!", Toast.LENGTH_SHORT).show()
        }
    }

    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            "" + Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY
        )
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }

        try {
            var pathsFileName = "" + Calendar.getInstance().getTimeInMillis() + ".jpg"
            val f = File(wallpaperDirectory, pathsFileName)
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                activity,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            log("TAG", "File Saved::---&gt;" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }

    fun choosePhotoFromGallary() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }


    private fun validatePermissions(isShow: Int) {
        log(TAG, "" + isShow)
        Dexter.withActivity(activity)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (isShow == 1 && report?.areAllPermissionsGranted()!!) {
                        showImagePickDialog()
                    }

                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?, token: PermissionToken?
                ) {
                    log("permissions", "denied")
                    token?.continuePermissionRequest()
                }

            })
            .check()

    }

    private fun showPermissionDalog(token: PermissionToken?) {

        AlertDialog.Builder(activity)
            .setTitle(
                "storage_permission_rationale_title"
            )
            .setMessage(
                "storage_permition_rationale_message"
            )
            .setNegativeButton(
                android.R.string.cancel
            ) { dialog, _ ->
                dialog.dismiss()
                token?.cancelPermissionRequest()
            }
            .setPositiveButton(
                android.R.string.ok
            ) { dialog, _ ->
                dialog.dismiss()
                token?.continuePermissionRequest()
            }
            .setOnDismissListener {
                token?.cancelPermissionRequest()
            }
            .show()
    }

    fun dialogDismiss(dialog: Dialog) {
        if (dialog != null) {
            if (dialog.isShowing) {
                dialog.dismiss()
            }
        }

    }

}
