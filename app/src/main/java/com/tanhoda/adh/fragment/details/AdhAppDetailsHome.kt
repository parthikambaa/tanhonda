package com.tanhoda.adh.fragment.details


import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.os.StrictMode
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.tanhoda.R
import com.tanhoda.adh.ADHActivity
import com.tanhoda.adh.fragment.ApplicationList.ADHApplicationList
import com.tanhoda.adh.fragment.details.app_details.ADHappDetails
import com.tanhoda.adh.fragment.details.farmer_details.ADHFarmerDetails
import com.tanhoda.adh.fragment.details.land_details.ADHLandDetails
import com.tanhoda.adh.fragment.details.tapadapter.AdhtapAdapter

import com.tanhoda.adh.fragment.schemes.schemeapplist.ADHSchemeAppList
import com.tanhoda.ho.fragment.details.land_details.land_view_documents.HoLandViewDocuments
import com.tanhoda.ho.fragment.details.land_details.land_view_status.HOAppLandViewStatus
import com.tanhoda.ho.fragment.details.viewappstatusPojo.HOappviewRES
import com.tanhoda.ho.fragment.details.viewappstatusPojo.StatusupdatesItem
import com.tanhoda.login_splash_screen.SplashActivity
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import com.tanhoda.utils.Utils.log
import customs.CustomTextView
import kotlinx.android.synthetic.main.dialog_view_status_document.*
import kotlinx.android.synthetic.main.fragment_adh_app_details.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AdhAppDetailsHome : Fragment() {

    var adhtapArrayList: ArrayList<String>? = null
    var adhtapAdapter: AdhtapAdapter? = null
    var typeface: Typeface? = null
    var txt: CustomTextView? = null
    var txt1: CustomTextView? = null
    var txt2: CustomTextView? = null

    var dialog: Dialog? = null

    companion object {
        var adhappdetRES: HOappviewRES? = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as ADHActivity).disableNavigationInAdoptionHome(getString(R.string.details))
        return inflater.inflate(R.layout.fragment_adh_app_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())

        txt = CustomTextView(activity, null)
        txt1 = CustomTextView(activity, null)
        txt2 = CustomTextView(activity, null)

        adhtapArrayList = ArrayList()
        adhtapArrayList?.add(getString(R.string.app_details))
        adhtapArrayList?.add(getString(R.string.land_details))
        adhtapArrayList?.add(getString(R.string.farmer_details))

        adh_app_det_viewpager?.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(adh_app_det_taplay))
        adh_app_det_taplay?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                try {
                    when (tab?.position) {
                        0 -> (activity as ADHActivity).disableNavigationInAdoptionHome(getString(R.string.app_details))
                        1 -> (activity as ADHActivity).disableNavigationInAdoptionHome(getString(R.string.land_details))
                        2 -> (activity as ADHActivity).disableNavigationInAdoptionHome(getString(R.string.farmer_details))
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        })

        when {
            Utils.haveNetworkConnection(activity) -> {
                getViewApplication()
            }
            else -> {
                adh_home_empty_List?.visibility = View.VISIBLE
                adh_home_empty_List?.text = getString(R.string.check_internet)
            }

        }

        status_category?.visibility = View.GONE
        view_status_fab_adh?.setOnClickListener {
            if (status_category?.visibility == View.GONE) {
                status_category?.visibility = View.VISIBLE
            } else {
                status_category?.visibility = View.GONE
            }
        }
        status_category?.setOnClickListener {
            status_category?.visibility = View.GONE
        }
        ho_land_view_status_btn?.setOnClickListener {

            try {
                if (adhappdetRES?.data?.statusupdates?.size != 0) {
                    //FragmentCallUtils.passFragmentWithoutAnim(activity, ADHLandViewStatus(), R.id.aho_container_body)

                    status_category?.visibility = View.GONE
                    val intent = Intent(this.context, HOAppLandViewStatus::class.java)
                    val bundle = Bundle()
                    bundle.putSerializable("details", adhappdetRES)
                    intent.putExtras(bundle)
                    startActivity(intent)
                    activity?.overridePendingTransition(0, 0)
                } else {
                    SplashActivity.snackBar =
                        MessageUtils.showSnackBar(activity, adh_app_det_snackview, getString(R.string.no_data_found))
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }
        ho_land_view_document_btn?.setOnClickListener {
            try {
                log("TAG", "Yes Come_1")
                if (adhappdetRES?.data?.documents?.size != 0) {
                    /* FragmentCallUtils.passFragmentWithoutAnim(
                         activity,
                         ADHAppLandViewDocuments(),
                         R.id.aho_container_body
                     )*/
                    log("adhappdetRESsdsd", "" + adhappdetRES?.data?.statusName)
                    status_category?.visibility = View.GONE
                    val intent = Intent(this.context, HoLandViewDocuments::class.java)
                    val bundle = Bundle()
                    bundle.putSerializable("details", adhappdetRES)
                    intent.putExtras(bundle)
                    startActivity(intent)
                    activity?.overridePendingTransition(0, 0)
                } else {
                    SplashActivity.snackBar =
                        MessageUtils.showSnackBar(activity, adh_app_det_snackview, getString(R.string.no_data_found))
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        ho_land_view_pdf_btn.setOnClickListener {
            try {
                if (adhappdetRES != null) {
                    status_category?.visibility = View.GONE
                    Utils.callWebURL(activity, Utils.PDF_LOADER + adhappdetRES?.data?.applications?.id)
                } else {
                    SplashActivity.snackBar =
                        MessageUtils.showSnackBar(activity, adh_app_det_snackview, getString(R.string.no_data_found))
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }


    private fun getViewApplication() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["user_id"] = SessionManager.getUserId(activity)

            if (!ADHSchemeAppList.ADHschemecurentAPP_ID.isEmpty()) {
                map["application_id"] = ADHSchemeAppList.ADHschemecurentAPP_ID
//                ADHSchemeAppList.ADHschemecurentAPP_ID=""
            } else {
                map["application_id"] =
                    ADHApplicationList.adhapplistRESADH?.data?.applications!![ADHApplicationList.adhcurrentposition!!.toInt()]?.id!!
            }

            map["user_scope_id"] = SessionManager.getuserSopeID(activity)
            val service = Utils.getInstance(activity)
            val callback = service.call_post("viewapplication", "Bearer " + SessionManager.getToken(activity), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            adhappdetRES = Gson().fromJson(response.body()?.string(), HOappviewRES::class.java)
                            if (adhappdetRES != null) {
                                if (adhappdetRES?.success!!) {
                                    if (adhappdetRES?.data != null) {

                                        val item = StatusupdatesItem(
                                            1, 1, "", "New Application", "", 0,
                                            adhappdetRES?.data?.applications?.createdAt, 0, null,
                                            "", "Approved"
                                        )
                                        adhappdetRES?.data?.statusupdates?.add(0, item)

                                        if (adhappdetRES?.data?.statusupdates?.size != 0) {
                                            ho_land_view_status_btn?.visibility = View.VISIBLE
                                        } else {
                                            ho_land_view_status_btn?.visibility = View.GONE
                                        }

                                        if (adhappdetRES?.data?.documents?.size != 0) {
                                            ho_land_view_document_btn?.visibility = View.VISIBLE
                                        } else {
                                            ho_land_view_document_btn?.visibility = View.GONE
                                        }
                                        setupViewPager()
                                        adh_home_empty_List?.visibility = View.GONE

                                    } else {

                                        SplashActivity.snackBar = MessageUtils.showSnackBar(
                                            activity,
                                            adh_app_det_taplay,
                                            adhappdetRES?.message
                                        )
                                        adh_home_empty_List?.visibility = View.VISIBLE
                                        adh_home_empty_List?.text = adhappdetRES?.message
                                    }
                                } else {
                                    SplashActivity.snackBar = MessageUtils.showSnackBar(
                                        activity, adh_app_det_taplay,
                                        adhappdetRES?.message
                                    )
                                }

                            } else {
                                SplashActivity.snackBar = MessageUtils.showSnackBar(
                                    activity,
                                    adh_app_det_taplay,
                                    getString(R.string.checkjsonformat)
                                )
                            }


                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            SplashActivity.snackBar = MessageUtils.showSnackBar(activity, adh_app_det_taplay, msg)
                            adh_home_empty_List?.visibility = View.VISIBLE
                            adh_home_empty_List?.text = msg
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }

                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    SplashActivity.snackBar = MessageUtils.showSnackBar(activity, adh_app_det_taplay, msg)
                    adh_home_empty_List?.visibility = View.VISIBLE
                    adh_home_empty_List?.text = msg

                }

            })
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        MessageUtils.dismissSnackBar(SplashActivity.snackBar)
    }

    private fun setupViewPager() {
        try {
            adhtapAdapter = AdhtapAdapter(childFragmentManager)
            adhtapAdapter!!.addFragment(ADHappDetails(), adhtapArrayList?.get(0)!!)
            adhtapAdapter!!.addFragment(ADHLandDetails(), adhtapArrayList?.get(1)!!)
            adhtapAdapter!!.addFragment(ADHFarmerDetails(), adhtapArrayList?.get(2)!!)
            adh_app_det_viewpager?.adapter = adhtapAdapter
            adh_app_det_taplay?.setupWithViewPager(adh_app_det_viewpager)
            initTabs()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    @SuppressLint("ResourceType")
    private fun initTabs() {
        try {

            txt?.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            txt?.gravity = Gravity.CENTER
            txt?.text = adhtapArrayList?.get(0)
            val tab = adh_app_det_taplay?.getTabAt(0)
            if (tab != null) {
                tab.customView = txt
                tab.text = adhtapAdapter!!.getPageTitle(0)
            }
            txt1?.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            txt1?.gravity = Gravity.CENTER
            txt1?.setText(adhtapArrayList?.get(1))
            val tab1 = adh_app_det_taplay?.getTabAt(1)
            if (tab1 != null) {
                tab1.setCustomView(txt1)
                tab1.setText(adhtapAdapter!!.getPageTitle(1))
            }

            txt2?.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            txt2?.gravity = Gravity.CENTER
            txt2?.setText(adhtapArrayList?.get(2))
            val tab2 = adh_app_det_taplay?.getTabAt(2)
            if (tab2 != null) {
                tab2.setCustomView(txt2)
                tab2.setText(adhtapAdapter!!.getPageTitle(2))
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


}