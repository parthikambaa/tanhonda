package com.tanhoda.adh.fragment.schemes.schemeapplist


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.gson.Gson
import com.tanhoda.R
import com.tanhoda.adh.ADHActivity
import com.tanhoda.adh.fragment.details.AdhAppDetailsHome
import com.tanhoda.adh.fragment.schemes.ADHSchemes.Companion.adhschemeid
import com.tanhoda.adh.fragment.schemes.schemeapplist.adapter.ADHscemeAppListADapter
import com.tanhoda.adh.fragment.schemes.schemeapplist.pojo.ADHschemeApplistRES
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.login_splash_screen.SplashActivity
import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_adhscheme_app_list.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ADHSchemeAppList : Fragment(), OnclickPostionEvent {

    var dialog: Dialog? = null
    var schemeAppRES: ADHschemeApplistRES? = null
    companion object {
        var ADHschemecurentAPP_ID=""
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as ADHActivity).disableNavigationInAdoptionHome("Schemes App list")
        return inflater.inflate(R.layout.fragment_adhscheme_app_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)

        /**
         * getting the stages AppList
         *
         */

        when{
            Utils.haveNetworkConnection(activity) ->{

                getttingschemApplist()
            }
            else ->{
                adh_schemeapp_empty_list?.visibility =View.VISIBLE
                adh_schemeapp_empty_list?.text =getString(R.string.check_internet)
                MessageUtils.showSnackBarAction(activity,adh_schemeapp_list_snack_view,getString(R.string.check_internet))
            }
        }

    }

    private fun getttingschemApplist() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["user_id"] =SessionManager.getUserId(activity)
//            map["user_id"] = 8
            map["scheme_id"] = adhschemeid
            val service = Utils.getInstance(activity)
            val callback =
                service.call_post("schemeapplicationlist", "Bearer " + SessionManager.getToken(activity), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    MessageUtils.showSnackBar(activity, adh_schemeapp_list_snack_view, msg)
                    adh_schemeapp_empty_list?.visibility =View.VISIBLE
                    adh_schemeapp_empty_list?.text =msg
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            schemeAppRES = Gson().fromJson(response.body()?.string(), ADHschemeApplistRES::class.java)
                            if (schemeAppRES != null) {
                                if (schemeAppRES?.success!!) {
                                    if (schemeAppRES?.data != null && schemeAppRES?.data?.applications?.size != 0) {
                                        setToadpter()
                                        recy_adh_schemeapp_list?.visibility = View.VISIBLE
                                        adh_schemeapp_empty_list?.visibility = View.GONE

                                    } else {

                                        adh_schemeapp_empty_list?.visibility = View.VISIBLE
                                        adh_schemeapp_empty_list?.setText(schemeAppRES?.message)
                                        recy_adh_schemeapp_list?.visibility = View.GONE
                                    }
                                } else {
                                    MessageUtils.showSnackBar(
                                        activity,
                                        adh_schemeapp_list_snack_view,
                                        schemeAppRES?.message
                                    )
                                }

                            } else {
                                MessageUtils.showSnackBar(
                                    activity,
                                    adh_schemeapp_list_snack_view,
                                    getString(R.string.checkjsonformat)
                                )
                            }

                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            MessageUtils.showSnackBar(activity, adh_schemeapp_list_snack_view, msg)
                            adh_schemeapp_empty_list?.visibility =View.VISIBLE
                            adh_schemeapp_empty_list?.text =msg
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setToadpter() {
        try {
            val schemAppListADpter = ADHscemeAppListADapter(activity!!, schemeAppRES?.data?.applications, this)
            recy_adh_schemeapp_list?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            recy_adh_schemeapp_list?.adapter = schemAppListADpter

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {
        try {
            ADHschemecurentAPP_ID = schemeAppRES?.data?.applications!![postion.toInt()]?.id.toString()
            FragmentCallUtils.passFragmentWithoutAnim(activity, AdhAppDetailsHome() , R.id.aho_container_body)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        MessageUtils.dismissSnackBar(SplashActivity.snackBar)
    }
}
