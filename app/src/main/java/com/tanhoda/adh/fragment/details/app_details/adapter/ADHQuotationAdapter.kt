package com.tanhoda.adh.fragment.details.app_details.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tanhoda.R
import java.io.File

class ADHQuotationAdapter(
    val activity: FragmentActivity,
    val filesArrayList: ArrayList<File>
): RecyclerView.Adapter<ADHQuotationAdapter.viewholder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ADHQuotationAdapter.viewholder {
        return viewholder(LayoutInflater.from(activity).inflate(R.layout.adapter_quotation_files_names,parent,false))
    }

    override fun getItemCount(): Int { return filesArrayList.size }

    override fun onBindViewHolder(holder: ADHQuotationAdapter.viewholder, position: Int) {
        try{
            holder.filename?.text = filesArrayList[position].name
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    class viewholder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val filename =itemView?.findViewById<TextView>(R.id.quotation_file_names_adp)

    }

}