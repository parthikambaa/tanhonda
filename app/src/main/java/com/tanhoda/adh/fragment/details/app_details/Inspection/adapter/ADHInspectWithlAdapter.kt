package com.tanhoda.adh.fragment.details.app_details.Inspection.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.tanhoda.R
import com.tanhoda.adh.fragment.details.app_details.Inspection.pojo.ADHInspectWithPojo
import com.tanhoda.ho.fragment.details.app_details.inspection.pojo.HOInspectWithPojo
import com.tanhoda.interfaces.OnclickPostionEvent
import customs.CustomTextEditView

class ADHInspectWithlAdapter(
    val activity: FragmentActivity,
    val inspectionArrayList: ArrayList<ADHInspectWithPojo>,
    val onclickPostionEvent: OnclickPostionEvent
) : RecyclerView.Adapter<ADHInspectWithlAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ADHInspectWithlAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(activity).inflate(R.layout.adapter_adh_inspect_with, parent, false))
    }

    override fun getItemCount(): Int {
        return inspectionArrayList.size
    }

    override fun onBindViewHolder(holder: ADHInspectWithlAdapter.ViewHolder, position: Int) {
        try {

            holder.name?.setText( inspectionArrayList[position].name )
            holder.designation?.setText( inspectionArrayList[position].designation)
            holder.remarks?.setText( inspectionArrayList[position].remarks )
            holder.remove?.setOnClickListener {
                try{
                    inspectionArrayList.removeAt(position)
                    notifyDataSetChanged()
                }catch (ex:Exception){ex.printStackTrace()}
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var  name   =itemView?.findViewById<CustomTextEditView>(R.id.adh_inspect_with_name_txt_adp)
        var  designation   =itemView?.findViewById<CustomTextEditView>(R.id.adh_inspect_with_designation_txt_adp)
        var  remarks   =itemView?.findViewById<CustomTextEditView>(R.id.adh_inspect_with_remarks_txt_adp)
        var  remove   =itemView?.findViewById<ImageButton>(R.id.adh_inspect_with_removemore_imgbtn_adp)

    }
}