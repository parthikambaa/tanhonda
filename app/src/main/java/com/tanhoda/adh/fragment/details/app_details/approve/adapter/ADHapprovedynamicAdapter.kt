package com.tanhoda.adh.fragment.details.app_details.approve.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import com.tanhoda.R
import com.tanhoda.adh.fragment.details.app_details.approve.pojo.ADHdynamicofficer
import com.tanhoda.interfaces.OnclickPostionEvent
import customs.CustomTextEditView

class ADHapprovedynamicAdapter(
    val context: Context,
    val extraofficerArrayList: ArrayList<ADHdynamicofficer>,
    val onclickPostionEvent: OnclickPostionEvent
) : RecyclerView.Adapter<ADHapprovedynamicAdapter.Viewholder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ADHapprovedynamicAdapter.Viewholder {
        return Viewholder(LayoutInflater.from(context).inflate(R.layout.item_dynamic_txt, parent, false))
    }

    override fun getItemCount(): Int {
        return extraofficerArrayList.size
    }

    override fun onBindViewHolder(holder: ADHapprovedynamicAdapter.Viewholder, position: Int) {
        try {
            holder.department?.setText(extraofficerArrayList[position].department!!)
            holder.officerName?.setText(extraofficerArrayList[position].officerName!!)
            holder.designation?.setText(extraofficerArrayList[position].designation!!)
            holder.remarks?.setText(extraofficerArrayList[position].remarks!!)
            holder.remove?.setOnClickListener {
                try {

                    onclickPostionEvent.OnclickPostionEvent("ADHofficer", position.toString())
                    extraofficerArrayList.removeAt(position)
                    notifyDataSetChanged()
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    class Viewholder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val officerName = itemView?.findViewById<CustomTextEditView>(R.id.edt_comer_name_txt)
        val department = itemView?.findViewById<CustomTextEditView>(R.id.edt_dept_name_txt)
        val designation = itemView?.findViewById<CustomTextEditView>(R.id.edt_comer_designation_txt)
        val remarks = itemView?.findViewById<CustomTextEditView>(R.id.edt_comer_remark_txt)
        val linear = itemView?.findViewById<LinearLayout>(R.id.edt_comer_linear_txt)
        val remove = itemView?.findViewById<ImageButton>(R.id.minus_more_txt)

    }
}