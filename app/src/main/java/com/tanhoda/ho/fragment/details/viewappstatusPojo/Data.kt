package com.tanhoda.ho.fragment.details.viewappstatusPojo

import com.google.gson.annotations.SerializedName

import java.io.Serializable
import javax.annotation.Generated


@Generated("com.robohorse.robopojogenerator")
data class Data(

    @field:SerializedName("documents")
    val documents: ArrayList<DocumentsItem?>? = null,

    @field:SerializedName("level")
    val level: Levelitem? = null,

    @field:SerializedName("statusupdates")
    val statusupdates: ArrayList<StatusupdatesItem?>? = null,

    @field:SerializedName("inspectionimages")
    val inspectionimages: ArrayList<InspectionsImages?>? = null,

    @field:SerializedName("approval")
    val approval: Approval? = null,

    @field:SerializedName("inspectionimage")
    val inspectionimage: Int? = null,

    @field:SerializedName("status_name")
    val statusName: String? = null,

    @field:SerializedName("approvals")
    val approvals: ArrayList<ApprovalsItem?>? = null,

    @field:SerializedName("count")
    val count: Int? = null,

    @field:SerializedName("checkstatus")
    val checkstatus: Int? = null,

    @field:SerializedName("approvalid")
    val approvalid: Approvalid? = null,

	@field:SerializedName("applications")
	val applications: Applications? = null
) :Serializable
