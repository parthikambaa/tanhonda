package com.tanhoda.ho.fragment.details.app_details.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.tanhoda.R
import com.tanhoda.utils.ImageUtils
import java.io.File

class HOQuotationAdapter(
    val activity: FragmentActivity,
    val filesArrayList: ArrayList<File>
) : RecyclerView.Adapter<HOQuotationAdapter.viewholder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewholder {
        return viewholder(LayoutInflater.from(activity).inflate(R.layout.adapter_quotation_files_names, parent, false))
    }

    override fun getItemCount(): Int {
        return filesArrayList.size
    }

    override fun onBindViewHolder(holder: viewholder, position: Int) {
        try {
            holder.filename?.text = filesArrayList[position].name
            ImageUtils.setImage(holder.imageView2, filesArrayList[position].absolutePath, activity)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    class viewholder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val filename = itemView?.findViewById<TextView>(R.id.quotation_file_names_adp)
        val imageView2 = itemView?.findViewById<ImageView>(R.id.imageView2)

    }

}