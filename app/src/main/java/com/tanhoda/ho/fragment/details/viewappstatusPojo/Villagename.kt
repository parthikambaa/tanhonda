package com.tanhoda.ho.fragment.details.viewappstatusPojo

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Villagename(

    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("state_id")
    var stateId: Int? = null,
    @SerializedName("district_id")
    var districtId: Int? = null,
    @SerializedName("block_id")
    var blockId: Int? = null,
    @SerializedName("panchayat_id")
    var panchayatId: Any? = null,
    @SerializedName("village")
    var village: String? = null,
    @SerializedName("village_code")
    var villageCode: Any? = null,
    @SerializedName("village_tamil")
    var villageTamil: Any? = null,
    @SerializedName("office_address")
    var officeAddress: Any? = null,
    @SerializedName("active")
    var active: Int? = null,
    @SerializedName("created_at")
    var createdAt: Any? = null,
    @SerializedName("updated_at")
    var updatedAt:Any?=null

) : Serializable