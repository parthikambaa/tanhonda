package com.tanhoda.ho.fragment.dashboard.adapter

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.tanhoda.R
import com.tanhoda.ho.fragment.dashboard.ho_dashboardpojo.HOappStarusPojo


import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.utils.ImageUtils
import com.tanhoda.utils.Utils

class HOPRIDashbordADapter
    (val context: Context, val statusArrayList: ArrayList<HOappStarusPojo>, val onclickPostionEvent: OnclickPostionEvent)
    : RecyclerView.Adapter<HOPRIDashbordADapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HOPRIDashbordADapter.ViewHolder {
        return   ViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_ho_pri_dashboard,parent,false))
    }

    override fun getItemCount(): Int { return  statusArrayList.size   }

    override fun onBindViewHolder(holder: HOPRIDashbordADapter.ViewHolder, position: Int) {
        try{

            ImageUtils.displayImageFromUrl(context,Utils.IMG_URL+statusArrayList[position].image,holder.image)
            holder.statusName?.text = statusArrayList[position].statusName
            holder.statusno?.text = statusArrayList[position].status_no
            holder.master?.setOnClickListener {
                try{
                    onclickPostionEvent.OnclickPostionEvent(statusArrayList[position].statusName,position.toString())
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val  image =itemView?.findViewById<ImageView>(R.id.imgv_app_pri_dashboard_icon)
        val  statusName =itemView?.findViewById<customs.CustomTextView>(R.id.txt_app_pri_dashboard_name)
        val  statusno =itemView?.findViewById<customs.CustomTextView>(R.id.txt_app_pri_dashboard_no)
        val  master =itemView?.findViewById<ConstraintLayout>(R.id.ho_pri_dashboard_master_layout)
    }

}