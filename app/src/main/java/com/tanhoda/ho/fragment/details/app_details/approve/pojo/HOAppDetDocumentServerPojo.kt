package com.tanhoda.ho.fragment.details.app_details.approve.pojo

import com.google.gson.annotations.SerializedName

class HOAppDetDocumentServerPojo(
    @field:SerializedName("document_value")
    var document_value:String?=null,
    @field:SerializedName("document_name")
    var document_name:String?=null
)