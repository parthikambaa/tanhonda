package com.tanhoda.ho.fragment.applicationList.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.tanhoda.R
import com.tanhoda.ho.fragment.applicationList.pojo.ApplicationsItem
import com.tanhoda.interfaces.OnclickPostionEvent
import customs.CustomTextView

class HoApplicationListAdapter (val context: Context, val hoappArrayList: ArrayList<ApplicationsItem?>?, val onclickEvent: OnclickPostionEvent) :
    RecyclerView.Adapter<HoApplicationListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HoApplicationListAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.adapter_ho_application_list,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return hoappArrayList?.size!!
    }

    override fun onBindViewHolder(holder: HoApplicationListAdapter.ViewHolder, position: Int) {
        try {
            holder.siNo?.text = (position + 1).toString()
            holder.category?.text = hoappArrayList!![position]?.appcategory?.category
            holder.category?.setSingleLine()
            holder.category?.ellipsize = TextUtils.TruncateAt.MARQUEE
            holder.category?.isSelected = true
            holder.componet?.text = hoappArrayList!![position]?.appcomponent?.componentName
            holder.componet?.setSingleLine()
            holder.componet?.ellipsize = TextUtils.TruncateAt.MARQUEE
            holder.componet?.isSelected = true
            holder.farmerName?.text =hoappArrayList!![position]?.appfarmer?.name
            holder.farmerName?.setSingleLine()
            holder.farmerName?.ellipsize = TextUtils.TruncateAt.MARQUEE
            holder.farmerName?.isSelected = true
            holder.scheme?.text = hoappArrayList!![position]?.appscheme?.shortName
            holder.scheme?.setSingleLine()
            holder.scheme?.ellipsize = TextUtils.TruncateAt.MARQUEE
            holder.scheme?.isSelected = true
            holder.layoutCompat?.setOnClickListener {
                try {
                    onclickEvent.OnclickPostionEvent("Jack", position.toString())
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val siNo = itemView?.findViewById<CustomTextView>(R.id.txt_sino_ho_app_list_adp)
        val scheme = itemView?.findViewById<CustomTextView>(R.id.txt_sheme_ho_app_list_adp)
        val category = itemView?.findViewById<CustomTextView>(R.id.txt_category_ho_app_list_adp)
        val componet = itemView?.findViewById<CustomTextView>(R.id.txt_component_ho_app_list_adp)
        val farmerName = itemView?.findViewById<CustomTextView>(R.id.txt_farmer_name_ho_app_list_adp)
        val layoutCompat = itemView?.findViewById<LinearLayout>(R.id.ho_adap_linear)

    }
}