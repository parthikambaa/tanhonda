package com.tanhoda.ho.fragment.dashboard.ho_dashboardpojo

import com.google.gson.annotations.SerializedName

class HOappStarusPojo(
    @field:SerializedName("image")
    var image:String,
    @field:SerializedName("status_name")
    var statusName:String,
    @field:SerializedName("status_no")
    var status_no:String

)