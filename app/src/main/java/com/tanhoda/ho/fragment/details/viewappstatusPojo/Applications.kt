package com.tanhoda.ho.fragment.details.viewappstatusPojo

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.annotation.Generated


@Generated("com.robohorse.robopojogenerator")
data class Applications(

    @field:SerializedName("appfarmer")
    val appfarmer: Appfarmer? = null,

    @field:SerializedName("scheme")
    val scheme: Int? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("appscheme")
    val appscheme: Appscheme? = null,

    @field:SerializedName("land_id")
    val landId: Int? = null,

    @field:SerializedName("estimate_cost")
    val estimateCost: String? = null,

    @field:SerializedName("appblock")
    val appblock: Appblock? = null,

    @field:SerializedName("passport_photo")
    val passportPhoto: Any? = null,

    @field:SerializedName("water_test")
    val waterTest: String? = null,

    @field:SerializedName("land_records")
    val landRecords: Any? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("farmer_id")
    val farmerId: Int? = null,

    @field:SerializedName("soil_test")
    val soilTest: String? = null,

    @field:SerializedName("application_status")
    val applicationStatus: String? = null,

    @field:SerializedName("block")
    val block: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("state")
    val state: Int? = null,

    @field:SerializedName("village")
    val village: Int? = null,

    @field:SerializedName("appdistrict")
    val appdistrict: Appdistrict? = null,

    @field:SerializedName("area_proposed")
    val areaProposed: String? = null,

    @field:SerializedName("appcategory")
    val appcategory: Appcategory? = null,

    @field:SerializedName("appstate")
    val appstate: Appstate? = null,

	@field:SerializedName("villagename")
	val villagename: Villagename? = null,

	@field:SerializedName("bank_loan")
	val bankLoan: String? = null,


    @field:SerializedName("active")
    val active: String? = null,

    @field:SerializedName("eligiblity_amount")
    val eligiblityAmount: Any? = null,

    @field:SerializedName("application_id")
    val applicationId: String? = null,

    @field:SerializedName("govt_subsidy")
    val govtSubsidy: String? = null,

    @field:SerializedName("waiting_for")
    val waitingFor: String? = null,

    @field:SerializedName("relevant")
    val relevant: String? = null,

    @field:SerializedName("component")
    val component: Int? = null,

    @field:SerializedName("appcomponent")
    val appcomponent: Appcomponent? = null,

    @field:SerializedName("appland")
    val appland: Appland? = null,

    @field:SerializedName("district")
    val district: Int? = null,

    @field:SerializedName("proposed_crop")
    val proposedCrop: String? = null,

    @field:SerializedName("application_date")
    val applicationDate: String? = null,

    @field:SerializedName("category")
    val category: Int? = null,

    @field:SerializedName("proposal_upload")
    val proposalUpload: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)   : Serializable


