package com.tanhoda.ho.fragment.details.land_details


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tanhoda.R
import com.tanhoda.ho.fragment.details.HoAppDetailsHome
import kotlinx.android.synthetic.main.fragment_ho_land_details.*

class HOLandDetails : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ho_land_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)





    }

    override fun onResume() {
        super.onResume()
        try{
                    setDatatoForm()
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setDatatoForm() {
        try{
            val data = HoAppDetailsHome.hoappviewRES?.data?.applications!!.appland!!
            val appitem = HoAppDetailsHome.hoappviewRES?.data?.applications!!
            if(data.serveyNo!=null){
                custxtviw_ho_land_survey_number?.setText(data.serveyNo)
            }
            if(data.sourceOfIrrigation!=null){
                custxtviw_ho_land_survey_irragtion?.setText(data.sourceOfIrrigation)
            }
            if(data.totalArea!=null){
                custxtviw_ho_land_total_area?.setText(data.totalArea)
            }
            if(appitem.village!=null){
                custxtviw_ho_land_village?.setText(appitem.villagename?.village)
            }
            if(appitem.appblock!=null){
                custxtviw_ho_land_block?.setText(appitem.appblock.block)
            }
            if(appitem.appdistrict!=null){
                custxtviw_ho_land_district?.setText(appitem.appdistrict.district)
            }
            if(appitem.appstate!=null){
                custxtviw_ho_land_state?.setText(appitem.appstate.state)
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
}
