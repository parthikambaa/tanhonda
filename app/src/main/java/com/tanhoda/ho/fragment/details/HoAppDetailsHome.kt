package com.tanhoda.ho.fragment.details


import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.os.StrictMode
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.tanhoda.R
import com.tanhoda.ho.HOActivity
import com.tanhoda.ho.fragment.applicationList.HoApplicationList
import com.tanhoda.ho.fragment.details.adapter.HoappDetailsTapadapter
import com.tanhoda.ho.fragment.details.app_details.HOappDetails
import com.tanhoda.ho.fragment.details.farmer_details.HoFarmerDetails
import com.tanhoda.ho.fragment.details.land_details.HOLandDetails
import com.tanhoda.ho.fragment.details.land_details.land_view_documents.HoLandViewDocuments
import com.tanhoda.ho.fragment.details.land_details.land_view_status.HOAppLandViewStatus
import com.tanhoda.ho.fragment.details.viewappstatusPojo.HOappviewRES
import com.tanhoda.ho.fragment.details.viewappstatusPojo.StatusupdatesItem
import com.tanhoda.ho.fragment.schemes.schemesapplis.HOSchemsAppList
import com.tanhoda.login_splash_screen.SplashActivity
import com.tanhoda.login_splash_screen.SplashActivity.Companion.snackBar
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import com.tanhoda.utils.Utils.log
import customs.CustomTextView
import kotlinx.android.synthetic.main.dialog_view_status_document.*
import kotlinx.android.synthetic.main.fragment_ho_details.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HoAppDetailsHome : Fragment() {

    var hotapArrayList: ArrayList<String>? = null
    var hoTapadapter: HoappDetailsTapadapter? = null
    var typeface: Typeface? = null
    var txt: CustomTextView? = null
    var txt1: CustomTextView? = null
    var txt2: CustomTextView? = null
    var dialog: Dialog? = null

    companion object {
        var hoappviewRES: HOappviewRES? = null
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as HOActivity).disableNavigationInAdoptionHome(getString(R.string.details))
        return inflater.inflate(R.layout.fragment_ho_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog = Dialog(activity)

        val newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())

        txt = CustomTextView(activity, null)
        txt1 = CustomTextView(activity, null)
        txt2 = CustomTextView(activity, null)

        hotapArrayList = ArrayList()
        hotapArrayList?.add(getString(R.string.app_details))
        hotapArrayList?.add(getString(R.string.land_details))
        hotapArrayList?.add(getString(R.string.farmer_details))

        ho_app_det_viewpager?.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(ho_app_det_taplay))

        ho_app_det_taplay?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                try {
                    when (tab?.position) {
                        0 -> (activity as HOActivity).disableNavigationInAdoptionHome(getString(R.string.app_details))
                        1 -> (activity as HOActivity).disableNavigationInAdoptionHome(getString(R.string.land_details))
                        2 -> (activity as HOActivity).disableNavigationInAdoptionHome(getString(R.string.farmer_details))
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        })


        status_category.visibility = View.GONE

        when {
            Utils.haveNetworkConnection(activity) -> {
                getViewApplication()
            }
            else -> {
                ho_home_empty_List?.visibility = View.VISIBLE
                ho_home_empty_List?.text = getString(R.string.check_internet)
                MessageUtils.showSnackBarAction(activity, ho_app_det_snackview, getString(R.string.check_internet))
            }

        }



        view_status_fab?.setOnClickListener {
            if (status_category?.visibility == View.GONE) {
                status_category?.visibility = View.VISIBLE
            } else {
                status_category?.visibility = View.GONE
            }
        }
        status_category?.setOnClickListener {
            status_category?.visibility = View.GONE
        }

        /**
         * set on click Listester
         */
        ho_land_view_status_btn?.setOnClickListener {

            try {
                if (hoappviewRES?.data?.statusupdates?.size != 0) {
                    //FragmentCallUtils.passFragmentWithoutAnim(activity, HOAppLandViewStatus(), R.id.aho_container_body)
                    status_category?.visibility = View.GONE
                    val intent = Intent(this.context, HOAppLandViewStatus::class.java)
                    val bundle = Bundle()
                    bundle.putSerializable("details", hoappviewRES)
                    intent.putExtras(bundle)
                    startActivity(intent)

                } else {

                    snackBar =
                        MessageUtils.showSnackBar(activity, ho_app_det_snackview, getString(R.string.no_data_found))

                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }
        ho_land_view_document_btn?.setOnClickListener {
            try {
                if (hoappviewRES?.data?.documents?.size != 0) {
                    status_category?.visibility = View.GONE
                    //FragmentCallUtils.passFragmentWithoutAnim(activity, HoLandViewDocuments(), R.id.aho_container_body)
                    val intent = Intent(this.context, HoLandViewDocuments::class.java);
                    val bundle = Bundle()
                    bundle.putSerializable("details", hoappviewRES)

                    intent.putExtras(bundle)
                    startActivity(intent)

                } else {
                    snackBar =
                        MessageUtils.showSnackBar(activity, ho_app_det_snackview, getString(R.string.no_data_found))
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        ho_land_view_pdf_btn.setOnClickListener {
            try {

                if (hoappviewRES != null) {
                    status_category?.visibility = View.GONE
                    Utils.callWebURL(activity, Utils.PDF_LOADER + hoappviewRES?.data?.applications?.id)
                } else {
                    SplashActivity.snackBar =
                        MessageUtils.showSnackBar(activity, ho_app_det_snackview, getString(R.string.no_data_found))
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

    }

    private fun setupViewPager() {
        try {
            log("HO_DEtails", "Pager_setup")
            hoTapadapter = HoappDetailsTapadapter(childFragmentManager)
            hoTapadapter!!.addFragment(HOappDetails(), hotapArrayList?.get(0)!!)
            hoTapadapter!!.addFragment(HOLandDetails(), hotapArrayList?.get(1)!!)
            hoTapadapter!!.addFragment(HoFarmerDetails(), hotapArrayList?.get(2)!!)
            ho_app_det_viewpager?.adapter = hoTapadapter
            ho_app_det_taplay?.setupWithViewPager(ho_app_det_viewpager)

            if (hoappviewRES?.data?.documents?.size != 0 || hoappviewRES?.data?.statusupdates?.size != 0) {
                view_status_fab?.visibility = View.VISIBLE
            } else {
                view_status_fab?.visibility = View.GONE
            }

            initTabs()

/*            log("clklskdlsd", "" + hoappviewRES?.data?.count)
            if (hoappviewRES?.data?.count != 0) {
                log("clklskdlsd", "0000")
                view_status_fab?.setPadding(0, 0, 16, 276)
                //view_status_fab?.visibility =View.GONE
                linear_ho_land?.setPadding(0, 0, 0, 176)
            } else {
                log("clklskdlsd", "111")
                view_status_fab?.setPadding(0, 0, 16, 56)
                linear_ho_land?.setPadding(0, 0, 0, 114)
            }*/
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    @SuppressLint("ResourceType")
    private fun initTabs() {
        try {

            txt?.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            txt?.gravity = Gravity.CENTER
            txt?.text = hotapArrayList?.get(0)
            val tab = ho_app_det_taplay?.getTabAt(0)
            if (tab != null) {
                tab.customView = txt
                tab.text = hoTapadapter!!.getPageTitle(0)
            }
            txt1?.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            txt1?.gravity = Gravity.CENTER
            txt1?.text = hotapArrayList?.get(1)
            val tab1 = ho_app_det_taplay?.getTabAt(1)
            if (tab1 != null) {
                tab1.customView = txt1
                tab1.text = hoTapadapter!!.getPageTitle(1)
            }

            txt2?.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            txt2?.gravity = Gravity.CENTER
            txt2?.text = hotapArrayList?.get(2)
            val tab2 = ho_app_det_taplay?.getTabAt(2)
            if (tab2 != null) {
                tab2.customView = txt2
                tab2.text = hoTapadapter!!.getPageTitle(2)
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun getViewApplication() {
        try {
            val loadDialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["user_id"] = SessionManager.getUserId(activity)
//            map["application_id"] ="19"

            if (!HOSchemsAppList.hoschemecurentAPP_ID.isEmpty()) {
                map["application_id"] = HOSchemsAppList.hoschemecurentAPP_ID
//                HOSchemsAppList.hoschemecurentAPP_ID = ""
                log("IDAPP", "" + map["application_id"])

            } else {
                map["application_id"] =
                    HoApplicationList.hoAppListRES?.data?.applications!![HoApplicationList.hocurrentpostion!!.toInt()]?.id!!
                log("IDScheme", "" + map["application_id"])

            }

            log("ID", "" + map["application_id"])

            map["user_scope_id"] = SessionManager.getuserSopeID(activity)
            val service = Utils.getInstance(activity)
            val callback = service.call_post("viewapplication", "Bearer " + SessionManager.getToken(activity), map)

            callback.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    log("HO_DEtails", "111")
                    MessageUtils.dismissDialog(loadDialog)
                    try {
                        log("HO_DEtails", "22")
                        if (response.isSuccessful) {
                            hoappviewRES = Gson().fromJson(response.body()?.string(), HOappviewRES::class.java)
                            log("HO_DEtails", "33")
                            if (hoappviewRES != null) {
                                log("HO_DEtails", "44")
                                if (hoappviewRES?.success!!) {
                                    log("HO_DEtails", "55")
                                    if (hoappviewRES?.data != null) {


                                        val item = StatusupdatesItem(
                                            1, 1, "", "New Application", "", 0,
                                            hoappviewRES?.data?.applications?.createdAt, 0, null,
                                            "", "Approved"
                                        )
                                        hoappviewRES?.data?.statusupdates?.add(0, item)
                                        log("HO_DEtails", "66")
                                        if (hoappviewRES?.data?.statusupdates?.size != 0) {
                                            log("HO_DEtails", "77")
                                            ho_land_view_status_btn?.visibility = View.VISIBLE
                                        } else {
                                            ho_land_view_status_btn?.visibility = View.GONE
                                        }

                                        if (hoappviewRES?.data?.documents?.size != 0) {
                                            ho_land_view_document_btn?.visibility = View.VISIBLE
                                        } else {
                                            ho_land_view_document_btn?.visibility = View.GONE
                                        }
                                        setupViewPager()
                                        ho_home_empty_List?.visibility = View.GONE
                                    } else {
                                        ho_home_empty_List?.visibility = View.VISIBLE
                                        snackBar = MessageUtils.showSnackBar(
                                            activity,
                                            ho_app_det_taplay,
                                            hoappviewRES?.message
                                        )
                                    }

                                } else {
                                    snackBar = MessageUtils.showSnackBar(
                                        activity, ho_app_det_taplay,
                                        hoappviewRES?.message
                                    )
                                }

                            } else {
                                snackBar = MessageUtils.showSnackBar(
                                    activity,
                                    ho_app_det_taplay,
                                    getString(R.string.checkjsonformat)
                                )
                            }


                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            snackBar = MessageUtils.showSnackBar(activity, ho_app_det_taplay, msg)
                            ho_home_empty_List?.visibility = View.VISIBLE
                            ho_home_empty_List?.text = msg

                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }

                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    snackBar = MessageUtils.showSnackBar(activity, ho_app_det_taplay, msg)
                    ho_home_empty_List?.visibility = View.VISIBLE
                    ho_home_empty_List?.text = msg
                    view_status_fab?.visibility = View.GONE

                }


            })
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}
