package com.tanhoda.ho.fragment.dashboard.ho_pri_dashboard_pojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("head")
	val head: Head? = null,

	@field:SerializedName("work")
	val work: Work? = null,

	@field:SerializedName("district")
	val district: District? = null,

	@field:SerializedName("block")
	val block: Block? = null,

	@field:SerializedName("payment")
	val payment: Payment? = null
)