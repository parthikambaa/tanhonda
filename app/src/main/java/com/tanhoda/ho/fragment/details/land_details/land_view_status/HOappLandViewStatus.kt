package com.tanhoda.ho.fragment.details.land_details.land_view_status


import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import com.tanhoda.R
import com.tanhoda.ho.fragment.details.land_details.land_view_status.adapter.HOAppLandViewStatusAdapter
import com.tanhoda.ho.fragment.details.viewappstatusPojo.HOappviewRES
import kotlinx.android.synthetic.main.fragment_ho_app_land_view_status.*

class HOAppLandViewStatus : AppCompatActivity() {

    val TAG = "HOAPPLICATIONLIST"
    var dialog: Dialog? = null
    lateinit var hoappviewRES: HOappviewRES

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_ho_app_land_view_status)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setToadapter()

    }

    private fun setToadapter() {
        try {
            hoappviewRES = intent.extras.getSerializable("details") as HOappviewRES
            if (hoappviewRES.data?.statusupdates?.size != 0) {
                val adapter = HOAppLandViewStatusAdapter(this, hoappviewRES?.data?.statusupdates)
                re_ho_app_land_view_status_list?.layoutManager =
                    LinearLayoutManager(this, LinearLayout.VERTICAL, false)
                re_ho_app_land_view_status_list?.adapter = adapter
                ho_app_land_view_starus_empty_view.visibility = View.GONE
                re_ho_app_land_view_status_list.visibility = View.VISIBLE
                ho_app_land_view_status_linear.visibility = View.VISIBLE
            } else {
                ho_app_land_view_starus_empty_view.visibility = View.VISIBLE
                re_ho_app_land_view_status_list.visibility = View.GONE
                ho_app_land_view_starus_empty_view.text = hoappviewRES.message

            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            ho_app_land_view_starus_empty_view.visibility = View.VISIBLE
            re_ho_app_land_view_status_list.visibility = View.GONE
            ho_app_land_view_starus_empty_view.text = ex.message
        }
    }
}
