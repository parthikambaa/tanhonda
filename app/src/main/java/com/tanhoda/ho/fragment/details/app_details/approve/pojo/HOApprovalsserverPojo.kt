package com.tanhoda.ho.fragment.details.app_details.approve.pojo


import com.google.gson.annotations.SerializedName

class HOApprovalsserverPojo(
    @field:SerializedName("approval_value")
    var approval_value: String? = null,
    @field: SerializedName("approval_name")
    var approval_name: String? = null
)