package com.tanhoda.ho.fragment.details.app_details.inspection.pojo

import com.google.gson.annotations.SerializedName

class HOInspectWithPojo (
    @field:SerializedName("name")
    var name:String?=null,
    @field:SerializedName("designation")
    var designation:String?=null,
    @field:SerializedName("remarks")
    var remarks:String?=null
)