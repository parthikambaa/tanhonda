package com.tanhoda.ho.fragment.applicationList


import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.google.gson.Gson
import com.tanhoda.R
import com.tanhoda.ho.HOActivity
import com.tanhoda.ho.fragment.applicationList.adapter.HoApplicationListAdapter
import com.tanhoda.ho.fragment.applicationList.pojo.HoAppListRES
import com.tanhoda.ho.fragment.dashboard.HoAppDashboard
import com.tanhoda.ho.fragment.details.HoAppDetailsHome
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.login_splash_screen.SplashActivity.Companion.snackBar

import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_ho_application_list.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HoApplicationList : Fragment(), OnclickPostionEvent {


    var hoApplicationadapter: HoApplicationListAdapter? = null
    val TAG = "AHOAPPLICATIONLIST"
    var dialog: Dialog? = null
    var snackViewHOUserList: RelativeLayout? = null
    var snackbar: Snackbar? = null
    companion object {
        var hocurrentpostion: String? = null
        var hoAppListRES: HoAppListRES? = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as HOActivity).disableNavigationInAdoptionHome(getString(R.string.applicationlist))
        return inflater.inflate(R.layout.fragment_ho_application_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)
        snackViewHOUserList = view.findViewById(R.id.rel_lay_ho_app_list_snackview)


        /*
         * get AHO Application List*/

        if (Utils.haveNetworkConnection(activity)) {
            getApplicationList()
        } else {
            ho_app_empty_list?.visibility =View.VISIBLE
            ho_app_empty_list?.text =getString(R.string.check_internet)
            snackBar =MessageUtils.showSnackBar(activity, snackViewHOUserList, getString(R.string.check_internet))
        }


    }

    private fun getApplicationList() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["userid"] = SessionManager.getUserId(activity)
            map["role"] =SessionManager.getuserSopeID(activity)
            map["status"] =HoAppDashboard.currentstatusname
            map["block"] =SessionManager.getBlockId(activity)
            map["district"]=SessionManager.getdistrictID(activity)
            val callinterface = Utils.getInstance(activity)
            val callback =
                callinterface.call_post("applicationlist", "Bearer " + SessionManager.getToken(activity), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t!!.message)
                    snackbar = MessageUtils.showSnackBar(activity, snackViewHOUserList, msg)
                    ho_app_empty_list?.visibility =View.VISIBLE
                    ho_app_empty_list?.text =msg
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            hoAppListRES =
                                    Gson().fromJson(response.body()?.string(), HoAppListRES::class.java)
                            if (hoAppListRES != null) {
                                if (hoAppListRES?.success!!) {
                                    if (hoAppListRES?.data != null) {
                                        if (hoAppListRES?.data?.applications != null && hoAppListRES?.data?.applications?.size != 0) {
                                            setToADapter()
                                            ho_app_empty_list?.visibility = View.GONE
                                            recy_ho_app_list?.visibility = View.VISIBLE
                                            ho_app_list_title_linear?.visibility = View.VISIBLE
                                        } else {
                                            ho_app_list_title_linear?.visibility = View.GONE
                                            recy_ho_app_list?.visibility = View.GONE
                                            ho_app_empty_list?.visibility = View.VISIBLE
                                            ho_app_empty_list?.text = hoAppListRES?.message
                                        }

                                    } else {
                                        snackBar=      MessageUtils.showSnackBar(
                                            activity,
                                            snackViewHOUserList,
                                            hoAppListRES?.message
                                        )
                                    }
                                } else {
                                    snackBar=      MessageUtils.showSnackBar(
                                        activity,
                                        snackViewHOUserList,
                                        hoAppListRES?.message
                                    )
                                }
                            } else {
                                snackBar=     MessageUtils.showSnackBar(
                                    activity,
                                    snackViewHOUserList,
                                    getString(R.string.checkjsonformat)
                                )
                            }
                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            ho_app_empty_list?.visibility =View.VISIBLE
                            ho_app_empty_list?.text =msg
                            snackBar=    MessageUtils.showSnackBar(activity, snackViewHOUserList, msg)
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }

                }

            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setToADapter() {
        try {
            recy_ho_app_list?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            hoApplicationadapter = HoApplicationListAdapter(activity!!, hoAppListRES?.data?.applications,this)
            recy_ho_app_list?.adapter = hoApplicationadapter
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {
        try {
            MessageUtils.dismissSnackBar(snackBar)
            hocurrentpostion=postion
            FragmentCallUtils.passFragment(activity, HoAppDetailsHome(), R.id.aho_container_body)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        MessageUtils.dismissSnackBar(snackBar)
    }


}
