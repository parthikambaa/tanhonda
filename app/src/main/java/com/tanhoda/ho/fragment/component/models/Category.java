
package com.tanhoda.ho.fragment.component.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Category {

    @SerializedName("active")
    private Long mActive;
    @SerializedName("category")
    private String mCategory;
    @SerializedName("categoryscheme")
    private Categoryscheme mCategoryscheme;
    @SerializedName("create_date")
    private String mCreateDate;
    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("id")
    private Long mId;
    @SerializedName("scheme_id")
    private Long mSchemeId;
    @SerializedName("updated_at")
    private String mUpdatedAt;

    public Long getActive() {
        return mActive;
    }

    public void setActive(Long active) {
        mActive = active;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String category) {
        mCategory = category;
    }

    public Categoryscheme getCategoryscheme() {
        return mCategoryscheme;
    }

    public void setCategoryscheme(Categoryscheme categoryscheme) {
        mCategoryscheme = categoryscheme;
    }

    public String getCreateDate() {
        return mCreateDate;
    }

    public void setCreateDate(String createDate) {
        mCreateDate = createDate;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Long getSchemeId() {
        return mSchemeId;
    }

    public void setSchemeId(Long schemeId) {
        mSchemeId = schemeId;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

}
