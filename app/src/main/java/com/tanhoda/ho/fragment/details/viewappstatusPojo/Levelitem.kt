package com.tanhoda.ho.fragment.details.viewappstatusPojo

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data  class Levelitem (
    @field:SerializedName("id")
    var id :Int?=null,
    @field:SerializedName("level_name")
    var level_name :String?=null,
    @field:SerializedName("stage_name")
    var stage_name :String?=null,
    @field:SerializedName("user_scope_id")
    var user_scope_id :String?=null,
    @field:SerializedName("img")
    var img :String?=null,
    @field:SerializedName("created_at")
    var created_at :String?=null,
    @field:SerializedName("updated_at")
    var updated_at :String?=null
): Serializable

