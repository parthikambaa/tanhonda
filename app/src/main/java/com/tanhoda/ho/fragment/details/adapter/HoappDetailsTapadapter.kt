package com.tanhoda.ho.fragment.details.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import java.util.ArrayList

class HoappDetailsTapadapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {
    private val fragmentlist = ArrayList<Fragment>()
    private val fragmenttitlelist = ArrayList<String>()


    fun addFragment(fragment: Fragment, title: String) {
        fragmentlist.add(fragment)
        fragmenttitlelist.add(title)
    }

    override fun getItem(position: Int): android.support.v4.app.Fragment? {
        return fragmentlist[position]
    }


    override fun getCount(): Int {
        return fragmentlist.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return fragmenttitlelist[position]
    }

}