package com.tanhoda.ho.fragment.details.app_details.approve.pojo

import com.google.gson.annotations.SerializedName

class Hodynamicofficer(
    @field:SerializedName("officer_name")
    var officerName:String?=null,
    @field:SerializedName("department")
    var department:String?=null,
    @field:SerializedName("designation")
    var designation:String?=null,
    @field:SerializedName("remarks")
    var remarks:String?=null
)