
package com.tanhoda.ho.fragment.sub_component.pojo;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.ArrayList;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("subcomponent")
    private ArrayList<Subcomponent> mSubcomponent;

    public ArrayList<Subcomponent> getSubcomponent() {
        return mSubcomponent;
    }

    public void setSubcomponent(ArrayList<Subcomponent> subcomponent) {
        mSubcomponent = subcomponent;
    }

}
