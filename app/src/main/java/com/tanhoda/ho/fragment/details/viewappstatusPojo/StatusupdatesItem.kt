package com.tanhoda.ho.fragment.details.viewappstatusPojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class StatusupdatesItem(

	@field:SerializedName("approved_by")
	val approvedBy: Int? = null,

	@field:SerializedName("status_id")
	val statusId: Int? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("application_status")
	val applicationStatus: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("approved_date")
	val approvedDate: String? = null,

	@field:SerializedName("application_id")
	val applicationId: Int? = null,

	@field:SerializedName("approveuser")
	val approveuser: Approveuser? = null,

	@field:SerializedName("remarks")
	val remarks: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)   : Serializable

