package com.tanhoda.ho.fragment.schemes.schemesapplis

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.gson.Gson

import com.tanhoda.R

import com.tanhoda.ho.HOActivity
import com.tanhoda.ho.fragment.details.HoAppDetailsHome
import com.tanhoda.ho.fragment.schemes.HOShems
import com.tanhoda.ho.fragment.schemes.schemesapplis.adapter.HOschemesAppListAdapter
import com.tanhoda.ho.fragment.schemes.schemesapplis.pojo.HOschemeAppListRES
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.login_splash_screen.SplashActivity.Companion.snackBar

import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_ho_schmes_app_list.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HOSchemsAppList : Fragment(), OnclickPostionEvent {

    var dialog: Dialog? = null
    var schemeAppRES: HOschemeAppListRES? = null

    companion object {
        var hoschemecurentAPP_ID = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as HOActivity).disableNavigationInAdoptionHome(getString(R.string.scheme_app_List))
        return inflater.inflate(R.layout.fragment_ho_schmes_app_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)

        /**
         * getting the stages AppList
         */

        when {
            Utils.haveNetworkConnection(activity) -> {
                gettingSchemApplist()
            }
            else -> {
                ho_schemeapp_empty_list?.visibility = View.VISIBLE
                ho_schemeapp_empty_list?.text = getString(R.string.check_internet)
                snackBar = MessageUtils.showSnackBarAction(
                    activity,
                    ho_schemeapp_list_snack_view,
                    getString(R.string.check_internet)
                )
            }
        }

    }

    private fun gettingSchemApplist() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
//            map["user_id"] =SessionManager.getUserId(activity)
            map["user_id"] = 8
            map["scheme_id"] = HOShems.hoschemeId
            val service = Utils.getInstance(activity)
            val callback =
                service.call_post("schemeapplicationlist", "Bearer " + SessionManager.getToken(activity), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    snackBar = MessageUtils.showSnackBar(activity, ho_schemeapp_list_snack_view, msg)
                    ho_schemeapp_empty_list?.visibility = View.VISIBLE
                    ho_schemeapp_empty_list?.text = msg
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            schemeAppRES = Gson().fromJson(response.body()?.string(), HOschemeAppListRES::class.java)
                            if (schemeAppRES != null) {
                                if (schemeAppRES?.success!!) {
                                    if (schemeAppRES?.data != null && schemeAppRES?.data?.applications?.size != 0) {
                                        setToadpter()
                                        recy_ho_schemeapp_list?.visibility = View.VISIBLE
                                        ho_schemeapp_empty_list?.visibility = View.GONE
                                        //ho_schemeapp_list_title_linear?.visibility = View.VISIBLE
                                    } else {
                                        //ho_schemeapp_list_title_linear?.visibility = View.GONE
                                        ho_schemeapp_empty_list?.visibility = View.VISIBLE
                                        ho_schemeapp_empty_list?.setText(schemeAppRES?.message)
                                        recy_ho_schemeapp_list?.visibility = View.GONE
                                    }
                                } else {
                                    snackBar = MessageUtils.showSnackBar(
                                        activity,
                                        ho_schemeapp_list_snack_view,
                                        schemeAppRES?.message
                                    )
                                }

                            } else {
                                snackBar = MessageUtils.showSnackBar(
                                    activity,
                                    ho_schemeapp_list_snack_view,
                                    getString(R.string.checkjsonformat)
                                )
                            }

                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            snackBar = MessageUtils.showSnackBar(activity, ho_schemeapp_list_snack_view, msg)
                            ho_schemeapp_empty_list?.visibility = View.VISIBLE
                            ho_schemeapp_empty_list?.text = msg
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setToadpter() {
        try {
            val schemAppListADpter = HOschemesAppListAdapter(activity!!, schemeAppRES?.data?.applications, this)
            recy_ho_schemeapp_list?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            recy_ho_schemeapp_list?.adapter = schemAppListADpter

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {
        try {
            hoschemecurentAPP_ID = schemeAppRES?.data?.applications!![postion.toInt()]?.id.toString()
            FragmentCallUtils.passFragmentWithoutAnim(activity, HoAppDetailsHome(), R.id.aho_container_body)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        MessageUtils.dismissSnackBar(snackBar)
    }
}
