package com.tanhoda.ho.fragment.details.viewappstatusPojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class Appstate(

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("state_tamil")
	val stateTamil: Any? = null,

	@field:SerializedName("active")
	val active: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: Any? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("state")
	val state: String? = null
)    : Serializable

