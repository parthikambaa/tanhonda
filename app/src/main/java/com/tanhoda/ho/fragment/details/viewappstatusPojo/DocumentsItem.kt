package com.tanhoda.ho.fragment.details.viewappstatusPojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class DocumentsItem(

	@field:SerializedName("verified_date")
	val verifiedDate: Any? = null,

	@field:SerializedName("document_name")
	val documentName: String? = null,

	@field:SerializedName("image_name")
	val imageName: String? = null,

	@field:SerializedName("verified_by")
	val verifiedBy: Any? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("verifyuser")
	val verifyuser: Any? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("application_id")
	val applicationId: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
	,
	@field:SerializedName("check")
	var check: Boolean =false
)   : Serializable

