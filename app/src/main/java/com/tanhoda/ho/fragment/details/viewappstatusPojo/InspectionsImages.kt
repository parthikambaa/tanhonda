package com.tanhoda.ho.fragment.details.viewappstatusPojo
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class InspectionsImages(
    @SerializedName("id")
    var id: Int,
    @SerializedName("application_id")
    var applicationId: Int,
    @SerializedName("statusupdate_id")
    var statusupdateId: Int,

    @SerializedName("image")
    var image: String,

    @SerializedName("type")
    var type: String,

    @SerializedName("created_at")
    var createdAt: String,

    @SerializedName("updated_at")
    var  updatedAt:String
)  : Serializable

