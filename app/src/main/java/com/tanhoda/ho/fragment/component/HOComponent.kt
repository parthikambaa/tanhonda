package com.tanhoda.ho.fragment.component


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.tanhoda.R
import com.tanhoda.ho.HOActivity
import com.tanhoda.ho.fragment.component.adapter.ItemComponent
import com.tanhoda.ho.fragment.component.models.ModelComponentCategoryBase
import com.tanhoda.login_splash_screen.SplashActivity
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_component.*
import kotlinx.android.synthetic.main.fragment_ho_sub_component.ho_component_snackview
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HOComponent : Fragment() {
    lateinit var dialog: Dialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        (activity as HOActivity).disableNavigationInAdoptionHome("Component")
        return inflater.inflate(R.layout.fragment_component, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recy_compo_list.layoutManager = LinearLayoutManager(activity)
        recy_compo_list.setHasFixedSize(false)
        if (Utils.haveNetworkConnection(activity)) {
            getComponentDetails()
        } else {
            recy_compo_list.visibility = View.GONE
            SplashActivity.snackBar =
                MessageUtils.showSnackBar(activity, ho_component_snackview, getString(R.string.check_internet))
            val msg = MessageUtils.setFailurerMessage(activity, getString(R.string.check_internet))
            header_component.visibility = View.GONE
            compo_empty_list?.visibility = View.VISIBLE
            compo_empty_list?.text = msg
        }
    }

    private fun getComponentDetails() {
        dialog = MessageUtils.showDialog(activity)
        var map = HashMap<String, Any>()
        map["user_id"] = SessionManager.getUserId(activity)
        map["block"] = SessionManager.getBlockId(activity)
        map["role"] = SessionManager.getuserSopeID(activity)
        map["district"] = SessionManager.getdistrictID(activity)
        map["user_scope_id"] = SessionManager.getuserSopeID(activity)
        val calinterface = Utils.getInstance(activity)
        val callback = calinterface.call_post("category", "Bearer " + SessionManager.getToken(activity!!), map)
        callback.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
                val msg = MessageUtils.setFailurerMessage(activity, t.message)
                SplashActivity.snackBar = MessageUtils.showSnackBar(activity, ho_component_snackview, msg)
                compo_empty_list?.visibility = View.VISIBLE
                compo_empty_list?.text = msg
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                MessageUtils.dismissDialog(dialog)
                try {
                    if (response.isSuccessful) {
                        val modelComponentBase: ModelComponentCategoryBase =
                            Gson().fromJson(response.body()?.string(), ModelComponentCategoryBase::class.java)
                        var count = 0
                        if (modelComponentBase != null) {
                            if (modelComponentBase.data.category != null) {
                                count = modelComponentBase.data.category.size

                            }
                            if (count != 0) {
                                recy_compo_list.visibility = View.VISIBLE
                                header_component.visibility = View.VISIBLE
                                compo_empty_list?.visibility = View.GONE
                                val adapter = ItemComponent(
                                    activity,
                                    modelComponentBase.data.category
                                )
                                recy_compo_list.adapter = adapter

                            } else {
                                header_component.visibility = View.GONE

                                recy_compo_list.visibility = View.GONE
                                compo_empty_list?.visibility = View.VISIBLE
                                compo_empty_list?.text = modelComponentBase.message

                            }
                        } else {
                            val msg = ""
                            SplashActivity.snackBar = MessageUtils.showSnackBar(activity, ho_component_snackview, msg)
                            compo_empty_list?.visibility = View.VISIBLE
                            compo_empty_list?.text = msg
                            recy_compo_list.visibility = View.GONE

                            header_component.visibility = View.GONE
                        }
                    } else {
                        val msg = MessageUtils.setErrorMessage(response.code())
                        SplashActivity.snackBar = MessageUtils.showSnackBar(activity, ho_component_snackview, msg)
                        compo_empty_list?.visibility = View.VISIBLE
                        compo_empty_list?.text = msg
                        recy_compo_list.visibility = View.GONE

                        header_component.visibility = View.GONE
                    }
                } catch (Ex: Exception) {
                    Ex.printStackTrace()
                }
            }

        })
    }


}
