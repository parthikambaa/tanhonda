package com.tanhoda.ho.fragment.component.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.tanhoda.R;
import com.tanhoda.ho.fragment.component.models.Category;
import customs.CustomTextView;

import java.util.List;

public class ItemComponent extends RecyclerView.Adapter<ItemComponent.ViewHolderFromItem> {
    private final List<Category> categories;
    private final FragmentActivity context;

    public ItemComponent(FragmentActivity context, List<Category> categories) {
        this.context = context;
        this.categories = categories;
    }

    @NonNull
    @Override
    public ViewHolderFromItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolderFromItem(LayoutInflater.from(context).inflate(R.layout.item_component, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderFromItem holder, int position) {
        holder.item_component.setText(categories.get(position).getCategory());
        holder.item_scheme.setText(categories.get(position).getCategoryscheme().getSchemeName());
        holder.item_sin_no.setText("" + (position + 1));
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class ViewHolderFromItem extends RecyclerView.ViewHolder {
        CustomTextView item_component, item_sin_no, item_scheme;

        public ViewHolderFromItem(View itemView) {
            super(itemView);
            item_component = itemView.findViewById(R.id.item_component);
            item_sin_no = itemView.findViewById(R.id.item_si_no);
            item_scheme = itemView.findViewById(R.id.item_scheme_name);
        }
    }
}
