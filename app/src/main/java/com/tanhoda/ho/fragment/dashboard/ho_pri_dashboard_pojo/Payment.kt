package com.tanhoda.ho.fragment.dashboard.ho_pri_dashboard_pojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Payment(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("paymentcount")
	val paymentcount: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)