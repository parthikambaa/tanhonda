package com.tanhoda.ho.fragment.details.viewappstatusPojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class Appland(

	@field:SerializedName("total_area")
	val totalArea: String? = null,

	@field:SerializedName("land_ownership")
	val landOwnership: String? = null,

	@field:SerializedName("servey_no")
	val serveyNo: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("source_of_irrigation")
	val sourceOfIrrigation: String? = null,

	@field:SerializedName("lease_agreement")
	val leaseAgreement: String? = null,

	@field:SerializedName("chitta_copy")
	val chittaCopy: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("farmer_id")
	val farmerId: Int? = null,

	@field:SerializedName("district")
	val district: Int? = null,

	@field:SerializedName("adangal_copy")
	val adangalCopy: String? = null,

	@field:SerializedName("block")
	val block: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("state")
	val state: Int? = null,

	@field:SerializedName("vao_certificate")
	val vaoCertificate: String? = null,

	@field:SerializedName("village")
	val village: Int? = null,

	@field:SerializedName("patta_copy")
	val pattaCopy: String? = null
)  : Serializable

