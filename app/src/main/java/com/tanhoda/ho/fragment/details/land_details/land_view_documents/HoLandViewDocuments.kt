package com.tanhoda.ho.fragment.details.land_details.land_view_documents

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.GridLayout
import com.tanhoda.R
import com.tanhoda.ho.fragment.details.land_details.land_view_documents.adapter.HoAppLandDocumentAdapter
import com.tanhoda.ho.fragment.details.land_details.land_view_documents.adapter.HoInspectionImageAdapter
import com.tanhoda.ho.fragment.details.viewappstatusPojo.HOappviewRES
import com.tanhoda.interfaces.onViewImages
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_ho_land_view_documents.*


class HoLandViewDocuments : AppCompatActivity(), onViewImages {
    var hoappviewRES: HOappviewRES? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_ho_land_view_documents)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setTOADapter()
    }

    private fun setTOADapter() {
        try {
            hoappviewRES = intent.extras.getSerializable("details") as HOappviewRES
            if (hoappviewRES?.data?.documents != null && hoappviewRES?.data?.documents?.size != 0) {
                val adapter = HoAppLandDocumentAdapter(this, hoappviewRES?.data?.documents, this)
                recy_ho_app_land_view_document_list?.layoutManager = GridLayoutManager(
                    this, 2,
                    GridLayout.VERTICAL, false
                )
                recy_ho_app_land_view_document_list?.adapter = adapter
            }

            if (hoappviewRES?.data?.inspectionimages != null && hoappviewRES?.data?.inspectionimages?.size != 0) {
                txt_ho_land_view_document_inspection?.visibility = View.VISIBLE
                val adapter =
                    HoInspectionImageAdapter(this, hoappviewRES?.data?.inspectionimages, this)
                recy_ho_app_land_view_document_inspection_list?.layoutManager = GridLayoutManager(
                    this, 2,
                    GridLayout.VERTICAL, false
                )
                recy_ho_app_land_view_document_inspection_list?.adapter = adapter
            } else {
                txt_ho_land_view_document_inspection?.visibility = View.GONE
            }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onLoadData(imageUrl: String, imageType: String, from: String) {
        try {
            Utils.viewDocumentImages(this, imageUrl, imageType, from)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}
