package com.tanhoda.ho.fragment.dashboard


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import com.google.gson.Gson

import com.tanhoda.R
import com.tanhoda.ho.HOActivity
import com.tanhoda.ho.fragment.dashboard.adapter.HOPRIDashbordADapter
import com.tanhoda.ho.fragment.dashboard.ho_dashboardpojo.HOappStarusPojo
import com.tanhoda.ho.fragment.dashboard.ho_pri_dashboard_pojo.HoPriDashboardRES
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.login_splash_screen.SplashActivity
import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_ho_pri_dashboard.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HoPriDashboard : Fragment(), OnclickPostionEvent {

    var dialog: Dialog? = null
    var priDashboradArrayList = ArrayList<HOappStarusPojo>()

    companion object {
        var currentclickpostionaname = ""
        var dashboradRES: HoPriDashboardRES? = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as HOActivity).enableToogle(getString(R.string.dashboard))
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ho_pri_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when {
            Utils.haveNetworkConnection(activity) -> {
                getDashboardRes()
            }

            else -> {
                ho_pri_dashboard_empty_List?.visibility = View.VISIBLE
                ho_pri_dashboard_empty_List?.text = getString(R.string.check_internet)
                SplashActivity.snackBar =
                    MessageUtils.showSnackBar(activity, ho_pridash_snackView, getString(R.string.check_internet))

            }
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as HOActivity).enableToogle(getString(R.string.dashboard))
    }

    private fun getDashboardRes() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["user_id"] = SessionManager.getUserId(activity)
            map["block"] = SessionManager.getBlockId(activity)
            map["role"] = SessionManager.getuserSopeID(activity)
            map["district"] = SessionManager.getdistrictID(activity)
            val calinterface = Utils.getInstance(activity)
            val callback =
                calinterface.call_post("officerprofile", "Bearer " + SessionManager.getToken(activity!!), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    SplashActivity.snackBar = MessageUtils.showSnackBar(activity, ho_pridash_snackView, msg)
                    ho_pri_dashboard_empty_List?.visibility = View.VISIBLE
                    ho_pri_dashboard_empty_List?.text = msg
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            dashboradRES = Gson().fromJson(response.body()?.string(), HoPriDashboardRES::class.java)
                            if (dashboradRES != null) {
                                if (dashboradRES?.success!!) {
                                    if (dashboradRES?.data != null) {
                                        priDashboradArrayList.clear()
                                        setApplicationStatus()

                                        ho_pri_dashboard_empty_List?.visibility = View.GONE
                                    } else {
                                        ho_pri_dashboard_empty_List?.visibility = View.VISIBLE
                                        SplashActivity.snackBar = MessageUtils.showSnackBar(
                                            activity, ho_pridash_snackView,
                                            dashboradRES?.message
                                        )

                                    }

                                } else {
                                    SplashActivity.snackBar = MessageUtils.showSnackBar(
                                        activity, ho_pridash_snackView,
                                        dashboradRES?.message
                                    )
                                }

                            } else {
                                SplashActivity.snackBar = MessageUtils.showSnackBar(
                                    activity,
                                    ho_pridash_snackView,
                                    getString(R.string.checkjsonformat)
                                )
                            }
                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            SplashActivity.snackBar = MessageUtils.showSnackBar(activity, ho_pridash_snackView, msg)
                            ho_pri_dashboard_empty_List?.visibility = View.VISIBLE
                            ho_pri_dashboard_empty_List?.text = msg
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setApplicationStatus() {
        try {
            val newapp = HOappStarusPojo(
                dashboradRES?.data?.block?.img!!,
                dashboradRES?.data?.block?.status!!,
                dashboradRES?.data?.block?.blockcount.toString()
            )
            priDashboradArrayList.add(newapp)
            val preinspection = HOappStarusPojo(
                dashboradRES?.data?.district?.img!!, dashboradRES?.data?.district?.status!!,
                dashboradRES?.data?.district?.districtcount.toString()
            )
            priDashboradArrayList.add(preinspection)
            val hoapprovel = HOappStarusPojo(
                dashboradRES?.data?.head?.img!!,
                dashboradRES?.data?.head?.status!!,
                dashboradRES?.data?.head?.headcount.toString()
            )
            priDashboradArrayList.add(hoapprovel)

            val affidavit = HOappStarusPojo(
                dashboradRES?.data?.work?.img!!,
                dashboradRES?.data?.work?.status!!,
                dashboradRES?.data?.work?.workcount.toString()
            )
            priDashboradArrayList.add(affidavit)

            val adhapproval = HOappStarusPojo(
                dashboradRES?.data?.payment?.img!!,
                dashboradRES?.data?.payment?.status!!,
                dashboradRES?.data?.payment?.paymentcount.toString()
            )
            priDashboradArrayList.add(adhapproval)


            recy_ho_pri_dashboard_list?.layoutManager = GridLayoutManager(activity, 2, GridLayout.VERTICAL, false)
            val statsusAdapter = HOPRIDashbordADapter(activity!!, priDashboradArrayList, this)
            recy_ho_pri_dashboard_list?.adapter = statsusAdapter
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {
        try {
            currentclickpostionaname = name
            if (priDashboradArrayList[postion.toInt()].status_no.toInt() != 0) {
                FragmentCallUtils.passFragment(activity, HoAppDashboard(), R.id.aho_container_body)
            } else {
                SplashActivity.snackBar =
                    MessageUtils.showSnackBar(activity, ho_pridash_snackView, getString(R.string.no_data_found))

            }

//            FragmentCallUtils.passFragment(activity, AHOAppDetails(), R.id.aho_container_body)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        MessageUtils.dismissSnackBar(SplashActivity.snackBar)
    }
}