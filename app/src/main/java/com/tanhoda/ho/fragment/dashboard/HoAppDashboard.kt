package com.tanhoda.ho.fragment.dashboard


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import com.google.gson.Gson
import com.tanhoda.R
import com.tanhoda.ho.HOActivity
import com.tanhoda.ho.fragment.applicationList.HoApplicationList
import com.tanhoda.ho.fragment.dashboard.adapter.HOappStatusAdapter
import com.tanhoda.ho.fragment.dashboard.ho_dashboardpojo.HOappStarusPojo
import com.tanhoda.ho.fragment.dashboard.ho_dashboardpojo.HOdashboardRES
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.login_splash_screen.SplashActivity
import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_ho_dashboard.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HoAppDashboard : Fragment(), OnclickPostionEvent {

    var dialog: Dialog? = null
    var dashboradRES: HOdashboardRES? = null
    var itemarraylist = ArrayList<HOappStarusPojo>()


    companion object {
        var currentstatusname = ""
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as HOActivity).disableNavigationInAdoptionHome(getString(R.string.dashboard))
        return inflater.inflate(R.layout.fragment_ho_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /**
         * get and set Application status
         * static Menus
         */
        when {
            Utils.haveNetworkConnection(activity) -> {
                getDashboardRes()
            }
            else -> {
                ho_dashboard_empty_List?.visibility = View.VISIBLE
                ho_dashboard_empty_List?.text = getString(R.string.check_internet)
                SplashActivity.snackBar =
                    MessageUtils.showSnackBar(activity, ho_dash_snackView, getString(R.string.check_internet))
            }
        }

    }

    private fun getDashboardRes() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["user_id"] = SessionManager.getUserId(activity)
            map["block"] = SessionManager.getBlockId(activity)
            map["role"] = SessionManager.getuserSopeID(activity)
            map["district"] = SessionManager.getdistrictID(activity)
            map["status"] = HoPriDashboard.currentclickpostionaname
            val calinterface = Utils.getInstance(activity)
            val callback = calinterface.call_post("dashboard", "Bearer " + SessionManager.getToken(activity!!), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    SplashActivity.snackBar = MessageUtils.showSnackBar(activity, ho_dash_snackView, msg)
                    ho_dashboard_empty_List?.visibility = View.VISIBLE
                    ho_dashboard_empty_List?.text = msg
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            dashboradRES = Gson().fromJson(response.body()?.string(), HOdashboardRES::class.java)
                            if (dashboradRES != null) {
                                if (dashboradRES?.success!!) {
                                    if (dashboradRES?.data != null) {
                                        itemarraylist.clear()
                                        for (item in dashboradRES?.data!!) {
                                            val ahostatuspojo =
                                                HOappStarusPojo(item?.img!!, item?.stageName!!, item.count.toString())
                                            itemarraylist.add(ahostatuspojo)
                                        }
                                        setApplicationStatus(itemarraylist)
                                        ho_dashboard_empty_List?.visibility = View.GONE
                                    } else {
                                        ho_dashboard_empty_List?.visibility = View.VISIBLE
                                        MessageUtils.showSnackBar(activity, ho_dash_snackView, dashboradRES?.message)
                                    }

                                } else {
                                    SplashActivity.snackBar =
                                        MessageUtils.showSnackBar(activity, ho_dash_snackView, dashboradRES?.message)
                                }

                            } else {
                                SplashActivity.snackBar = MessageUtils.showSnackBar(
                                    activity,
                                    ho_dash_snackView,
                                    getString(R.string.checkjsonformat)
                                )
                            }
                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            SplashActivity.snackBar = MessageUtils.showSnackBar(activity, ho_dash_snackView, msg)
                            ho_dashboard_empty_List?.visibility = View.VISIBLE
                            ho_dashboard_empty_List?.text = msg
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setApplicationStatus(itemarraylist: ArrayList<HOappStarusPojo>) {
        try {
            recy_ho_app_status_list?.layoutManager = GridLayoutManager(activity, 2, GridLayout.VERTICAL, false)
            val statsusAdapter = HOappStatusAdapter(activity!!, itemarraylist, this)
            recy_ho_app_status_list?.adapter = statsusAdapter
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {
        try {
            currentstatusname = name
            if (itemarraylist[postion.toInt()].status_no.toInt() != 0) {
                FragmentCallUtils.passFragment(activity, HoApplicationList(), R.id.aho_container_body)
            } else {
                SplashActivity.snackBar =
                    MessageUtils.showSnackBar(activity, ho_dash_snackView, getString(R.string.no_data_found))
            }

//            FragmentCallUtils.passFragment(activity, AHOAppDetails(), R.id.aho_container_body)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        MessageUtils.dismissSnackBar(SplashActivity.snackBar)
    }
}
