package com.tanhoda.ho.fragment.details.app_details.inspection


import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

import com.tanhoda.R
import com.tanhoda.ho.HOActivity
import com.tanhoda.ho.fragment.dashboard.HoPriDashboard
import com.tanhoda.ho.fragment.details.HoAppDetailsHome
import com.tanhoda.ho.fragment.details.app_details.approve.pojo.HOAppDetDocumentServerPojo
import com.tanhoda.ho.fragment.details.app_details.approve.pojo.HOApprovalsserverPojo
import com.tanhoda.ho.fragment.details.app_details.inspection.adapter.HOINspectionImageAdpter
import com.tanhoda.ho.fragment.details.app_details.inspection.adapter.HOInspectProjectAdapter
import com.tanhoda.ho.fragment.details.app_details.inspection.adapter.HOInspectWithlAdapter
import com.tanhoda.ho.fragment.details.app_details.inspection.pojo.HOInspectWithPojo
import com.tanhoda.ho.fragment.details.app_details.inspection.pojo.HOInspectprojectpojo
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.utils.*
import com.tanhoda.utils.Utils.log
import kotlinx.android.synthetic.main.bottom_cancel_approve.*
import kotlinx.android.synthetic.main.dialog_dismiss_btn.*
import kotlinx.android.synthetic.main.dialog_permissions.*
import kotlinx.android.synthetic.main.fragment_ho_inspection.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class HOInspection : Fragment(), OnclickPostionEvent {

    var inspectPROJECTList = ArrayList<HOInspectprojectpojo>()
    var inspectWIthList = ArrayList<HOInspectWithPojo>()
    var inspectproAdpater: HOInspectProjectAdapter? = null
    var inspectwithAdpater: HOInspectWithlAdapter? = null

    var invoicefileList: ArrayList<File>? = null
    var inspectionfileList: ArrayList<File>? = null

    var dialog: Dialog? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as HOActivity).disableNavigationInAdoptionHome(HoAppDetailsHome.hoappviewRES?.data?.statusName!!)
        return inflater.inflate(R.layout.fragment_ho_inspection, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        ho_inspect_capact_edt?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                log("Inspection after", "" + s)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                log("Inspection before", "" + s)
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (!s.isNullOrEmpty() && !s?.equals("0")!!) {
                    log("Inspection", "" + s)
                    val value = s.toString().toInt()

                    val qty = ho_inspect_quantity_edt?.text.toString()
                    log("Inspection", "" + qty)
                    if (qty.isEmpty() || qty.toInt() <= 0) {
                        MessageUtils.showSnackBar(activity, ho_inspection, "Quantity can't Be Empty")
                    } else {
                        val total = value * qty.toInt()
                        ho_inspect_amount_edt?.setText(total.toString())
                    }


                }
            }

        })


        ho_inspect_quantity_edt?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                log("Inspection", "" + s)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                log("Inspection", "" + s)
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                log("Inspection", "" + s)
                if (!s.isNullOrEmpty() && !s?.equals("0")!!) {
                    val value = s.toString().toInt()
                    val qty = ho_inspect_capact_edt?.text.toString()
                    if (qty.isEmpty() || qty.toInt() <= 0) {
                        MessageUtils.showSnackBar(activity, ho_inspection, "Capacity can't Be Empty")
                    } else {
                        val total = value * qty.toInt()
                        ho_inspect_amount_edt?.setText(total.toString())
                    }

                } else {

                }
            }

        })


        ho_inspect_act_capacity_edt?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                log("Inspection after", "" + s)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                log("Inspection before", "" + s)
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (!s.isNullOrEmpty() && !s?.equals("0")!!) {
                    log("Inspection", "" + s)
                    val value = s.toString().toInt()

                    val qty = ho_inspect_act_quantity_edt?.text.toString()
                    log("Inspection", "" + qty)
                    if (qty.isEmpty() || qty.toInt() <= 0) {
                        MessageUtils.showSnackBar(activity, ho_inspection, "Quantity can't Be Empty")
                    } else {
                        val total = value * qty.toInt()
                        ho_inspect_act_amnt_edt?.setText(total.toString())
                    }

                } else {

                }
            }

        })


        ho_inspect_act_quantity_edt?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                log("Inspection", "" + s)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                log("Inspection", "" + s)
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                log("Inspection", "" + s)
                if (!s.isNullOrEmpty() && !s?.equals("0")!!) {
                    val value = s.toString().toInt()

                    val qty = ho_inspect_act_capacity_edt?.text.toString()
                    if (qty.isEmpty() || qty.toInt() <= 0) {
                        MessageUtils.showSnackBar(activity, ho_inspection, "Capacity can't Be Empty")
                    } else {
                        val total = value * qty.toInt()
                        ho_inspect_act_amnt_edt?.setText(total.toString())
                    }

                } else {

                }
            }

        })


        /**
         * set onclickListener
         *
         */
        ho_inspect_invoice_txt?.setOnClickListener {
            try {
                getruntimepermissionforMedia(7)

            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }

        ho_inspect_image_txt?.setOnClickListener {
            try {
                getruntimepermissionforMedia(8)

            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }


        ho_inspection?.setOnClickListener {
            try {
                val projectparticular = ho_inspect_particul_edt?.text.toString()
                val projectspecification = ho_inspect_specifi_edt?.text.toString()
                val projectcapacity = ho_inspect_capact_edt?.text.toString()
                val projectquantity = ho_inspect_quantity_edt?.text.toString()
                val projectamount = ho_inspect_amount_edt?.text.toString()

                val inspectspecificaion = ho_inspect_act_specification_edt?.text.toString()
                val inspectcapacity = ho_inspect_act_capacity_edt?.text.toString()
                val inspectquntity = ho_inspect_act_quantity_edt?.text.toString()
                val inspectamount = ho_inspect_act_amnt_edt?.text.toString()
                val inspectremarks = ho_inspect_act_remarks_edt?.text.toString()


                if (validationfoact(
                        projectparticular,
                        projectspecification,
                        projectcapacity,
                        projectquantity,
                        projectamount,
                        inspectspecificaion,
                        inspectcapacity,
                        inspectquntity,
                        inspectamount,
                        inspectremarks
                    )
                ) {

                    val listitem = HOInspectprojectpojo(
                        projectparticular,
                        projectspecification,
                        projectcapacity,
                        projectquantity,
                        projectamount,
                        inspectspecificaion,
                        inspectcapacity,
                        inspectquntity,
                        inspectamount,
                        inspectremarks
                    )
                    inspectPROJECTList.add(listitem)
                    inspectproAdpater = HOInspectProjectAdapter(activity!!, inspectPROJECTList, this)
                    recyl_ho_inspect_act?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
                    recyl_ho_inspect_act?.adapter = inspectproAdpater
                    setToInspectionEmpty()
                }


            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }




        ho_inspect_with_addmore?.setOnClickListener {
            try {
                val name = ho_inspect_with_name_edt?.text.toString()
                val designation = ho_inspect_with_designation_edt?.text.toString()
                val remarks = ho_inspect_with_remarks_edt?.text.toString()

                if (validationfowith(name, designation, remarks)) {
                    val inspectActitem = HOInspectWithPojo(name, designation, remarks)
                    inspectWIthList.add(inspectActitem)
                    inspectwithAdpater = HOInspectWithlAdapter(activity!!, inspectWIthList, this)
                    recyl_ho_inspect_with?.layoutManager =
                            LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
                    recyl_ho_inspect_with?.adapter = inspectwithAdpater
                    setTOInspectWith()
                }


            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }


        btn_cancel?.setOnClickListener {
            try {
                activity!!.onBackPressed()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        btn_approval?.setOnClickListener {
            try {

                if (inspectionfileList != null) {
                    if (ho_inspectionbottom_remarks_edt?.text.toString().isEmpty()) {
                        MessageUtils.showSnackBar(
                            activity,
                            ho_inspection,
                            "Inspection Remarks Can't Be Empty"
                        )
                    } else {

                        val projectparticular = ho_inspect_particul_edt?.text.toString()
                        val projectspecification = ho_inspect_specifi_edt?.text.toString()
                        val projectcapacity = ho_inspect_capact_edt?.text.toString()
                        val projectquantity = ho_inspect_quantity_edt?.text.toString()
                        val projectamount = ho_inspect_amount_edt?.text.toString()

                        val inspectspecificaion = ho_inspect_act_specification_edt?.text.toString()
                        val inspectcapacity = ho_inspect_act_capacity_edt?.text.toString()
                        val inspectquntity = ho_inspect_act_quantity_edt?.text.toString()
                        val inspectamount = ho_inspect_act_amnt_edt?.text.toString()
                        val inspectremarks = ho_inspect_act_remarks_edt?.text.toString()

                        val name = ho_inspect_with_name_edt?.text.toString()
                        val designation = ho_inspect_with_designation_edt?.text.toString()
                        val remarks = ho_inspect_with_remarks_edt?.text.toString()

                        if (validationfoact(
                                projectparticular,
                                projectspecification,
                                projectcapacity,
                                projectquantity,
                                projectamount,
                                inspectspecificaion,
                                inspectcapacity,
                                inspectquntity,
                                inspectamount,
                                inspectremarks
                            )
                        ) {

                            val listitem = HOInspectprojectpojo(
                                projectparticular,
                                projectspecification,
                                projectcapacity,
                                projectquantity,
                                projectamount,
                                inspectspecificaion,
                                inspectcapacity,
                                inspectquntity,
                                inspectamount,
                                inspectremarks
                            )
                            inspectPROJECTList.add(listitem)

                            if (validationfowith(name, designation, remarks)) {
                                val inspectActitem = HOInspectWithPojo(name, designation, remarks)
                                inspectWIthList.add(inspectActitem)
                                when {
                                    Utils.haveNetworkConnection(activity) -> uploadToSERver()
                                    else -> MessageUtils.showSnackBar(
                                        activity,
                                        ho_inspection,
                                        getString(R.string.check_internet)
                                    )
                                }
                            }
                        }
                    }
                } else {
                    MessageUtils.showSnackBar(activity, ho_inspection, "File Can't be Empty")
                }


            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }


    }

    private fun setTOInspectWith() {
        try{

            ho_inspect_with_name_edt?.setText("")
            ho_inspect_with_designation_edt?.setText("")
            ho_inspect_with_remarks_edt?.setText("")
        } catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setToInspectionEmpty() {
        try{
            ho_inspect_particul_edt?.setText("")
            ho_inspect_specifi_edt?.setText("")
            ho_inspect_capact_edt?.setText("")
            ho_inspect_quantity_edt?.setText("")
            ho_inspect_amount_edt?.setText("")

            ho_inspect_act_specification_edt?.setText("")
            ho_inspect_act_capacity_edt?.setText("")
            ho_inspect_act_quantity_edt?.setText("")
            ho_inspect_act_amnt_edt?.setText("")
            ho_inspect_act_remarks_edt?.setText("")


        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }



    private fun validationfoact(
        projectparticular: String,
        projectspecification: String,
        projectcapacity: String,
        projectquantity: String,
        projectamount: String,
        inspectspecificaion: String,
        inspectcapacity: String,
        inspectquntity: String,
        inspectamount: String,
        inspectremarks: String
    ): Boolean {
        try {

            when {
                projectparticular.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, ho_inspection, "Particulars Can't Be Empty")
                    return false
                }
                projectspecification.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, ho_inspection, "Specification Can't Be Empty")
                    return false
                }
                projectcapacity.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, ho_inspection, "Capacity Can't Be Empty")
                    return false
                }
                projectquantity.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, ho_inspection, "Quantity Can't Be Empty")
                    return false
                }
                projectamount.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, ho_inspection, "Amount Can't Be Empty")
                    return false
                }
                inspectspecificaion.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, ho_inspection, "Specification Can't Be Empty")
                    return false
                }
                inspectcapacity.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, ho_inspection, "Capacity Can't Be Empty")
                    return false
                }
                inspectquntity.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, ho_inspection, "Quantity Can't Be Empty")
                    return false
                }
                inspectamount.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, ho_inspection, "Amount Can't Be Empty")
                    return false
                }
                inspectremarks.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, ho_inspection, "Remarks Can't Be Empty")
                    return false
                }

            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return true
    }

    private fun uploadToSERver() {
        try {
            dialog = MessageUtils.showDialog(activity)

            val documentList = ArrayList<HOAppDetDocumentServerPojo>()
            val approvalsList = ArrayList<HOApprovalsserverPojo>()

            for (item in HoAppDetailsHome.hoappviewRES?.data?.documents!!) {
                if (item?.check!!) {
                    log("check", item?.documentName)
                    val documentserveritem = HOAppDetDocumentServerPojo(item.id.toString(), item.documentName)
                    documentList.add(documentserveritem)
                }
            }

            for (item in HoAppDetailsHome.hoappviewRES?.data?.approvals!!) {
                log("check", item?.applicationStatus)
                val approvals = HOApprovalsserverPojo(item?.id.toString(), item?.applicationStatus)
                approvalsList.add(approvals)
            }


            val hashMap = HashMap<String, Any>()
            hashMap["user_id"] = SessionManager.getUserId(activity).toInt()
            hashMap["role"] = SessionManager.getuserSopeID(activity).toInt()
            hashMap["block"] = SessionManager.getBlockId(activity).toInt()
            hashMap["district"] = SessionManager.getdistrictID(activity).toInt()
            hashMap["remarks"] = ho_inspectionbottom_remarks_edt?.text.toString()
            hashMap["application_id"] = HoAppDetailsHome.hoappviewRES?.data?.applications?.id!!
            hashMap["approvals"] = approvalsList
            hashMap["documents"] = documentList
            hashMap["name"] = inspectWIthList
            hashMap["particulars"] = inspectPROJECTList
            val inspectionpartlist = ArrayList<MultipartBody.Part>()
            val invoicepartlist = ArrayList<MultipartBody.Part>()


            if (invoicefileList!=null && invoicefileList?.size != 0) {

                for (item in invoicefileList!!) {
                    val invoicerequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), item!!)
                    invoicepartlist.add(MultipartBody.Part.createFormData("invoice[]", item.name, invoicerequestBody))
                }


            } else {
                val invoicerequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "")
                invoicepartlist.add(MultipartBody.Part.createFormData("invoice[]", "", invoicerequestBody))
            }

            if (invoicefileList!=null && inspectionfileList?.size != 0) {

                for (item in inspectionfileList!!) {
                    val inspectionImage = RequestBody.create(MediaType.parse("multipart/form-data"), item)
                    inspectionpartlist?.add(
                        MultipartBody.Part.createFormData(
                            "inspectionimage[]",
                            item.name,
                            inspectionImage
                        )
                    )
                }


            } else {
                val inspectionImage = RequestBody.create(MediaType.parse("multipart/form-data"), "")
                inspectionpartlist.add(MultipartBody.Part.createFormData("inspectionimage[]", "", inspectionImage))
            }


            val service = Utils.getInstance(activity)
            val callback = service.call_twoImageArray(
                "Ahoapproval", "Bearer " +
                        SessionManager.getToken(activity), hashMap, invoicepartlist, inspectionpartlist
            )
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    MessageUtils.showSnackBar(activity, ho_inspection, msg)

                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {

                        if (response.isSuccessful) {

                            val obj = JSONObject(response.body()?.string())
                            val success = obj.getBoolean("success")
                            val message = obj.getString("message")
                            if (success) {

                                MessageUtils.showToastMessage(activity, message)
                                FragmentCallUtils.passFragmentWithoutBackStatck(
                                    activity,
                                    HoPriDashboard(), R.id.aho_container_body
                                )

                            } else {

                                MessageUtils.showSnackBar(activity, ho_inspection, message)
                            }


                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            MessageUtils.showSnackBar(activity, ho_inspection, msg)
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

            })


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun validationfowith(name: String, designation: String, remarks: String): Boolean {

        try {

            when {
                name.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, ho_inspection, "Name Can't Be Empty")
                    return false
                }
                designation.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, ho_inspection, "Designation Can't Be Empty")
                    return false
                }
                remarks.isEmpty() -> {
                    MessageUtils.showSnackBar(activity, ho_inspection, "Remarks Can't Be Empty")
                    return false
                }

            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return true
    }


    override fun OnclickPostionEvent(name: String, postion: String) {
        try {

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }


    private fun getruntimepermissionforMedia(i: Int) {
        try {
            if (
                ContextCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED

            ) {

                val dialog = Dialog(activity)
                dialog.setContentView(R.layout.dialog_permissions)
                dialog.setCancelable(false)
                dialog.window.setBackgroundDrawableResource(R.color.dialog_tans)
                dialog.window.setGravity(Gravity.BOTTOM)
                dialog.show()
                dialog.window.setWindowAnimations(R.style.UpDownDialogAnim)
                dialog.btn_grand.visibility = View.VISIBLE
                dialog.permission_title.text = "Permission Required"
                dialog.permission_message.text = "CAMERA\n" +
                        "The App needs access to the camera to click your land Information\n" +
                        "\n" +
                        "STORAGE\n" +
                        "The App accesses to your document verification"

                dialog.btn_dismiss.setOnClickListener {
                    dialog.dismiss()
//                        token?.cancelPermissionRequest()
                }
                dialog.btn_grand.setOnClickListener {
                    dialog.dismiss()
                    validatePermissions(1, i)
                }

            } else {
                val intent = Intent()
                intent.type = "*/*"
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Files"), i)

            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun validatePermissions(isShow: Int, i: Int) {
        Dexter.withActivity(activity)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (isShow == 1 && report?.areAllPermissionsGranted()!!) {
                        val intent = Intent()
                        intent.type = "*/*"
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                        intent.action = Intent.ACTION_GET_CONTENT
                        startActivityForResult(Intent.createChooser(intent, "Select Files"), i)

                    }

                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?, token: PermissionToken?
                ) {
                    log("permissions", "denied")
                    token?.continuePermissionRequest()
                }

            })
            .check()

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        log("selectFile", "" + requestCode.toString())
        try {

            if (requestCode == 7 && resultCode == Activity.RESULT_OK && data != null) {

                invoicefileList = ArrayList()

                if (data.data != null) {
                    invoicefileList?.clear()
                    val file = CompressFile.getCompressedImageFile(
                        File(
                            PathUtil.getPath(
                                activity,
                                data.data
                            )
                        ), activity
                    )

                    val fileSize = file?.length()!! / 1024
                    if (fileSize <= 300) {
                        invoicefileList?.add(file)
                        log("InVoiceIMAge SIze", "" + fileSize)
                    } else {
                        MessageUtils.showToastMessage(activity, "File Too Large  Below 300 KB")
                    }

                }
                else {
                    if (data.clipData != null) {
                        invoicefileList?.clear()
                        try {
                            for (i: Int in 0 until data.clipData.itemCount) {

                                val file = CompressFile.getCompressedImageFile(
                                    File(
                                        PathUtil.getPath(
                                            activity,
                                            data.clipData.getItemAt(i).uri
                                        )
                                    ), activity
                                )

                                val fileSize = file?.length()!! / 1024
                                if (fileSize <= 300) {
                                    invoicefileList?.add(file)
                                    log("InVoiceIMAge SIze", "" + fileSize)
                                }
                            }


                        } catch (ex: Exception) {
                            ex.printStackTrace()

                        }
                    }
                }
                setToADpte(invoicefileList!!, 1)

            }
            else if (requestCode == 8 && resultCode == Activity.RESULT_OK && data != null) {
                inspectionfileList = ArrayList()
                if (data.data != null) {
                    inspectionfileList?.clear()
                    val file = CompressFile.getCompressedImageFile(
                        File(
                            PathUtil.getPath(
                                activity,
                                data.data
                            )
                        ), activity
                    )

                    val fileSize = file?.length()!! / 1024
                    if (fileSize <= 300) {
                        log("InspectionIMAge SIze", "" + fileSize)
                        inspectionfileList?.add(file)
                    } else {
                        MessageUtils.showToastMessage(activity, "File Too Large  Below 300 KB")
                    }

                }
                else {
                    if (data.clipData != null) {
                        inspectionfileList?.clear()
                        try {
                            for (i: Int in 0 until data.clipData.itemCount) {

                                val file = CompressFile.getCompressedImageFile(
                                    File(
                                        PathUtil.getPath(
                                            activity,
                                            data.clipData.getItemAt(i).uri
                                        )
                                    ), activity
                                )

                                val fileSize = file?.length()!! / 1024
                                if (fileSize <= 300) {
                                    log("InspectionIMAge SIze", "" + fileSize)
                                    inspectionfileList?.add(file)
                                }
                            }

                        } catch (ex: Exception) {
                            ex.printStackTrace()

                        }
                    }
                }
                setToADpte(inspectionfileList!!, 2)

            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setToADpte(fileList: ArrayList<File>, i: Int) {
        try {
            log("HOSelectedImage", "" + i + "\t" + fileList.size)
            val adapter = HOINspectionImageAdpter(activity!!, fileList, this)
            if (i == 1) {
                recy_ho_invoce_image_list?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
                recy_ho_invoce_image_list?.adapter = adapter
            } else {
                recy_ho_inspection_image_list?.layoutManager =
                        LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
                recy_ho_inspection_image_list?.adapter = adapter
            }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

}
