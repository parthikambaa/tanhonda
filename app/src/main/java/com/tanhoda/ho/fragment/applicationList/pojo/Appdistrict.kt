package com.tanhoda.ho.fragment.applicationList.pojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Appdistrict(

	@field:SerializedName("office_address")
	val officeAddress: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: Any? = null,

	@field:SerializedName("district_code")
	val districtCode: Any? = null,

	@field:SerializedName("district")
	val district: String? = null,

	@field:SerializedName("active")
	val active: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: Any? = null,

	@field:SerializedName("district_tamil")
	val districtTamil: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("state_id")
	val stateId: Int? = null
)