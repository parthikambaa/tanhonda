package com.tanhoda.ho.fragment.profiles


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson

import com.tanhoda.R
import com.tanhoda.ho.HOActivity
import com.tanhoda.ho.fragment.profiles.pojo.ProfileRES
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_ho_profiles.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HoProfiles : Fragment() {


    var dialog: Dialog? = null
    var profilesRes: ProfileRES?=null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as HOActivity).disableNavigationInAdoptionHome(getString(R.string.profile))
        return inflater.inflate(R.layout.fragment_ho_profiles, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /**
         * get ProfilesDetails
         */
        when{
            Utils.haveNetworkConnection(activity)-> {
                try{
                    getProfilesRES()
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
            else -> {
                ho_profile_empty_List?.visibility =View.VISIBLE
                ho_profile_empty_List?.text =getString(R.string.check_internet)
                MessageUtils.showSnackBar(activity,prof_snack_view_ho,getString(R.string.check_internet))
            }

        }
    }

    private fun getProfilesRES() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["userid"] = SessionManager.getUserId(activity)
            val calinterface = Utils.getInstance(activity)
            val callback = calinterface.call_post("profile", "Bearer " + SessionManager.getToken(activity), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    MessageUtils.showSnackBar(activity, prof_snack_view_ho, msg)
                    ho_profile_empty_List?.visibility =View.VISIBLE
                    ho_profile_empty_List?.text =msg
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            profilesRes = Gson().fromJson(response.body()?.string(), ProfileRES::class.java)
                            if (profilesRes!=null) {
                                if(profilesRes?.success!!){
                                    if(profilesRes?.data!=null){
                                        setTOFormdata()
                                        ho_profile_empty_List?.visibility =View.GONE

                                    }else{
                                        ho_profile_empty_List?.visibility =View.VISIBLE
                                        MessageUtils.showSnackBar(activity, prof_snack_view_ho,profilesRes?.message)
                                    }

                                }else{
                                    MessageUtils.showSnackBar(activity, prof_snack_view_ho,profilesRes?.message)
                                }
                            } else {
                                MessageUtils.showSnackBar(activity, prof_snack_view_ho, getString(R.string.checkjsonformat))
                            }

                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            MessageUtils.showSnackBar(activity, prof_snack_view_ho, msg)
                            ho_profile_empty_List?.visibility =View.VISIBLE
                            ho_profile_empty_List?.text =msg
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setTOFormdata() {
            try{

            ho_profile_name?.setText(profilesRes?.data?.users?.name)
            ho_profile_block?.setText(profilesRes?.data?.users?.block?.block)
            ho_profile_district?.setText(profilesRes?.data?.users?.district?.district)
            ho_profile_email?.setText(profilesRes?.data?.users?.email)
            ho_profile_mobile?.setText(profilesRes?.data?.users?.contactNo)
            ho_profile_role?.setText(profilesRes?.data?.users?.role?.userScope)
            ho_profile_state?.setText(profilesRes?.data?.users?.state?.state)

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
}
