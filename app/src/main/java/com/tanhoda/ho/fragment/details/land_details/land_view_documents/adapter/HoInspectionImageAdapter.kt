package com.tanhoda.ho.fragment.details.land_details.land_view_documents.adapter

import android.support.constraint.ConstraintLayout
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.tanhoda.R
import com.tanhoda.ho.fragment.details.viewappstatusPojo.InspectionsImages
import com.tanhoda.interfaces.onViewImages
import com.tanhoda.utils.ImageUtils
import com.tanhoda.utils.Utils
import customs.CustomTextView

class HoInspectionImageAdapter(
    val context: FragmentActivity,
    val applicationArrayList: ArrayList<InspectionsImages?>?,
    val onclickEvent: onViewImages
) :
    RecyclerView.Adapter<HoInspectionImageAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HoInspectionImageAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.adapter_ho_app_land_view_document,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return applicationArrayList?.size!!
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            if (applicationArrayList!![position]?.image != null) {
                ImageUtils.displayImageFromUrl(
                    context,
                    Utils.IMG_INSPECTION_URL + applicationArrayList[position]?.image.toString(), holder.image
                )
            }
            if (applicationArrayList[position]?.type != null) {
                holder.imagename?.text = applicationArrayList[position]?.type
            }

            holder.item_document_layout?.setOnClickListener {
                onclickEvent.onLoadData(
                    applicationArrayList[position]?.image.toString(),
                    applicationArrayList[position]?.type.toString(), "ins"
                )
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val image = itemView?.findViewById<ImageView>(R.id.img_ho_app_land_view_document_adp)
        val imagename = itemView?.findViewById<CustomTextView>(R.id.text_ho_app_land_view_document_adp)
        val item_document_layout = itemView?.findViewById<ConstraintLayout>(R.id.item_document_layout)


    }
}