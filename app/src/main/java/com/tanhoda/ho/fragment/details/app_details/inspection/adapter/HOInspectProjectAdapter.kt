package com.tanhoda.ho.fragment.details.app_details.inspection.adapter
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.tanhoda.R
import com.tanhoda.ho.fragment.details.app_details.inspection.pojo.HOInspectprojectpojo
import com.tanhoda.interfaces.OnclickPostionEvent
import customs.CustomTextEditView

class HOInspectProjectAdapter(
    val activity: FragmentActivity,
    val inspectionArrayList: ArrayList<HOInspectprojectpojo>,
    val onclickPostionEvent: OnclickPostionEvent
) : RecyclerView.Adapter<HOInspectProjectAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HOInspectProjectAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(activity).inflate(R.layout.adapter_ho_inspect_project, parent, false))
    }

    override fun getItemCount(): Int {
        return inspectionArrayList.size
    }

    override fun onBindViewHolder(holder: HOInspectProjectAdapter.ViewHolder, position: Int) {
        try {
            holder.projectparticulars?.setText( inspectionArrayList[position].projectparticulars)
            holder.projectspecification?.setText( inspectionArrayList[position].projectspec)
            holder.projectcapaciy?.setText( inspectionArrayList[position].projectcapacity)
            holder.projectquantity?.setText(inspectionArrayList[position].projectqty)
            holder.projectamount?.setText(inspectionArrayList[position].projectamount)

            holder.inspectionspecification?.setText( inspectionArrayList[position].inspectionspec)
            holder.inspectioncapaciy?.setText( inspectionArrayList[position].inspectioncapacity)
            holder.inspectionquantity?.setText( inspectionArrayList[position].inspectionqty)
            holder.inspectionamount?.setText(inspectionArrayList[position].inspectionqty)
            holder.inspectionremarks?.setText(inspectionArrayList[position].inspectionremarks)

            holder.remove?.setOnClickListener {
                try{
                    inspectionArrayList.removeAt(position)
                    notifyDataSetChanged()
                }catch (ex:Exception){ex.printStackTrace()}
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {

        var  projectspecification   =itemView?.findViewById<CustomTextEditView>(R.id.ho_inspect_project_specification_txt_adp)
        var  projectcapaciy   =itemView?.findViewById<CustomTextEditView>(R.id.ho_inspect_project_capacity_txt_adp)
        var  projectquantity   =itemView?.findViewById<CustomTextEditView>(R.id.ho_inspect_project_quantity_txt_adp)
        var  projectamount   =itemView?.findViewById<CustomTextEditView>(R.id.ho_inspect_project_amount_txt_adp)
        var  projectparticulars   =itemView?.findViewById<CustomTextEditView>(R.id.ho_inspect_project_particulars_txt_adp)

        var  inspectionspecification   =itemView?.findViewById<CustomTextEditView>(R.id.ho_inspect_act_specification_txt_adp)
        var  inspectioncapaciy   =itemView?.findViewById<CustomTextEditView>(R.id.ho_inspect_act_capacity_txt_adp)
        var  inspectionquantity   =itemView?.findViewById<CustomTextEditView>(R.id.ho_inspect_act_quantity_txt_adp)
        var  inspectionamount   =itemView?.findViewById<CustomTextEditView>(R.id.ho_inspect_act_amount_txt_adp)
        var  inspectionremarks   =itemView?.findViewById<CustomTextEditView>(R.id.ho_inspect_act_remarks_txt_adp)

        var  remove   =itemView?.findViewById<ImageButton>(R.id.ho_inspect_project_removemore_imgbtn_adp)

    }
}