package com.tanhoda.ho.fragment.schemes.hoschemespojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("schemes")
	val schemes: ArrayList<SchemesItem?>? = null
)