package com.tanhoda.ho.fragment.details.farmer_details


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tanhoda.R
import com.tanhoda.ho.fragment.details.HoAppDetailsHome
import kotlinx.android.synthetic.main.fragment_ho_farmer_details.*


class HoFarmerDetails : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ho_farmer_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        try {
            setTOFormappData()

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setTOFormappData() {
        try {
            val data = HoAppDetailsHome.hoappviewRES?.data?.applications?.appfarmer!!


            if(data.name!=null){
                custxtviw_ho_far_name.setText(data.name)
            }
            if(data.gname!=null){
                custxtviw_ho_far_farthername.setText(data.gname)
            }
            if(data.farmerType!=null){
                custxtviw_ho_far_fartype.setText(data.farmerType)
            }
            if(data.socialStatus!=null){
                custxtviw_ho_far_social.setText(data.socialStatus)
            }
            if(data.houseNo!=null &&  data.street!=null && data.habitation!=null && data.pincode!=null){
                custxtviw_ho_far_address.setText(data.houseNo+"\n"+data.street+"\n"+data.habitation+"\n"+data.pincode)
            }
            if(data.mobileNumber!=null){
                custxtviw_ho_far_mobile.setText(data.mobileNumber)
            }
            if(data.gender!=null){
                custxtviw_ho_far_gender.setText(data.gender)
            }
            if(data.age!=null){
                custxtviw_ho_far_age.setText(data.age)
            }
            if(data.income!=null){
                custxtviw_ho_far_income.setText(data.income)
            }
            if(data.aadhaarId!=null){
                custxtviw_ho_far_aadhar_no.setText(data.aadhaarId)
            }
            if(data.bankAccountNo!=null){
                custxtviw_ho_far_account_no.setText(data.bankAccountNo)
            }
            if(data.ifscCode!=null){
                custxtviw_ho_far_ifsc_code.setText(data.ifscCode)
            }
            if(data.bankName!=null){
                custxtviw_ho_far_bankname.setText(data.bankName)
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

}
