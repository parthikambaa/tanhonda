package com.tanhoda.ho.fragment.sub_component


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.gson.Gson

import com.tanhoda.R
import com.tanhoda.ho.HOActivity
import com.tanhoda.ho.fragment.sub_component.adapter.HOComponetAdpter
import com.tanhoda.ho.fragment.sub_component.pojo.HOSUBComponentRES
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.login_splash_screen.SplashActivity
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_ho_sub_component.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HOSUBComponent : Fragment(), OnclickPostionEvent {
    var dialog: Dialog? = null
    var componentRES: HOSUBComponentRES? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as HOActivity).disableNavigationInAdoptionHome(getString(R.string.sub_component))
        return inflater.inflate(R.layout.fragment_ho_sub_component, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)
        /**
         * getcomponentList()
         */
        when {
            Utils.haveNetworkConnection(activity) -> {
                getComponent()
            }
            else -> {
                ho_compo_empty_list?.visibility = View.VISIBLE
                ho_compo_empty_list?.text = getString(R.string.check_internet)
                SplashActivity.snackBar = MessageUtils.showSnackBarAction(
                    activity,
                    ho_component_snackview,
                    getString(R.string.check_internet)
                )
            }
        }
    }

    private fun getComponent() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["user_id"] = SessionManager.getUserId(activity)
//            map["user_id"] = 8
            map["district"] = SessionManager.getdistrictID(activity)
//            map["district"] =25
            val service = Utils.getInstance(activity)
            val callback = service.call_post("components", "Bearer " + SessionManager.getToken(activity), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    SplashActivity.snackBar = MessageUtils.showSnackBar(activity, ho_component_snackview, msg)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            componentRES = Gson().fromJson(response.body()?.string(), HOSUBComponentRES::class.java)
                            if (componentRES != null) {
                                if (componentRES?.success!!) {
                                    if (componentRES?.data != null && componentRES?.data?.subcomponent?.size != 0) {
                                        setToadapter()
                                        recy_ho_compo_list?.visibility = View.VISIBLE
                                        ho_compo_list_title_linear?.visibility = View.GONE
                                        ho_compo_empty_list?.visibility = View.GONE
                                    } else {
                                        ho_compo_list_title_linear?.visibility = View.GONE
                                        ho_compo_empty_list?.visibility = View.VISIBLE
                                        recy_ho_compo_list?.visibility = View.GONE
                                    }
                                } else {
                                    SplashActivity.snackBar = MessageUtils.showSnackBar(
                                        activity,
                                        ho_component_snackview,
                                        componentRES?.message
                                    )
                                }
                            } else {
                                SplashActivity.snackBar = MessageUtils.showSnackBar(
                                    activity,
                                    ho_component_snackview,
                                    getString(R.string.checkjsonformat)
                                )
                            }

                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            SplashActivity.snackBar = MessageUtils.showSnackBar(activity, ho_component_snackview, msg)
                            ho_compo_empty_list?.visibility = View.VISIBLE
                            ho_compo_empty_list?.text = msg
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setToadapter() {
        try {
            val componetAdpter = HOComponetAdpter(activity!!, componentRES?.data?.subcomponent, this)
            recy_ho_compo_list?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            recy_ho_compo_list?.adapter = componetAdpter
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {
        try {

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}
