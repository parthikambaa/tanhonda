package com.tanhoda.ho.fragment.details.viewappstatusPojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Generated("com.robohorse.robopojogenerator")
data class Approveuser(

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("active")
	val active: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("email_verified_at")
	val emailVerifiedAt: Any? = null,

	@field:SerializedName("user_scope_id")
	val userScopeId: Int? = null,

	@field:SerializedName("block_id")
	val blockId: Int? = null,

	@field:SerializedName("userkey")
	val userkey: String? = null,

	@field:SerializedName("user_type")
	val userType: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("contact_no")
	val contactNo: String? = null,

	@field:SerializedName("scheme_id")
	val schemeId: Any? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("state_id")
	val stateId: Int? = null,

	@field:SerializedName("district_id")
	val districtId: Int? = null,

	@field:SerializedName("email")
	val email: String? = null
): Serializable