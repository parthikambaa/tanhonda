package com.tanhoda.ho.fragment.dashboard.ho_pri_dashboard_pojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Block(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("blockcount")
	val blockcount: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)