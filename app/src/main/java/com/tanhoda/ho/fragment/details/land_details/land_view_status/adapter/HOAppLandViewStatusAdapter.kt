package com.tanhoda.ho.fragment.details.land_details.land_view_status.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.tanhoda.R
import com.tanhoda.ho.fragment.details.viewappstatusPojo.StatusupdatesItem
import com.tanhoda.utils.DatetimeUtils
import com.tanhoda.utils.Utils
import customs.CustomTextView

class HOAppLandViewStatusAdapter(
    val context: Context,
    val applicationArrayList: ArrayList<StatusupdatesItem?>?

) : RecyclerView.Adapter<HOAppLandViewStatusAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.adapter_ho_app_land_view_status,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return applicationArrayList?.size!!
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.siNo?.text = (position + 1).toString()
            /*holder.status?.text = applicationArrayList!![position]?.status
            holder.status?.setSingleLine()
            holder.status?.ellipsize = TextUtils.TruncateAt.MARQUEE
            holder.status?.isSelected = true*/


            if (applicationArrayList!![position]?.status.toString().toLowerCase().equals(Utils.APPROVED.toLowerCase())) {
                holder.status!!.setImageResource(R.drawable.vector_drawable_accept_sign)
            } else if (applicationArrayList[position]?.status.toString().toLowerCase().equals(Utils.PENDING.toLowerCase())) {
                holder.status!!.setImageResource(R.drawable.vector_drawable_pending)
            } else if (applicationArrayList[position]?.status.toString().toLowerCase().equals(Utils.OH_HOLD.toLowerCase())) {
                holder.status!!.setImageResource(R.drawable.ic_hold)
            } else {
                holder.status!!.setImageResource(R.drawable.vector_drawable_reject_sign)
            }

            if (applicationArrayList!![position]?.approvedDate != null) {
                holder.date?.text =
                    DatetimeUtils.convertDates(applicationArrayList[position]?.approvedDate, "yyyy-mm-dd", "dd-mm-yy")
            } else {
                holder.date?.text = "-"
            }

            holder.date?.setSingleLine()
            holder.date?.ellipsize = TextUtils.TruncateAt.MARQUEE
            holder.date?.isSelected = true
            if (applicationArrayList!![position]?.approveuser != null) {
                holder.approvedBy?.text = applicationArrayList!![position]?.approveuser?.name
            } else {
                holder.approvedBy?.text = "-"
            }
            holder.approvedBy?.setSingleLine()
            holder.approvedBy?.ellipsize = TextUtils.TruncateAt.MARQUEE
            holder.approvedBy?.isSelected = true
            holder.stages?.text = applicationArrayList!![position]?.applicationStatus
            holder.stages?.setSingleLine()
            holder.stages?.ellipsize = TextUtils.TruncateAt.MARQUEE
            holder.stages?.isSelected = true


        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val siNo = itemView?.findViewById<CustomTextView>(R.id.txt_sino_ho_app_land_view_status_adap)
        val stages = itemView?.findViewById<CustomTextView>(R.id.txt_statges_ho_app_land_view_status_adap)
        val status = itemView?.findViewById<ImageView>(R.id.txt_status_ho_app_land_view_status_adap)
        val date = itemView?.findViewById<CustomTextView>(R.id.txt_date_ho_app_land_view_status_adap)
        val approvedBy = itemView?.findViewById<CustomTextView>(R.id.txt_approve_by_ho_app_land_view_status_adap)
        val layoutCompat = itemView?.findViewById<LinearLayout>(R.id.ho_app_land_view_status_adap_linear)

    }
}