package com.tanhoda.ho

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Spannable
import android.text.SpannableString
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.tanhoda.R
import com.tanhoda.ho.fragment.component.HOComponent
import com.tanhoda.ho.fragment.dashboard.HoPriDashboard
import com.tanhoda.ho.fragment.profiles.HoProfiles
import com.tanhoda.ho.fragment.schemes.HOShems
import com.tanhoda.ho.fragment.sub_component.HOSUBComponent
import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import customs.CustomTypefaceSpan
import customs.Fonts
import kotlinx.android.synthetic.main.aho_activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*


class HOActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var toggle: ActionBarDrawerToggle
    var doubleBackToExitPressedOnce = false
    var logoutdiabuil: AlertDialog.Builder? = null
    var logoutdialog: AlertDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.aho_activity_main)
        setSupportActionBar(toolbar)
        logoutdiabuil = AlertDialog.Builder(this)


        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        nav_view.getMenu().clear()
        nav_view.inflateMenu(R.menu.aho_main_drawer)

//        disableNavigationInAdoptionHome("HOActivty")


        try {
            FragmentCallUtils.passFragmentWithoutBackStatck(this, HoPriDashboard(), R.id.aho_container_body)
            nav_view?.getHeaderView(0)?.findViewById<TextView>(R.id.aho_nav_useremail)?.text =
                SessionManager.getEmailId(this)
            nav_view?.getHeaderView(0)?.findViewById<TextView>(R.id.aho_nav_username)?.text =
                SessionManager.getName(this)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    fun enableToogle(titleStr: String) {
        this.title = null
        app_bar_title.text = titleStr
        setFontTypeface()
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        supportActionBar!!.show()
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        supportActionBar!!.setHomeButtonEnabled(true)
        toggle?.isDrawerIndicatorEnabled = true
        nav_view.menu.getItem(0).isChecked = true


    }

    fun disableNavigationInAdoptionHome(titleStr: String) {
        this.title = null
        app_bar_title.text = titleStr
        setFontTypeface()
        supportActionBar!!.show()
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        toggle?.isDrawerIndicatorEnabled = false
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(false)
        toggle?.toolbarNavigationClickListener = View.OnClickListener { onBackPressed() }
    }

    private fun setFontTypeface() {
        val m = nav_view.menu
        for (i in 0 until m.size()) {
            val mi = m.getItem(i)
            //for aapplying a font to subMenu ...
            val subMenu = mi.subMenu
            if (subMenu != null && subMenu.size() > 0) {
                for (j in 0 until subMenu.size()) {
                    val subMenuItem = subMenu.getItem(j)
                    applyFontToMenuItem(subMenuItem)
                }
            }
            applyFontToMenuItem(mi)
        }
    }

    private fun applyFontToMenuItem(mi: MenuItem) {
        val font: Typeface = Typeface.createFromAsset(assets, Fonts.REGULAR)

        val mNewTitle = SpannableString(mi.title)
        mNewTitle.setSpan(CustomTypefaceSpan("", font), 0, mNewTitle.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        mi.title = mNewTitle
    }


    override fun onBackPressed() {
        val ff = supportFragmentManager.findFragmentById(R.id.aho_container_body)
        val count = supportFragmentManager.backStackEntryCount
        when (ff) {
            is HoPriDashboard -> {
                if (drawer_layout?.isDrawerOpen(Gravity.START)!!) {
                    drawer_layout?.closeDrawer(Gravity.START)
                } else {
                    checkBackPress()
                }


            }
            else -> super.onBackPressed()
        }
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_profile -> {
                // Handle the camera action
                FragmentCallUtils.passFragment(this, HoProfiles(), R.id.aho_container_body)
            }
            R.id.nav_logout -> {
                // Handle the logout action
                Utils.logoutDialog(this, nav_view)
            }
            R.id.nav_category -> {
                FragmentCallUtils.passFragment(
                    this,
                    HOComponent(), R.id.aho_container_body
                )
            }
            R.id.nav_component -> {
                FragmentCallUtils.passFragment(
                    this,
                    HOSUBComponent(), R.id.aho_container_body
                )
            }
            R.id.nav_schemes -> {
                FragmentCallUtils.passFragment(
                    this,
                    HOShems(), R.id.aho_container_body
                )
            }
            R.id.nav_dashboard -> {
                FragmentCallUtils.passFragmentWithoutBackStatckAnaAnim(
                    this,
                    HoPriDashboard(), R.id.aho_container_body
                )
            } R.id.nav_privacy_policy -> {
            Utils.callWebURL(this@HOActivity, Utils.PRIVACY_POLICY)
        }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    fun checkBackPress() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            finish()

        }

        doubleBackToExitPressedOnce = true

        Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show()
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }


    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        try {
            val view = currentFocus
            if (view != null && (ev.action == MotionEvent.ACTION_UP || ev.action == MotionEvent.ACTION_MOVE) && view is EditText && !view.javaClass.name.startsWith(
                    "android.webkit."
                )
            ) {
                val scrcoords = IntArray(2)
                view.getLocationOnScreen(scrcoords)
                val x = ev.rawX + view.left - scrcoords[0]
                val y = ev.rawY + view.top - scrcoords[1]
                if (x < view.left || x > view.right || y < view.top || y > view.bottom)
                    (this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                        this.window.decorView.applicationWindowToken,
                        0
                    )
            }
        } catch (e: RuntimeException) {
            e.printStackTrace()
        }

        return super.dispatchTouchEvent(ev)
    }

    override fun onDestroy() {
        super.onDestroy()
        val ff = supportFragmentManager.findFragmentById(R.id.aho_container_body)

        if (ff is HoPriDashboard) {
            finish()
        }
    }


}
