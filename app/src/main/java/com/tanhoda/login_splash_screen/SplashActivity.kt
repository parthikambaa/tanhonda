package com.tanhoda.login_splash_screen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AnimationUtils
import com.tanhoda.R
import com.tanhoda.adh.ADHActivity
import com.tanhoda.aho.AHOActivity
import com.tanhoda.dd.DDActivity
import com.tanhoda.ho.HOActivity
import com.tanhoda.login_splash_screen.login.Login
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.splash_screen.*


class SplashActivity : AppCompatActivity() {
    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable

    private lateinit var mHandlerRocket: Handler
    private lateinit var mRunnableRocket: Runnable

    companion object {

        var snackBar: Snackbar? = null
    }

    var sessionManager: SessionManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.getWindow()
            .setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        supportActionBar?.hide()
        setContentView(R.layout.splash_screen)

        sessionManager = SessionManager(this)

        image_logo.visibility = View.VISIBLE

        mHandler = Handler()
        mHandlerRocket = Handler()
        val top = AnimationUtils.loadAnimation(this, R.anim.anim_fade_in)
        image_logo?.startAnimation(top)
    }


    override fun onResume() {
        super.onResume()
        try {


            mRunnable = Runnable {

                checkLogins()
            }

            mRunnableRocket = Runnable {

                val top = AnimationUtils.loadAnimation(this, R.anim.anim_up_out)
                image_logo?.startAnimation(top)

            }
            if (Utils.haveNetworkConnection(this)) {
                mHandler.postDelayed(
                    mRunnable, // Runnable
                    4000 // Delay in milliseconds
                )


                mHandlerRocket.postDelayed(
                    mRunnableRocket, // Runnable
                    3800 // Delay in milliseconds
                )


            } else {
                MessageUtils.showNetworkDialog(this)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        if (mHandler != null && mRunnable != null) {
            mHandler.removeCallbacks(mRunnable)
        }
    }

    override fun onPause() {
        super.onPause()
        if (mHandler != null && mRunnable != null) {
            mHandler.removeCallbacks(mRunnable)
        }
    }

    private fun checkLogins() {


        if (sessionManager?.isLoggedIn!!) {
            when {
                SessionManager.getUserType(this).equals("AHO") -> {
                    startActivity(
                        Intent(
                            this, AHOActivity::class.java
                        ).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    )
                    finish()
                }
                SessionManager.getUserType(this).equals("HO") -> {
                    startActivity(
                        Intent(
                            this,
                            HOActivity::class.java
                        ).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    )
                    finish()
                }
                SessionManager.getUserType(this).equals("ADH") -> {

                    startActivity(
                        Intent(
                            this,
                            ADHActivity::class.java
                        ).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    )
                    finish()
                }
                SessionManager.getUserType(this).equals("DD") -> {
                    startActivity(
                        Intent(
                            this,
                            DDActivity::class.java
                        ).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    )
                    finish()

                }
                else -> {
                    startActivity(Intent(this, Login::class.java))
                    finish()

                }
            }
            overridePendingTransition(0, 0)
        } else {
            startActivity(Intent(this, Login::class.java))
            finish()

        }


    }
}
