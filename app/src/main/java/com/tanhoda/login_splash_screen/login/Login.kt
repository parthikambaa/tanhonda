package com.tanhoda.login_splash_screen.login


import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.util.Patterns
import android.view.*
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.tanhoda.R
import com.tanhoda.adh.ADHActivity
import com.tanhoda.aho.AHOActivity
import com.tanhoda.dd.DDActivity
import com.tanhoda.ho.HOActivity
import com.tanhoda.login_splash_screen.SplashActivity.Companion.snackBar
import com.tanhoda.login_splash_screen.login.pojo.Data
import com.tanhoda.login_splash_screen.login.pojo.LoginRES
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import customs.CustomTextEditView
import customs.CustomTextView
import kotlinx.android.synthetic.main.fragment_login.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Pattern


class Login : AppCompatActivity() {

    var dialog: Dialog? = null
    var loginRES: LoginRES? = null
    var sessionManager: SessionManager? = null


/*
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.getWindow()
            .setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.fragment_login)
        sessionManager = SessionManager(this)
        dialog = Dialog(this)
        val top = AnimationUtils.loadAnimation(this, R.anim.anim_up)
        logo_img?.startAnimation(top)

        forgot_password?.setOnClickListener {
            val popupdialog = Dialog(this@Login)
            popupdialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            popupdialog?.setContentView(R.layout.dialog_remarksfr_reject)
            popupdialog?.setCancelable(false)
            popupdialog?.window?.setBackgroundDrawableResource(R.color.dialog_trans)
            popupdialog?.window?.setGravity(Gravity.BOTTOM)
            popupdialog?.show()
            popupdialog?.window?.setWindowAnimations(R.style.UpDownDialogAnim)
            val popuptitle = popupdialog.findViewById<CustomTextView>(R.id.aho_app_det_title_diag)
            val popupcancel = popupdialog.findViewById<CustomTextView>(R.id.btn_dismiss)
            val popupsubmit = popupdialog.findViewById<CustomTextView>(R.id.btn_grand)
            val popupremarks = popupdialog.findViewById<CustomTextEditView>(R.id.aho_app_det_remarks_dig)
            popupremarks.hint = resources.getString(R.string.email)
            popupsubmit?.text = resources.getString(R.string.submit)
            popupsubmit?.visibility = View.VISIBLE
            popuptitle.setText(resources.getString(R.string.forgot_password))

            popupcancel.setOnClickListener {
                val email = popupremarks.text.toString()
                Log.d("sdsdsd", "" + Patterns.EMAIL_ADDRESS.pattern())
                if (!email.isNotEmpty()) {
                    if (Pattern.matches(Patterns.EMAIL_ADDRESS.pattern(), email)) {
                        callForgotAPI(popupdialog, email)

                    } else {
                        snackBar =
                            MessageUtils.showSnackBar(
                                this@Login,
                                rel_lay_snackView_login,
                                getString(R.string.enter_valid_email_id)
                            )
                    }
                } else {
                    snackBar = MessageUtils.showSnackBar(
                        this@Login,
                        rel_lay_snackView_login,
                        getString(R.string.email_id_missing)
                    )
                }
            }

        }

        loginbtn?.setOnClickListener {
            try {
                val username = login_username?.text.toString()
                val password = login_password?.text.toString()

                if (validateformat(username, password)) {
                    /*  if(username.equals("8489419417")){
                          startActivity(Intent(activity, AHOActivity::class.java))
                      }else if(username.equals("8489419418")) {
                          startthis(Intent(activity, HOActivity::class.java))
                      }else if(username.equals("8489419419")) {
                          startActivity(Intent(activity, ADHActivity::class.java))
                      }else{
                          startActivity(Intent(activity, DDActivity::class.java))
                      }*/

                    if (Utils.haveNetworkConnection(this)) {

                        callApi()
                    } else {
                        MessageUtils.showNetworkDialog(this)
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }


    }

    private fun callForgotAPI(popupdialog: Dialog, email: String) {
        val map = HashMap<String, Any>()
        val dialog = MessageUtils.showDialog(this@Login)
        map["email"] = email
        Utils.getInstance(this@Login).call_post("category", "Bearer " + SessionManager.getToken(this@Login), map)
            .enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                    MessageUtils.dismissDialog(dialog)
                    val message = MessageUtils.setFailurerMessage(this@Login, t.message)
                    snackBar = MessageUtils.showSnackBar(this@Login, rel_lay_snackView_login, message)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            val modelForgotPassword =
                                Gson().fromJson(response.body()?.string(), ModelForgotPassword::class.java)
                            if (modelForgotPassword.success!!) {
                                MessageUtils.dismissDialog(popupdialog)
                                MessageUtils.showToastMessageLong(this@Login, modelForgotPassword.message)

                            } else {
                                snackBar = MessageUtils.showSnackBar(
                                    this@Login,
                                    rel_lay_snackView_login,
                                    modelForgotPassword.message
                                )
                            }
                        } else {
                            snackBar = MessageUtils.showSnackBar(
                                this@Login,
                                rel_lay_snackView_login,
                                MessageUtils.setErrorMessage(response.code())
                            )
                        }
                    } catch (ex: java.lang.Exception) {
                        ex.printStackTrace()
                    }
                }
            })
    }

    private fun callApi() = try {
        dialog = MessageUtils.showDialog(this)
        val calinterface = Utils.getInstance(this)
        val map = HashMap<String, Any>()
        map["username"] = login_username?.text.toString()
        map["password"] = login_password?.text.toString()
        val callback = calinterface.call_post("login", map)//Local Server

        callback.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
                val message = MessageUtils.setFailurerMessage(this@Login, t.message)
                snackBar = MessageUtils.showSnackBar(this@Login, rel_lay_snackView_login, message)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                MessageUtils.dismissDialog(dialog)
                try {
                    if (response.isSuccessful) {
                        loginRES = Gson().fromJson(response.body()?.string(), LoginRES::class.java)
                        if (loginRES != null) {
                            if (loginRES?.success!!) {
                                if (loginRES?.data != null) {
                                    sessionManager?.createLoginSession(
                                        loginRES?.data?.userdetails?.name!!,
                                        loginRES?.data?.userdetails?.email!!,
                                        loginRES?.data?.userdetails?.contactNo!!,
                                        loginRES?.data?.userdetails?.id.toString(),
                                        loginRES?.data?.accessToken!!,
                                        loginRES?.data?.userdetails?.role?.userScope!!,
                                        loginRES?.data?.userdetails?.role?.id.toString(),
                                        loginRES?.data?.userdetails?.districtId.toString(),
                                        loginRES?.data?.userdetails?.userScopeId.toString(),
                                        loginRES?.data?.userdetails?.blockId.toString(),
                                        loginRES?.data?.userdetails?.stateId.toString()
                                    )

                                    /**
                                     * Move to next Page With Corresponding Login
                                     */
                                    when {
                                        loginRES?.data?.userdetails?.role?.userScope!!.equals("AHO") -> {
                                            startActivity(Intent(this@Login, AHOActivity::class.java))
                                            finish()
                                        }
                                        loginRES?.data?.userdetails?.role?.userScope!!.equals("HO") -> {
                                            startActivity(Intent(applicationContext, HOActivity::class.java))
                                            finish()
                                        }
                                        loginRES?.data?.userdetails?.role?.userScope!!.equals("ADH") -> {
                                            startActivity(Intent(this@Login, ADHActivity::class.java))
                                            finish()
                                        }
                                        loginRES?.data?.userdetails?.role?.userScope!!.equals("DD") -> {
                                            startActivity(Intent(this@Login, DDActivity::class.java))
                                            finish()
                                        }
                                        else -> {
                                            snackBar = MessageUtils.showSnackBar(
                                                this@Login,
                                                rel_lay_snackView_login,
                                                "Please Login Authorized User"
                                            )

                                            //startActivity(Intent(this@Login, SplashActivity::class.java))
                                            //finish()
                                        }


                                    }


                                } else {
                                    snackBar = MessageUtils.showSnackBar(
                                        this@Login,
                                        rel_lay_snackView_login,
                                        loginRES?.message
                                    )
                                }
                            } else {
                                snackBar =
                                    MessageUtils.showSnackBar(this@Login, rel_lay_snackView_login, loginRES?.message)
                            }

                        } else {
                            val message = MessageUtils.setErrorMessage(response.code())
                            snackBar = MessageUtils.showSnackBar(this@Login, rel_lay_snackView_login, message)
                        }
                    } else {
                        val message = MessageUtils.setErrorMessage(response.code())
                        snackBar = MessageUtils.showSnackBar(this@Login, rel_lay_snackView_login, message)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        })
    } catch (ex: Exception) {
        ex.printStackTrace()
    }

    private fun validateformat(username: String, password: String): Boolean {
        try {
            when {
                username.isEmpty() -> {
                    snackBar =
                        MessageUtils.showSnackBar(this@Login, rel_lay_snackView_login, "Username Can't Be Empty")
                    return false
                }
                /* username.length != 10 -> {
                     MessageUtils.showSnackBar(activity, rel_lay_snackView_login, "Invalid User name")
                     return false
                 }*/
                password.isEmpty() -> {
                    snackBar =
                        MessageUtils.showSnackBar(this@Login, rel_lay_snackView_login, "Password Can't Be Empty")
                    return false
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return false
        }
        return true
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        try {
            val view = currentFocus
            if (view != null && (ev.action == MotionEvent.ACTION_UP || ev.action == MotionEvent.ACTION_MOVE) && view is EditText && !view.javaClass.name.startsWith(
                    "android.webkit."
                )
            ) {
                val scrcoords = IntArray(2)
                view.getLocationOnScreen(scrcoords)
                val x = ev.rawX + view.left - scrcoords[0]
                val y = ev.rawY + view.top - scrcoords[1]
                if (x < view.left || x > view.right || y < view.top || y > view.bottom)
                    (this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                        this.window.decorView.applicationWindowToken,
                        0
                    )
            }
        } catch (e: RuntimeException) {
            e.printStackTrace()
        }

        return super.dispatchTouchEvent(ev)
    }

    override fun onDestroy() {
        super.onDestroy()
        MessageUtils.dismissSnackBar(snackBar)
    }


    data class ModelForgotPassword(

        @field:SerializedName("data")
        val data: Data? = null,

        @field:SerializedName("success")
        val success: Boolean? = null,

        @field:SerializedName("message")
        val message: String? = null
    )
}

