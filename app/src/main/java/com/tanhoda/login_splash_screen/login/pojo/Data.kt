package com.tanhoda.login_splash_screen.login.pojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("access_token")
	val accessToken: String? = null,

	@field:SerializedName("expires_at")
	val expiresAt: String? = null,

	@field:SerializedName("userdetails")
	val userdetails: Userdetails? = null,

	@field:SerializedName("token_type")
	val tokenType: String? = null
)