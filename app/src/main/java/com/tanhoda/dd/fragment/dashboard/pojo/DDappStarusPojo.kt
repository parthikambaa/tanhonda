package com.tanhoda.dd.fragment.dashboard.pojo

import com.google.gson.annotations.SerializedName

class DDappStarusPojo(
    @field:SerializedName("image")
    var image:Int,
    @field:SerializedName("status_name")
    var statusName:String,
    @field:SerializedName("status_no")
    var status_no:String

)