package com.tanhoda.dd.fragment.applicationList.pojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("district")
	val district: Int? = null,

	@field:SerializedName("block")
	val block: Int? = null,

	@field:SerializedName("applications")
	val applications: ArrayList<ApplicationsItem?>? = null
)