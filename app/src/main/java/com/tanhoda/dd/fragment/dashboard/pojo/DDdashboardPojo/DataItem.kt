package com.tanhoda.dd.fragment.dashboard.pojo.DDdashboardPojo


import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class DataItem(

	@field:SerializedName("stage_name")
	val stageName: String? = null,

	@field:SerializedName("count")
	val count: Int? = null
)