package com.tanhoda.dd.fragment.details.appdetails.approve


import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

import com.tanhoda.R
import com.tanhoda.dd.DDActivity
import com.tanhoda.dd.fragment.details.appdetails.approve.adapter.DDdynamicviewadapter
import com.tanhoda.dd.fragment.details.appdetails.approve.pojo.DDdynamicofficer
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.Utils.log
import kotlinx.android.synthetic.main.dialog_dismiss_btn.*
import kotlinx.android.synthetic.main.dialog_permissions.*
import kotlinx.android.synthetic.main.dialog_pic_img.*
import kotlinx.android.synthetic.main.fragment_ddapp_approve.*
import kotlinx.android.synthetic.main.item_dynamic.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class DDappApprove : Fragment(), View.OnClickListener, OnclickPostionEvent {


    var GALLERY: Int = 0
    var CAMERA: Int = 1
    var IMAGE_DIRECTORY: String = "/TanHoda/Land"
    var path: String = ""
    var TAG = "AHO_FRAGMENT"
    var extraofficerArrayList = ArrayList<DDdynamicofficer>()
    var extraofficeradapter: DDdynamicviewadapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as DDActivity).disableNavigationInAdoptionHome("Details")
        return inflater.inflate(R.layout.fragment_ddapp_approve, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dd_app_approve_cancel.setOnClickListener(this)
        dd_app_approve_submit.setOnClickListener(this)
        dd_img_select.setOnClickListener(this)

        ins_add_more?.setOnClickListener {
            try {
                val officername = edt_comer_name?.text.toString()
                val department = edt_dept_name?.text.toString()
                val designation = edt_comer_designation?.text.toString()
                val remarks = edt_comer_remark?.text.toString()
                if (validateextraofficerdata(officername, department, designation, remarks)) {
                    val officeritem = DDdynamicofficer(officername, department, designation, remarks)
                    extraofficerArrayList.add(officeritem)
                    dd_recy_dynamic_view?.layoutManager =
                            LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
                    extraofficeradapter = DDdynamicviewadapter(activity!!, extraofficerArrayList, this)
                    dd_recy_dynamic_view?.adapter =extraofficeradapter
                    setCardViewempty()
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }


    private fun setCardViewempty() {
        try {
            edt_comer_name?.setText("")
            edt_comer_designation?.setText("")
            edt_comer_remark?.setText("")
            edt_dept_name?.setText("")
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun validateextraofficerdata(
        officername: String,
        department: String,
        designation: String,
        remarks: String
    ): Boolean {
        try {
            if (officername.isEmpty()) {
                MessageUtils.showSnackBar(activity, dd_app_approv_snackview, getString(R.string.enter_ooficername))
                return false
            } else if (department.isEmpty()) {
                MessageUtils.showSnackBar(activity, dd_app_approv_snackview, getString(R.string.enter_department))
                return false
            } else if (designation.isEmpty()) {
                MessageUtils.showSnackBar(activity, dd_app_approv_snackview, getString(R.string.enter_designation))
                return false
            } else if (remarks.isEmpty()) {
                MessageUtils.showSnackBar(activity, dd_app_approv_snackview, getString(R.string.enter_remark))
                return false
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return true
    }

    override fun onClick(v: View?) {
        try {
            if (v != null) {
                when (v.id) {
                    R.id.dd_app_approve_cancel -> {
                        activity!!.onBackPressed()
                    }

                    R.id.dd_app_approve_submit -> {

                        val isPatta = dd_ch_patta_copy.isChecked
                        val isChitta = dd_ch_chitta_copy.isChecked
                        val isAdangal = dd_ch_adangal_copy.isChecked
                        val isVAO = dd_ch_vao_certificate.isChecked
                        val isLease = dd_ch_lease_agreement.isChecked
                        val isSoil = dd_soil_report_ch.isChecked
                        val isWater = dd_water_report_ch.isChecked
                        var strRemarks = dd_edt_remarks.text.toString().trim()

                        val hashMap = HashMap<String, Any>()

                        val file = File(path)
                        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
                        // MultipartBody.Part is used to send also the actual file name
                        val body = MultipartBody.Part.createFormData("selfie", file.getName(), requestFile)
                        // add another part within the multipart request
                        val fullName = RequestBody.create(MediaType.parse("multipart/form-data"), file.getName())

                        hashMap["isPatta"] = isPatta
                        hashMap["isChitta"] = isChitta
                        hashMap["isAdangal"] = isAdangal
                        hashMap["isVAO"] = isVAO
                        hashMap["isLease"] = isLease
                        hashMap["isSoil"] = isSoil
                        hashMap["isWater"] = isWater

                        hashMap["body"] = body
                        hashMap["fullName"] = fullName
                        hashMap["strRemarks"] = strRemarks

                        log("hashMapsd", "" + hashMap)


                    }
                    R.id.dd_img_select -> {
                        if (
                            ContextCompat.checkSelfPermission(
                                activity!!,
                                android.Manifest.permission.CAMERA
                            ) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(
                                activity!!,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE
                            ) != PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(
                                activity!!,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ) != PackageManager.PERMISSION_GRANTED

                        ) {

                            val dialog = Dialog(activity)
                            dialog.setContentView(R.layout.dialog_permissions)
                            dialog.setCancelable(false)
                            dialog.window.setBackgroundDrawableResource(R.color.dialog_tans)
                            dialog.window.setGravity(Gravity.BOTTOM)
                            dialog.window.setWindowAnimations(R.style.UpDownDialogAnim)
                            dialog.show()
                            dialog.btn_grand.visibility = View.VISIBLE
                            dialog.permission_title.text = "Permission Required"
                            dialog.permission_message.text = "CAMERA\n" +
                                    "The App needs access to the camera to click your land Information\n" +
                                    "\n" +
                                    "STORAGE\n" +
                                    "The App accesses to your document verification"

                            dialog.btn_dismiss.setOnClickListener {
                                dialog.dismiss()
//                        token?.cancelPermissionRequest()
                            }
                            dialog.btn_grand.setOnClickListener {
                                dialog.dismiss()
                                validatePermissions(1)
                            }

                        } else {
                            showImagePickDialog()
                        }

                    }
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {

    }


    private fun showImagePickDialog() {

        val dialog = Dialog(activity)
        dialog.setContentView(R.layout.dialog_pic_img)
        dialog.setCancelable(false)
        dialog.window.setBackgroundDrawableResource(R.color.dialog_tans)
        dialog.window.setGravity(Gravity.BOTTOM)
        dialog.window.setWindowAnimations(R.style.UpDownDialogAnim)
        dialog.show()

        dialog.btn_dismiss.setOnClickListener {
            dialogDismiss(dialog)
        }

        dialog.gallery.setOnClickListener {
            dialogDismiss(dialog)

            choosePhotoFromGallary()
        }
        dialog.camera.setOnClickListener {
            dialogDismiss(dialog)
            takePhotoFromCamera()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_CANCELED) {
            dd_lay_file_name.visibility = View.GONE
            return
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data.data
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(activity!!.getContentResolver(), contentURI)
                    path = saveImage(bitmap)
                    log("pathsdsd", "" + path)
                    dd_txt_img_file_name.text = File(path).name
                    Toast.makeText(activity, "Image Saved!", Toast.LENGTH_SHORT).show()
                    dd_img_land.setImageBitmap(bitmap)
                    dd_lay_file_name.visibility = View.VISIBLE
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(activity, "Failed!", Toast.LENGTH_SHORT).show()
                }

            }

        } else if (requestCode == CAMERA) {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            dd_img_land.setImageBitmap(thumbnail)

            path = saveImage(thumbnail)
            log("pathCamera", path)
            dd_txt_img_file_name.text = File(path).name
            dd_lay_file_name.visibility = View.VISIBLE
            Toast.makeText(activity, "Image Saved!", Toast.LENGTH_SHORT).show()
        }
    }

    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            "" + Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY
        )
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }

        try {
            var pathsFileName = "" + Calendar.getInstance().getTimeInMillis() + ".jpg"
            val f = File(wallpaperDirectory, pathsFileName)
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                activity,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            log("TAG", "File Saved::---&gt;" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }

    fun choosePhotoFromGallary() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }


    private fun validatePermissions(isShow: Int) {
        log(TAG, "" + isShow)
        Dexter.withActivity(activity)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (isShow == 1 && report?.areAllPermissionsGranted()!!) {
                        showImagePickDialog()
                    }

                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?, token: PermissionToken?
                ) {
                    log("permissions", "denied")
                    token?.continuePermissionRequest()
                }

            })
            .check()

    }

    private fun showPermissionDalog(token: PermissionToken?) {

        AlertDialog.Builder(activity)
            .setTitle(
                "storage_permission_rationale_title"
            )
            .setMessage(
                "storage_permition_rationale_message"
            )
            .setNegativeButton(
                android.R.string.cancel
            ) { dialog, _ ->
                dialog.dismiss()
                token?.cancelPermissionRequest()
            }
            .setPositiveButton(
                android.R.string.ok
            ) { dialog, _ ->
                dialog.dismiss()
                token?.continuePermissionRequest()
            }
            .setOnDismissListener {
                token?.cancelPermissionRequest()
            }
            .show()
    }

    fun dialogDismiss(dialog: Dialog) {
        if (dialog != null) {
            if (dialog.isShowing) {
                dialog.dismiss()
            }
        }

    }


}
