package com.tanhoda.dd.fragment.details


import android.annotation.SuppressLint
import android.graphics.Typeface
import android.os.Bundle
import android.os.StrictMode
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tanhoda.R
import com.tanhoda.aho.AHOActivity
import com.tanhoda.dd.DDActivity
import com.tanhoda.dd.fragment.details.appdetails.DDapplDetails
import com.tanhoda.dd.fragment.details.ddtapadapter.DDappTapAdpter
import com.tanhoda.dd.fragment.details.farmerdetails.DDFarmerDetails
import com.tanhoda.dd.fragment.details.landdetails.DDLandDetails
import customs.CustomTextView

import kotlinx.android.synthetic.main.fragment_ddapp_details.*

class DDAppDetailsHome : Fragment() {

    var ddtapArrayList: ArrayList<String>? = null
    var dDappTapAdpter: DDappTapAdpter? = null
    var  typeface : Typeface?=null
    var txt:CustomTextView?=null
    var txt1:CustomTextView?=null
    var txt2:CustomTextView?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as DDActivity).disableNavigationInAdoptionHome(getString(R.string.details))
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ddapp_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())

        txt = CustomTextView(activity,null)
        txt1 = CustomTextView(activity,null)
        txt2 = CustomTextView(activity,null)

        ddtapArrayList = ArrayList()
        ddtapArrayList?.add(getString(R.string.app_details))
        ddtapArrayList?.add(getString(R.string.land_details))
        ddtapArrayList?.add(getString(R.string.farmer_details))

        dd_app_det_viewpager?.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(dd_app_det_taplay))
        dd_app_det_taplay?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                try{
                    when (tab?.position){
                        0 -> (activity as AHOActivity).disableNavigationInAdoptionHome(getString(R.string.app_details))
                        1 ->  (activity as AHOActivity).disableNavigationInAdoptionHome(getString(R.string.land_details))
                        2 ->  (activity as AHOActivity).disableNavigationInAdoptionHome(getString(R.string.farmer_details))
                    }

                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }

        })
        setupViewPager()
    }

    private fun setupViewPager() {
        try {
            dDappTapAdpter = DDappTapAdpter(childFragmentManager)
            dDappTapAdpter!!.addFragment(DDapplDetails(), ddtapArrayList?.get(0)!!)
            dDappTapAdpter!!.addFragment(DDLandDetails(), ddtapArrayList?.get(1)!!)
            dDappTapAdpter!!.addFragment(DDFarmerDetails(), ddtapArrayList?.get(2)!!)
            dd_app_det_viewpager?.adapter = dDappTapAdpter
            dd_app_det_taplay?.setupWithViewPager(dd_app_det_viewpager)
            initTabs()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    @SuppressLint("ResourceType")
    private fun initTabs() {
        try {

            txt?.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            txt?.gravity = Gravity.CENTER
            txt?.text = ddtapArrayList?.get(0)
            val tab = dd_app_det_taplay?.getTabAt(0)
            if (tab != null) {
                tab.customView = txt
                tab.text = dDappTapAdpter!!.getPageTitle(0)
            }
            txt1?.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            txt1?.gravity = Gravity.CENTER
            txt1?.text = ddtapArrayList?.get(1)
            val tab1 = dd_app_det_taplay?.getTabAt(1)
            if (tab1 != null) {
                tab1.customView = txt1
                tab1.text = dDappTapAdpter!!.getPageTitle(1)
            }

            txt2?.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            txt2?.gravity = Gravity.CENTER
            txt2?.text = ddtapArrayList?.get(2)
            val tab2 = dd_app_det_taplay?.getTabAt(2)
            if (tab2 != null) {
                tab2.customView = txt2
                tab2.text = dDappTapAdpter!!.getPageTitle(2)
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }


}