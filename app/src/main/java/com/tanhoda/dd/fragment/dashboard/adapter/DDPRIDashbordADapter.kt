package com.tanhoda.dd.fragment.dashboard.adapter

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.tanhoda.R
import com.tanhoda.dd.fragment.dashboard.pojo.DDappStarusPojo
import com.tanhoda.interfaces.OnclickPostionEvent

class DDPRIDashbordADapter(val context: Context, val statusArrayList: ArrayList<DDappStarusPojo>, val onclickPostionEvent: OnclickPostionEvent)
    : RecyclerView.Adapter<DDPRIDashbordADapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DDPRIDashbordADapter.ViewHolder {
        return   ViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_dd_pri_dashboard,parent,false))
    }

    override fun getItemCount(): Int { return  statusArrayList.size   }

    override fun onBindViewHolder(holder: DDPRIDashbordADapter.ViewHolder, position: Int) {
        try{
            holder.image?.setImageResource(statusArrayList[position].image)
            holder.statusName?.text = statusArrayList[position].statusName
            holder.statusno?.text = statusArrayList[position].status_no
            holder.master?.setOnClickListener {
                try{
                    onclickPostionEvent.OnclickPostionEvent(statusArrayList[position].statusName,position.toString())
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val  image =itemView?.findViewById<ImageView>(R.id.imgv_app_pri_dashboard_icon)
        val  statusName =itemView?.findViewById<customs.CustomTextView>(R.id.txt_app_pri_dashboard_name)
        val  statusno =itemView?.findViewById<customs.CustomTextView>(R.id.txt_app_pri_dashboard_no)
        val  master =itemView?.findViewById<ConstraintLayout>(R.id.dd_pri_dashboard_master_layout)
    }

}