package com.tanhoda.dd.fragment.dashboard.pojo.ddpirdashboardpojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Payment(

	@field:SerializedName("paymentcount")
	val paymentcount: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)