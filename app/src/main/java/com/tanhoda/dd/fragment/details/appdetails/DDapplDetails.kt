package com.tanhoda.dd.fragment.details.appdetails


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import com.tanhoda.R
import com.tanhoda.dd.fragment.applicationList.DDAppList.Companion.ddappListRES
import com.tanhoda.dd.fragment.applicationList.DDAppList.Companion.ddcurrentpositon
import com.tanhoda.dd.fragment.details.appdetails.approve.DDappApprove
import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import customs.CustomTextEditView
import customs.CustomTextView
import kotlinx.android.synthetic.main.dialog_approved_views.*
import kotlinx.android.synthetic.main.fragment_ddappl_details.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DDapplDetails : Fragment() {


    var popuptitle: CustomTextView? = null
    var popupremarks: CustomTextEditView? = null
    var popupcancel: CustomTextView? = null
    var popupsubmit: CustomTextView? = null
    var popupdialog: Dialog? = null
    var loaddialog: Dialog? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ddappl_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_ho_app_det_aprve?.setOnClickListener {
            try {
                FragmentCallUtils.passFragmentWithoutAnim(
                    activity,
                    DDappApprove(), R.id.aho_container_body
                )
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        btn_ho_app_det_reject?.setOnClickListener {
            try {
                remarksDialogdisplay(1)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        btn_ho_app_det_onhold?.setOnClickListener {
            try {
                remarksDialogdisplay(2)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        try {
            settoForm()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun settoForm() {
        try {
            val data = ddappListRES?.data?.applications!![ddcurrentpositon!!.toInt()]!!

            custxtviw_dd_app_det_scheme?.setText(data.appscheme?.schemeName!!)
            custxtviw_dd_app_det_category?.setText(data.appcategory?.category!!)
            custxtviw_dd_app_det_component?.setText(data.appcomponent?.componentName!!)
            if (data.applicationId != null) {
                custxtviw_dd_app_det_app_id?.setText(data.applicationId.toString())
            }

            if (data.applicationStatus != null) {
                custxtviw_dd_app_det_app_status?.setText(data.applicationStatus)
            }
            if (data.waitingFor != null) {
                custxtviw_dd_app_det_waitingfor?.setText(data.waitingFor)
            }

            /**
             * Proposal Details
             */

            if (data.areaProposed != null) {
                custxtviw_dd_app_det_area_proposed?.setText(data.areaProposed.toString())
            }
            if (data.proposedCrop != null) {
                custxtviw_dd_app_det_proposed_crop?.setText(data.proposedCrop.toString())
            }
            if (data.estimateCost != null) {
                custxtviw_dd_app_det_estimated_amt?.setText(data.estimateCost.toString())
            }
            if (data.govtSubsidy != null) {
                custxtviw_dd_app_det_whetherany?.setText(data.govtSubsidy.toString())
            }
            if (data.relevant != null) {
                custxtviw_dd_app_det_remarks?.setText(data.relevant)
            }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun remarksDialogdisplay(i: Int) {
        try {
            popupdialog = Dialog(activity)
            popupdialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            popupdialog?.setContentView(R.layout.dialog_remarksfr_reject)
            popupdialog?.setCancelable(false)
            popupdialog?.window?.setBackgroundDrawableResource(R.color.dialog_trans)
            popupdialog?.window?.setGravity(Gravity.BOTTOM)
            popupdialog?.show()
            popupdialog?.window?.setWindowAnimations(R.style.UpDownDialogAnim)
            popuptitle = popupdialog?.findViewById(R.id.aho_app_det_title_diag)
            popupcancel = popupdialog?.findViewById(R.id.btn_dismiss)
            popupsubmit = popupdialog?.findViewById(R.id.btn_grand)
            popupremarks = popupdialog?.findViewById(R.id.aho_app_det_remarks_dig)
            popupsubmit?.text = resources.getString(R.string.submit)
            popupsubmit?.visibility = View.VISIBLE

            if (i == 1) {
                popuptitle?.text = getString(R.string.reject_form)
            } else {
                popuptitle?.text = getString(R.string.onhold_form)
            }
            popupcancel?.setOnClickListener {
                try {
                    if (popupdialog != null && popupdialog?.isShowing!!) {
                        popupdialog?.dismiss()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            popupsubmit?.setOnClickListener {
                try {

                    if (Utils.haveNetworkConnection(activity)) {
                        if (popupremarks?.text.toString().isEmpty()) {
                            MessageUtils.showSnackBar(activity, popuptitle, getString(R.string.remarksvalidmsg))
                        } else {

                            sendtoUpdateREmarksinserver()
                        }
                    } else {
                        MessageUtils.showSnackBar(activity, popuptitle, getString(R.string.check_internet))
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun sendtoUpdateREmarksinserver() {
        try {
            loaddialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["user_id"] = SessionManager.getUsertypeId(activity)
//            map["user_id"] = 8
            map["application_id"] = ddappListRES?.data?.applications!![ddcurrentpositon!!.toInt()]?.id!!
            if (popuptitle?.text.toString().equals(getString(R.string.reject_form))) {
                map["submit_button"] = "Reject"
            } else {
                map["submit_button"] = "ON Hold"
            }
//            map["approval_id"] =DDAppDetailsHome
//            map["status"] ="AHO Holded"
            map["remarks"] = popupremarks?.text.toString()
            val calinterface = Utils.getInstance(activity)
            val callback =
                calinterface?.call_post("apprejectonhold", "Bearer " + SessionManager.getToken(activity), map)
            callback?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(loaddialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    MessageUtils.showSnackBar(activity, popuptitle, msg)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(loaddialog)

                    try {
                        if (response.isSuccessful) {
                            val obj = JSONObject(response.body()?.string())
                            val success = obj.getBoolean("success")
                            val message = obj.getString("message")
                            if (success) {
                                if (popupdialog != null && popupdialog?.isShowing!!) {
                                    popupdialog?.dismiss()
                                }
                                MessageUtils.showToastMessage(activity, message)
                            } else {
                                MessageUtils.showSnackBar(activity, popuptitle, message)
                            }
                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            MessageUtils.showSnackBar(activity, popuptitle, msg)
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }


                }

            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}
