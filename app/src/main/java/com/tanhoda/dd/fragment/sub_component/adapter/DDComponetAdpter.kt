package com.tanhoda.dd.fragment.sub_component.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.tanhoda.R
import com.tanhoda.dd.fragment.sub_component.pojo.SchemesItem
import com.tanhoda.interfaces.OnclickPostionEvent
import customs.CustomTextView

class DDComponetAdpter(
    val context: Context,
    val componetnArrayList: ArrayList<SchemesItem?>?,
    val onclickPostionEvent: OnclickPostionEvent
):RecyclerView.Adapter<DDComponetAdpter.Viewholader>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DDComponetAdpter.Viewholader {
        return  Viewholader(LayoutInflater.from(context).inflate(R.layout.adapter_dd_component_list , parent, false))
    }

    override fun getItemCount(): Int{ return   componetnArrayList?.size!!  }
    override fun onBindViewHolder(holder: DDComponetAdpter.Viewholader, position: Int) {
        try{
            holder.availble?.text = componetnArrayList!![position]?.availableIn
            holder.component?.text =componetnArrayList[position]?.componentName
            holder.siNo?.text =(position+1).toString()
            holder.subsidyamt?.text = componetnArrayList[position]?.subsidyAmount
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

    class Viewholader(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val siNo= itemView?.findViewById<CustomTextView>(R.id.txt_sino_dd_component_app_list_adp)
        val component= itemView?.findViewById<CustomTextView>(R.id.txt_component_dd_component_app_list_adp)
        val subsidyamt = itemView?.findViewById<CustomTextView>(R.id.txt_subsidy_dd_component_app_list_adp)
        val availble= itemView?.findViewById<CustomTextView>(R.id.txt_availble_dd_component_app_list_adp)
        val linear= itemView?.findViewById<LinearLayout>(R.id.dd_component_adap_linear)
    }

}