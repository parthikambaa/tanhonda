package com.tanhoda.dd.fragment.schemes.schemeappList


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.gson.Gson
import com.tanhoda.R
import com.tanhoda.dd.DDActivity
import com.tanhoda.dd.fragment.schemes.DDShemes
import com.tanhoda.dd.fragment.schemes.schemeappList.adapter.DDschemesApplistAdpter
import com.tanhoda.dd.fragment.schemes.schemeappList.pojo.DDschemeAppListRES
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_ddscheme_app_list.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DDschemeAppList : Fragment(), OnclickPostionEvent {
    var dialog: Dialog? = null
    var schemeAppRES: DDschemeAppListRES? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as DDActivity).disableNavigationInAdoptionHome("Scheme")
        return inflater.inflate(R.layout.fragment_ddscheme_app_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)

        /**
         * getting the stages AppList
         */

        getttingschemApplist()

    }

    private fun getttingschemApplist() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
//            map["user_id"] =SessionManager.getUserId(activity)
            map["user_id"] = 8
            map["scheme_id"] = DDShemes.ddshemeid
            val service = Utils.getInstance(activity)
            val callback =
                service.call_post("schemeapplicationlist", "Bearer " + SessionManager.getToken(activity), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    MessageUtils.showSnackBar(activity, dd_schemeapp_list_snack_view, msg)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            schemeAppRES = Gson().fromJson(response.body()?.string(), DDschemeAppListRES::class.java)
                            if (schemeAppRES != null) {
                                if (schemeAppRES?.success!!) {
                                    if (schemeAppRES?.data != null && schemeAppRES?.data?.applications?.size != 0) {
                                        setToadpter()
                                        recy_dd_schemeapp_list?.visibility = View.VISIBLE
                                        dd_schemeapp_empty_list?.visibility = View.GONE
                                        //dd_schemeapp_list_title_linear?.visibility = View.VISIBLE
                                    } else {
                                        //dd_schemeapp_list_title_linear?.visibility = View.GONE
                                        dd_schemeapp_empty_list?.visibility = View.VISIBLE
                                        dd_schemeapp_empty_list?.setText(schemeAppRES?.message)
                                        recy_dd_schemeapp_list?.visibility = View.GONE
                                    }
                                } else {
                                    MessageUtils.showSnackBar(
                                        activity,
                                        dd_schemeapp_list_snack_view,
                                        schemeAppRES?.message
                                    )
                                }

                            } else {
                                MessageUtils.showSnackBar(
                                    activity,
                                    dd_schemeapp_list_snack_view,
                                    getString(R.string.checkjsonformat)
                                )
                            }

                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            MessageUtils.showSnackBar(activity, dd_schemeapp_list_snack_view, msg)
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setToadpter() {
        try {
            val schemAppListADpter = DDschemesApplistAdpter(activity!!, schemeAppRES?.data?.applications, this)
            recy_dd_schemeapp_list?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            recy_dd_schemeapp_list?.adapter = schemAppListADpter

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {
        try {

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

}
