package com.tanhoda.dd.fragment.dashboard.pojo.ddpirdashboardpojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Head(

	@field:SerializedName("headcount")
	val headcount: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)