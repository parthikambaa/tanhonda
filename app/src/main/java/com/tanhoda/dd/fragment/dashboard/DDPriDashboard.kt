package com.tanhoda.dd.fragment.dashboard


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import com.google.gson.Gson

import com.tanhoda.R
import com.tanhoda.dd.DDActivity
import com.tanhoda.dd.fragment.dashboard.adapter.DDPRIDashbordADapter
import com.tanhoda.dd.fragment.dashboard.pojo.DDappStarusPojo
import com.tanhoda.dd.fragment.dashboard.pojo.ddpirdashboardpojo.DDPriDashboardRES
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_ddpri_dashboard.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DDPriDashboard : Fragment(),OnclickPostionEvent {
    var dialog: Dialog? = null
    var priDashboradArrayList=ArrayList<DDappStarusPojo>()
    companion object {
        var currentclickpostionaname=""
        var dashboradRES:DDPriDashboardRES?=null
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as DDActivity).enableToogle(getString(R.string.dashboard))
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ddpri_dashboard, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when{
            Utils.haveNetworkConnection(activity)->{
                getDashboardRes()
            }
            else ->{
                MessageUtils.showSnackBar(activity,dd_pridash_snackView,getString(R.string.check_internet))
            }
        }
    }
    private fun getDashboardRes() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["user_id"] = SessionManager.getUserId(activity)
            map["block"] = SessionManager.getBlockId(activity)
            map["role"] = SessionManager.getuserSopeID(activity)
            map["district"] = SessionManager.getdistrictID(activity)
            val calinterface = Utils.getInstance(activity)
            val callback = calinterface.call_post("officerprofile", "Bearer " + SessionManager.getToken(activity!!), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    MessageUtils.showSnackBar(activity, dd_pridash_snackView, msg)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            dashboradRES = Gson().fromJson(response.body()?.string(), DDPriDashboardRES::class.java)
                            if(dashboradRES!=null){
                                if (dashboradRES?.success!!){
                                    if(dashboradRES?.data!=null){
                                        priDashboradArrayList.clear()
                                        setApplicationStatus()

                                    }else{
                                        MessageUtils.showSnackBar(activity,dd_pridash_snackView,dashboradRES?.message)
                                    }

                                }else{
                                    MessageUtils.showSnackBar(activity,dd_pridash_snackView,dashboradRES?.message)
                                }

                            }else{
                                MessageUtils.showSnackBar(activity,dd_pridash_snackView,getString(R.string.checkjsonformat))
                            }
                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            MessageUtils.showSnackBar(activity, dd_pridash_snackView, msg)
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setApplicationStatus() {
        try {
            val newapp = DDappStarusPojo(R.drawable.ic_menu_camera, dashboradRES?.data?.block?.status!!, dashboradRES?.data?.block?.blockcount.toString())
            priDashboradArrayList.add(newapp)
            val preinspection = DDappStarusPojo(R.drawable.ic_menu_gallery, dashboradRES?.data?.district?.status!!,
                dashboradRES?.data?.district?.districtcount.toString())
            priDashboradArrayList.add(preinspection)
            val hoapprovel = DDappStarusPojo(R.drawable.ic_menu_camera, dashboradRES?.data?.head?.status!!, dashboradRES?.data?.head?.headcount.toString())
            priDashboradArrayList.add(hoapprovel)
            val adhapproval = DDappStarusPojo(R.drawable.ic_menu_camera, dashboradRES?.data?.payment?.status!!, dashboradRES?.data?.payment?.paymentcount.toString())
            priDashboradArrayList.add(adhapproval)
            val affidavit = DDappStarusPojo(R.drawable.ic_menu_camera, dashboradRES?.data?.work?.status!!, dashboradRES?.data?.work?.workcount.toString())
            priDashboradArrayList.add(affidavit)

            recy_dd_pri_dashboard_list?.layoutManager = GridLayoutManager(activity, 2, GridLayout.VERTICAL, false)
            val statsusAdapter = DDPRIDashbordADapter(activity!!, priDashboradArrayList, this)
            recy_dd_pri_dashboard_list?.adapter = statsusAdapter
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {
        try {
            currentclickpostionaname=name
            FragmentCallUtils.passFragment(activity, DDAppDashBoard(), R.id.aho_container_body)
//            FragmentCallUtils.passFragment(activity, ddAppDetails(), R.id.dd_container_body)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }
}
