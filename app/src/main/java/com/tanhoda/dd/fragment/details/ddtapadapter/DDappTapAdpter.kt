package com.tanhoda.dd.fragment.details.ddtapadapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import java.util.ArrayList

class DDappTapAdpter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {
    private val DetailsList = ArrayList<Fragment>()
    private val TitileList = ArrayList<String>()


    fun addFragment(fragment: Fragment, title: String) {
        DetailsList.add(fragment)
        TitileList.add(title)
    }

    override fun getItem(position: Int): android.support.v4.app.Fragment? {
        return DetailsList[position]
    }


    override fun getCount(): Int {
        return DetailsList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return TitileList[position]
    }

}