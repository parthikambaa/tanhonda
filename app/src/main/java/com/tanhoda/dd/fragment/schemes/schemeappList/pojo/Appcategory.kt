package com.tanhoda.dd.fragment.schemes.schemeappList.pojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Appcategory(

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("scheme_id")
	val schemeId: Int? = null,

	@field:SerializedName("active")
	val active: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("status")
	val category: String? = null,

	@field:SerializedName("create_date")
	val createDate: String? = null
)