package com.tanhoda.dd.fragment.schemes


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.gson.Gson
import com.tanhoda.R


import com.tanhoda.dd.DDActivity
import com.tanhoda.dd.fragment.schemes.adapter.DDschemeADpter
import com.tanhoda.dd.fragment.schemes.ddschemepojo.DDschemeRES
import com.tanhoda.dd.fragment.schemes.schemeappList.DDschemeAppList
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_ddshemes.*

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DDShemes : Fragment(),OnclickPostionEvent {

    var dialog: Dialog?=null
    var schemeRES: DDschemeRES?=null
    companion object {
        var ddshemeid=""
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as DDActivity).disableNavigationInAdoptionHome("Schemes")
        return inflater.inflate(R.layout.fragment_ddshemes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog =Dialog(activity)
        /**
         * getschemeList
         */
        getschemeList()
    }
    private fun getschemeList() {
        try{
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String,Any>()
//            map["user_id"] = SessionManager.getUserId(activity)
            map["user_id"] = 8
            map["district"] =SessionManager.getdistrictID(activity)
            val service = Utils.getInstance(activity)
            val callback =service.call_post("schemes","Bearer "+ SessionManager.getToken(activity),map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity,t.message)
                    MessageUtils.showSnackBar(activity, dd_scheme_snack_view,msg)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try{
                        if(response.isSuccessful){
                            schemeRES = Gson().fromJson(response.body()?.string(), DDschemeRES::class.java)
                            if(schemeRES!=null){
                                if(schemeRES?.success!!){
                                    if(schemeRES?.data!=null && schemeRES?.data?.schemes?.size!=0){
                                        recy_dd_scheme_list?.visibility =View.VISIBLE
                                        dd_scheme_list_title_linear?.visibility =View.VISIBLE
                                        dd_scheme_empty_list?.visibility =View.GONE
                                        setToAdapter()
                                    }else{
                                        dd_scheme_empty_list?.visibility =View.VISIBLE
                                        dd_scheme_empty_list?.text = schemeRES?.message
                                        recy_dd_scheme_list?.visibility =View.GONE
                                        dd_scheme_list_title_linear?.visibility =View.GONE
                                    }

                                }else{
                                    MessageUtils.showSnackBar(activity,dd_scheme_snack_view,schemeRES?.message)
                                }
                            }else{
                                MessageUtils.showSnackBar(activity,dd_scheme_snack_view,getString(R.string.checkjsonformat))
                            }

                        }else {
                            val  msg = MessageUtils.setErrorMessage(response.code())
                            MessageUtils.showSnackBar(activity,dd_scheme_snack_view,msg)
                        }

                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

            })

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setToAdapter() {
        try {
            val ddschemeadpter = DDschemeADpter(activity!!,schemeRES?.data?.schemes,this)
            recy_dd_scheme_list?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL,false)
            recy_dd_scheme_list?.adapter = ddschemeadpter

        }catch (ex:Exception){ex.printStackTrace()}
    }


    override fun OnclickPostionEvent(name: String, postion: String) {
        try {
           ddshemeid = schemeRES?.data?.schemes!![postion.toInt()]?.id.toString()
            FragmentCallUtils.passFragmentWithoutAnim(activity, DDschemeAppList(),R.id.aho_container_body)
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
}
