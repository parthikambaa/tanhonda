package com.tanhoda.dd.fragment.dashboard.adapter

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.tanhoda.R
import com.tanhoda.dd.fragment.dashboard.pojo.DDappStarusPojo
import com.tanhoda.interfaces.OnclickPostionEvent

class DDappStatusAdapter(val context:Context, val statusArrayList: ArrayList<DDappStarusPojo>, val onclickPostionEvent: OnclickPostionEvent)
    :RecyclerView.Adapter<DDappStatusAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DDappStatusAdapter.ViewHolder {
        return   ViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_dd_app_status,parent,false))
    }

    override fun getItemCount(): Int { return  statusArrayList.size   }

    override fun onBindViewHolder(holder: DDappStatusAdapter.ViewHolder, position: Int) {
        try{
            holder.image?.setImageResource(statusArrayList[position].image)
            holder.statusName?.text = statusArrayList[position].statusName
            holder.statusno?.text = statusArrayList[position].status_no
            holder.master?.setOnClickListener {
                try{
                    onclickPostionEvent.OnclickPostionEvent(statusArrayList[position].statusName,position.toString())
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val  image =itemView?.findViewById<ImageView>(R.id.imgv_app_status_icon)
        val  statusName =itemView?.findViewById<customs.CustomTextView>(R.id.txt_app_status_name)
        val  statusno =itemView?.findViewById<customs.CustomTextView>(R.id.txt_app_status_no)
        val  master =itemView?.findViewById<ConstraintLayout>(R.id.dd_status_master_layout)
    }

}