package com.tanhoda.dd.fragment.sub_component

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.gson.Gson
import com.tanhoda.R
import com.tanhoda.dd.DDActivity
import com.tanhoda.dd.fragment.sub_component.adapter.DDComponetAdpter
import com.tanhoda.dd.fragment.sub_component.pojo.DDComponentRES
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.login_splash_screen.SplashActivity.Companion.snackBar

import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_ddsub_component.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DDSUBComponent : Fragment() ,OnclickPostionEvent{
    var dialog : Dialog?=null
    var componentRES: DDComponentRES?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as DDActivity).disableNavigationInAdoptionHome(" DD component")
        return inflater.inflate(R.layout.fragment_ddsub_component, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)
        /**
         * getcomponentList()
         */
        getComponent()
    }

    private fun getComponent() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map  =HashMap<String,Any>()
//            map["user_id"] = SessionManager.getUserId(activity)
            map["user_id"] = 8
//            map["district"] =SessionManager.getdistrictID(activity)
            map["district"] =25
            val service = Utils.getInstance(activity)
            val callback =service.call_post("components","Bearer "+ SessionManager.getToken(activity),map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity,t.message)
                    snackBar =  MessageUtils.showSnackBar(activity,dd_component_snackview,msg)
                }
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if(response.isSuccessful){
                            componentRES = Gson().fromJson(response.body()?.string(), DDComponentRES::class.java)
                            if(componentRES!=null){
                                if(componentRES?.success!!){
                                    if(componentRES?.data!=null && componentRES?.data?.schemes?.size!=0){
                                        setToadapter()
                                        recy_dd_compo_list?.visibility =View.VISIBLE
                                        dd_compo_list_title_linear?.visibility =View.VISIBLE
                                        dd_compo_empty_list?.visibility=View.GONE
                                    }else{
                                        dd_compo_list_title_linear?.visibility =View.GONE
                                        dd_compo_empty_list?.visibility=View.VISIBLE
                                        recy_dd_compo_list?.visibility =View.GONE
                                    }
                                }else{
                                    snackBar = MessageUtils.showSnackBar(activity,dd_component_snackview,componentRES?.message)
                                }
                            }else{
                                snackBar = MessageUtils.showSnackBar(activity,dd_component_snackview,getString(R.string.checkjsonformat))
                            }

                        }else{
                            val msg = MessageUtils.setErrorMessage(response.code())
                            snackBar =  MessageUtils.showSnackBar(activity,dd_component_snackview,msg)
                        }

                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }
            })

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setToadapter() {
        try{
            val componetAdpter= DDComponetAdpter(activity!!,componentRES?.data?.schemes,this)
            recy_dd_compo_list?.layoutManager = LinearLayoutManager(activity , LinearLayout.VERTICAL,false)
            recy_dd_compo_list?.adapter =componetAdpter
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {
        try{

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
}
