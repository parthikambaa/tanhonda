package com.tanhoda.dd.fragment.details.landdetails


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tanhoda.R
import com.tanhoda.dd.fragment.applicationList.DDAppList.Companion.ddappListRES
import com.tanhoda.dd.fragment.applicationList.DDAppList.Companion.ddcurrentpositon
import kotlinx.android.synthetic.main.fragment_ddland_details.*

class DDLandDetails : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ddland_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        try{
            setToForm()
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setToForm() {
        try{
            
            val data=ddappListRES?.data?.applications!![ddcurrentpositon!!.toInt()]?.appland!!
            val appitem = ddappListRES?.data?.applications!![ddcurrentpositon!!.toInt()]!!

            if(data.serveyNo!=null){
                custxtviw_dd_land_survey_number?.setText(data.serveyNo)
            }
            if(data.sourceOfIrrigation!=null){
                custxtviw_dd_land_survey_irragtion?.setText(data.sourceOfIrrigation)
            }
            if(data.totalArea!=null){
                custxtviw_dd_land_total_area?.setText(data.totalArea)
            }
            if(appitem.village!=null){
                custxtviw_dd_land_village?.setText(appitem.village.toString())
            }
            if(appitem.appblock!=null){
                custxtviw_dd_land_block?.setText(appitem.appblock.block)
            }
            if(appitem.appdistrict!=null){
                custxtviw_dd_land_district?.setText(appitem.appdistrict.district)
            }
            if(appitem.appstate!=null){
                custxtviw_dd_land_state?.setText(appitem.appstate.state)
            }
            
        }catch (ex:Exception){
            ex.printStackTrace()
        }
        
    }

}
