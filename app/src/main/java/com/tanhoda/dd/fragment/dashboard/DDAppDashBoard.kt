package com.tanhoda.dd.fragment.dashboard


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import com.google.gson.Gson
import com.tanhoda.R
import com.tanhoda.dd.DDActivity
import com.tanhoda.dd.fragment.applicationList.DDAppList
import com.tanhoda.dd.fragment.dashboard.adapter.DDappStatusAdapter
import com.tanhoda.dd.fragment.dashboard.pojo.DDappStarusPojo
import com.tanhoda.dd.fragment.dashboard.pojo.DDdashboardPojo.DDdashboardRES
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils

import kotlinx.android.synthetic.main.fragment_ddapp_dash_board.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DDAppDashBoard : Fragment(), OnclickPostionEvent {

    var dialog: Dialog? = null
    var dashboradRES: DDdashboardRES? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as DDActivity).disableNavigationInAdoptionHome(getString(R.string.dashboard))
        return inflater.inflate(R.layout.fragment_ddapp_dash_board, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /**
         * get and set Application status
         * static Menus
         */
        when {
            Utils.haveNetworkConnection(activity) -> {
                getDashboardRes()
            }
            else -> {
                MessageUtils.showSnackBar(activity, dd_dash_snackView, getString(R.string.check_internet))
            }
        }
    }

    private fun getDashboardRes() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["user_id"] = SessionManager.getUserId(activity)
            map["block"] = SessionManager.getBlockId(activity)
            map["role"] = SessionManager.getuserSopeID(activity)
            map["district"] = SessionManager.getdistrictID(activity)
            map["status"] = DDPriDashboard.currentclickpostionaname
            val calinterface = Utils.getInstance(activity)
            val callback = calinterface.call_post("dashboard", "Bearer " + SessionManager.getToken(activity!!), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    MessageUtils.showSnackBar(activity, dd_dash_snackView, msg)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            dashboradRES = Gson().fromJson(response.body()?.string(), DDdashboardRES::class.java)
                            if (dashboradRES != null) {
                                if (dashboradRES?.success!!) {
                                    if (dashboradRES?.data != null) {
                                        if (dashboradRES?.data != null) {
                                            val itemarraylist = ArrayList<DDappStarusPojo>()
                                            for (item in dashboradRES?.data!!) {
                                                val ahostatuspojo = DDappStarusPojo(
                                                    R.drawable.ic_menu_gallery,
                                                    item?.stageName!!,
                                                    item.count.toString()
                                                )
                                                itemarraylist.add(ahostatuspojo)
                                            }
                                            SetApplicationStatus(itemarraylist)
                                        } else {
                                            MessageUtils.showSnackBar(
                                                activity,
                                                dd_dash_snackView,
                                                dashboradRES?.message
                                            )
                                        }


                                    } else {
                                        MessageUtils.showSnackBar(activity, dd_dash_snackView, dashboradRES?.message)
                                    }

                                } else {
                                    MessageUtils.showSnackBar(activity, dd_dash_snackView, dashboradRES?.message)
                                }

                            } else {
                                MessageUtils.showSnackBar(
                                    activity,
                                    dd_dash_snackView,
                                    getString(R.string.checkjsonformat)
                                )
                            }
                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            MessageUtils.showSnackBar(activity, dd_dash_snackView, msg)
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun SetApplicationStatus(itemarraylist: ArrayList<DDappStarusPojo>) {
        try {
            recy_dd_app_status_list?.layoutManager = GridLayoutManager(activity, 2, GridLayout.VERTICAL, false)
            val statsusAdapter = DDappStatusAdapter(activity!!, itemarraylist, this)
            recy_dd_app_status_list?.adapter = statsusAdapter
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {
        try {
            FragmentCallUtils.passFragment(activity, DDAppList(), R.id.aho_container_body)
//            FragmentCallUtils.passFragment(activity, DDappApprove(), R.id.aho_container_body)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }
}
