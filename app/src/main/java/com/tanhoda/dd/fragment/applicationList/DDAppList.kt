package com.tanhoda.dd.fragment.applicationList


import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.google.gson.Gson

import com.tanhoda.R
import com.tanhoda.dd.DDActivity
import com.tanhoda.dd.fragment.applicationList.adapter.DDAppListAdapter
import com.tanhoda.dd.fragment.applicationList.pojo.DDApplistRES
import com.tanhoda.dd.fragment.details.DDAppDetailsHome
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.login_splash_screen.SplashActivity.Companion.snackBar

import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_ddapp_list.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DDAppList : Fragment(),OnclickPostionEvent {
    var ddApplicationadapter: DDAppListAdapter? = null
    val TAG = "AHOAPPLICATIONLIST"
    var dialog: Dialog? = null
    var snackViewADH: RelativeLayout? = null
    var snackbar: Snackbar?=null

    companion object {
        var ddappListRES: DDApplistRES?=null
        var ddcurrentpositon: String?=null
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as DDActivity).disableNavigationInAdoptionHome(getString(R.string.applicationlist))
        return inflater.inflate(R.layout.fragment_ddapp_list, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)
        snackViewADH = view.findViewById(R.id.rel_lay_dd_app_list_snackview)

        /*
         * get DD Application List*/

        if (Utils.haveNetworkConnection(activity)) {
            getApplicationList()
        } else {
            snackBar =     MessageUtils.showSnackBar(activity, snackViewADH, getString(R.string.check_internet))
        }


    }
    private fun getApplicationList() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["userid"] = SessionManager.getUserId(activity)
//            map["role"] = SessionManager.getUsertypeId(activity)
            map["role"] =2
            map["status"] = "AHO Approved"
            val callinterface = Utils.getInstance(activity)
            val callback =  callinterface.call_post("applicationlist","Bearer "+SessionManager.getToken(activity), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t!!.message)
                    snackbar = MessageUtils.showSnackBar(activity, snackViewADH, msg)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if(response.isSuccessful){
                            ddappListRES = Gson().fromJson(response.body()?.string(), DDApplistRES::class.java)
                            if (ddappListRES != null) {
                                if (ddappListRES?.success!!) {
                                    if (ddappListRES?.data != null) {
                                        if (ddappListRES?.data?.applications != null && ddappListRES?.data?.applications?.size != 0) {
                                            setToADapter()
                                            dd_app_empty_list?.visibility = View.GONE
                                            recy_dd_app_list?.visibility = View.VISIBLE
                                            dd_app_list_title_linear?.visibility = View.VISIBLE
                                        } else {
                                            dd_app_list_title_linear?.visibility = View.GONE
                                            recy_dd_app_list?.visibility = View.GONE
                                            dd_app_empty_list?.visibility = View.VISIBLE
                                            dd_app_empty_list?.setText(ddappListRES?.message)
                                        }

                                    } else {
                                        snackBar =  MessageUtils.showSnackBar(
                                            activity,
                                            snackViewADH,
                                            ddappListRES?.message
                                        )
                                    }
                                } else {
                                    snackBar = MessageUtils.showSnackBar(
                                        activity,
                                        snackViewADH,
                                        ddappListRES?.message
                                    )
                                }
                            } else {
                                snackBar =   MessageUtils.showSnackBar(
                                    activity,
                                    snackViewADH,
                                    getString(R.string.checkjsonformat)
                                )
                            }

                        }else{
                            val msg = MessageUtils.setErrorMessage(response.code())
                            snackBar = MessageUtils.showSnackBar(activity,snackViewADH,msg)
                        }

                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }

                }

            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setToADapter() {
        try {
            recy_dd_app_list?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            ddApplicationadapter = DDAppListAdapter(activity!!, ddappListRES?.data?.applications ,this)
            recy_dd_app_list?.adapter = ddApplicationadapter
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {
        try {
            MessageUtils.dismissSnackBar(snackBar)
            ddcurrentpositon=postion
            FragmentCallUtils.passFragment(activity, DDAppDetailsHome(),R.id.aho_container_body)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        MessageUtils.dismissSnackBar(snackBar)
    }
}

