package com.tanhoda.dd.fragment.details.appdetails.approve.pojo

import com.google.gson.annotations.SerializedName

class DDdynamicofficer(
    @field:SerializedName("officer_name")
    var officerName:String?=null,
    @field:SerializedName("department")
    var department:String?=null,
    @field:SerializedName("designation")
    var designation:String?=null,
    @field:SerializedName("remarks")
    var remarks:String?=null
)