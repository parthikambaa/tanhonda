package com.tanhoda.common

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import android.view.WindowManager
import com.tanhoda.R
import com.tanhoda.utils.ImageUtils
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.content_view_image.*

class ViewImage : AppCompatActivity() {
    val TAG: String = "ImageViewLoader"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.getWindow()
            .setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.content_view_image)

        var imageUrl = intent.getStringExtra("imageUrl")
        val imageType = intent.getStringExtra("imageType")
        val from = intent.getStringExtra("from")


        if (from.equals("doc")) {
            imageUrl = Utils.IMG_DOCUMNTS_URL + imageUrl
        } else {
            imageUrl = Utils.IMG_INSPECTION_URL+ imageUrl
        }

        ImageUtils.displayImageFromUrl(
            this.applicationContext,
            imageUrl, photoView
        )
    }

}
