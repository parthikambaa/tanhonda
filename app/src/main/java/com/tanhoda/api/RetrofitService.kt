package com.tanhoda.api

import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*
import java.util.*
import okhttp3.RequestBody
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


interface RetrofitService {

    @POST("{path}")
    @FormUrlEncoded
    fun call_post_get(@Path("path") path: String, @Header("Authorization") auth: String, @FieldMap map: HashMap<String, Any>): Call<ResponseBody>

    @POST("{path}")
    fun call_post(@Path("path") path: String, @Body map: HashMap<String, Any>): Call<ResponseBody>

    @POST("{path}")
    fun call_post(@Path("path") path: String, @Header("Authorization") auth: String, @Body map: HashMap<String, Any>): Call<ResponseBody>

    @Multipart
    @POST("{path}")
    fun call_post(
        @Path("path") method: String,
        @Header("Authorization") auth: String,
        @PartMap map: HashMap<String, Any>,
        @Part Image: MultipartBody.Part
    ): Call<ResponseBody>

    @Multipart
    @POST("{path}")
    fun call_postimage(
        @Path("path") method: String,
        @Header("Authorization") auth: String,
        @PartMap partMap:HashMap<String,Any>,
        @Part map: ArrayList<MultipartBody.Part>
    ): Call<ResponseBody>

    @Multipart
    @POST("{path}")
    fun call_twoImageArray(
        @Path("path") method:String,
        @Header("Authorization") auth:String,
        @PartMap partMap: HashMap<String, Any>,
        @Part imagepart:ArrayList<MultipartBody.Part>,
        @Part inspectionPart: ArrayList<MultipartBody.Part>

    ):Call<ResponseBody>


}