package com.tanhoda;

import android.app.Application;
import android.util.Log;
import com.tanhoda.utils.Utils;
import customs.Fonts;
import customs.TypefaceUtil;

public class ApplicationInit extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", Fonts.BOLD);

    }
}
