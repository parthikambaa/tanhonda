package com.tanhoda.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.app.FragmentActivity;
import com.tanhoda.R;
import com.tanhoda.login_splash_screen.login.Login;

import java.util.HashMap;


public class SessionManager {

    private SharedPreferences pref;
    private Editor editor;
    private FragmentActivity context;
    private int PRIVATE_MODE = 0;

    /*
       "id": 2,
      "user_scope_id": 8,
      "name": "Vanitha",
      "email": "vanitha@kambaa.com",
      "email_verified_at": null,
      "contact_no": "9485437853",
      "address": "12\/1 Arasu paniyalar nagar\r\nCheran manager",
      "state_id": 1,
      "district_id": 1,
      "block_id": 1,
      "user_type": "All",
      "scheme_id": 0,
      "active": 1,
      "created_at": "2019-03-25 00:53:14",
      "updated_at": "2019-03-25 00:53:14"
     */


    public static String KEY_ID = "key_id";
    private static String KEY_USERNAME = "key_username";
    private static String KEY_EMAIL = "key_email";
    public static String KEY_MOBILE = "9999999999";
    private static String IS_LOGIN = "IsLogedIn";
    public static String KEY_NO = "PURPLE";
    public static String KET_PROFILE_PHOTO = "";
    public static String KEY_TOKEN = "token";
    public static String KEY_TOKENTYPE = "tokentype";
    public static String KEY_USERTYPE = "usertype";
    public static String KEY_USERTYPE_ID = "usertypeID";
    public static String LANGUAGES = "English";
    public static String DISTRICT_ID = "district_id";
    public static String USER_SCOPE_ID = "user_scope_id";
    public static String BLOCK_ID = "block_id";
    public static String STATE_ID = "state_id";

    public SessionManager(FragmentActivity context) {
        this.context = context;
        this.pref = context.getSharedPreferences(KEY_NO, PRIVATE_MODE);
        this.editor = pref.edit();
    }

    public SessionManager() {

    }

    public void createLoginSession(String name, String email, String mobile, String id, String token, String usert_type, String user_type_id,
                                   String district_id, String user_scope_id, String block_id, String state_id) {

        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USERNAME, name);
        //editor.putString(KET_PROFILE_PHOTO, photo);
        editor.putString(KEY_ID, id);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_MOBILE, mobile);
        editor.putString(KEY_TOKEN, token);
        editor.putString(KEY_USERTYPE, usert_type);
        editor.putString(KEY_USERTYPE_ID, user_type_id);
        editor.putString(DISTRICT_ID, district_id);
        editor.putString(USER_SCOPE_ID, user_scope_id);
        editor.putString(BLOCK_ID, block_id);
        editor.putString(STATE_ID, state_id);
        editor.commit();

    }

    public void Setlanguages(String lang) {
        editor.putString(LANGUAGES, lang);
        editor.commit();
    }

    public boolean checkLogin() {
        if (!this.isLoggedIn()) {
            // context.getSupportFragmentManager().beginTransaction().replace(R.id.container_body, new Login()).commit();
            return false;
        } else {
            //context.getSupportFragmentManager().beginTransaction().replace(R.id.container_body, new Attendace()).commit();
            return true;
        }

    }

    public static String getUserId(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(KEY_ID, KEY_ID);
    }

    public static String getuserSopeID(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(USER_SCOPE_ID, USER_SCOPE_ID);
    }

    public static String getBlockId(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(BLOCK_ID, BLOCK_ID);
    }

    public static String getStateId(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(STATE_ID, STATE_ID);
    }

    public static String getdistrictID(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(DISTRICT_ID, DISTRICT_ID);
    }

    public static String getUsertypeId(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(KEY_USERTYPE_ID, KEY_USERTYPE_ID);
    }

    public static String getLANGUAGES(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(LANGUAGES, LANGUAGES);
    }

    public static String getUserType(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(KEY_USERTYPE, KEY_USERTYPE);
    }

    public static String getToken(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(KEY_TOKEN, KEY_TOKEN);
    }


    public static String getMobile(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(KEY_MOBILE, KEY_MOBILE);
    }

    public static String getEmailId(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(KEY_EMAIL, KEY_EMAIL);
    }

    public static String getName(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(KEY_USERNAME, KEY_USERNAME);
    }

    public static String getProfileImg(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        return pref.getString(KET_PROFILE_PHOTO, KET_PROFILE_PHOTO);
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put("profilePhoto", pref.getString(KET_PROFILE_PHOTO, KET_PROFILE_PHOTO));
        user.put("mobileNumber", pref.getString(KEY_MOBILE, KEY_MOBILE));
        user.put("email", pref.getString(KEY_EMAIL, KEY_EMAIL));
        user.put("name", pref.getString(KEY_USERNAME, KEY_USERNAME));
        user.put("id", pref.getString(KEY_ID, KEY_ID));

        return user;
    }

    public static void logoutUser(FragmentActivity fragmentActivity) {
        SharedPreferences pref = fragmentActivity.getSharedPreferences(KEY_NO, 0);
        Editor editor = pref.edit();
        editor.clear();
        editor.commit();
        MessageUtils.showToastMessage(fragmentActivity, fragmentActivity.getResources().getString(R.string.logoutsuccess));
        fragmentActivity.startActivity(new Intent(fragmentActivity, Login.class));
        fragmentActivity.finish();
    }

    public void upDate(String Employeephoto) {
        editor.putString(KET_PROFILE_PHOTO, Employeephoto);
        editor.commit();
    }


    public static void upDateProfileImage(String Employeephoto, Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NO, 0);
        Editor editor = pref.edit();
        editor.putString(KET_PROFILE_PHOTO, Employeephoto);
        editor.commit();
    }


    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }


}
