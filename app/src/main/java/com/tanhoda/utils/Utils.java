package com.tanhoda.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.InputFilter;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.tanhoda.R;
import com.tanhoda.api.RetrofitCall;
import com.tanhoda.api.RetrofitService;
import com.tanhoda.common.ViewImage;
import customs.CustomTextView;
import org.jetbrains.annotations.NotNull;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Formatter;

import static android.support.constraint.Constraints.TAG;


/**
 * Created by AND I5 on 30-03-2018.
 */

public class Utils {

    //Live
    //public static String BASE_PARENT = "http://35.154.245.147:8008";


    //Public
    private static String BASE_PARENT = "http://122.165.208.45/";
    //Local
    //public static String BASE_PARENT = "http://192.168.2.4:8007/";


    public static String IMG_URL = BASE_PARENT;
    public static String IMG_COMPONENT_URL = BASE_PARENT + "uploads/components/";

    public static String IMG_INSPECTION_URL = BASE_PARENT + "inspection/";
    public static String BASE_URL = BASE_PARENT + "api/officers/";
    public static String IMG_DOCUMNTS_URL = BASE_PARENT + "documents/";

    public static final String APPROVED = "Approved";
    public static final String PENDING = "Pending";
    public static final String OH_HOLD = "On Hold";
    public static String ahocurrentAPPID = "";

    //public static String PDF_LOADER = "http://docs.google.com/gview?embedded=true&url=" + BASE_PARENT + "web/applicationpdf/";
    public static String PDF_LOADER = "https://docs.google.com/viewerng/viewer?url=" + BASE_PARENT + "web/applicationpdf/";
    public static String PRIVACY_POLICY = "https://app-privacy-policy-generator.firebaseapp.com/#";


    public static int BLOCK_LEVEL_PRE_INSPECTIONS = 1;
    public static int BLOCK_LEVEL_HO_APPROVAL = 2;
    public static int BLOCK_LEVEL_AHO_APPROVAL = 3;
    public static int AFFIDAVIT = 4;
    public static int DISTRICT_LEVEL_SUPERRENT_APPROVAL = 9;
    public static int DISTRICT_LEVEL_HO_TECH_APPROVAL = 10;
    public static int DISTRICT_LEVEL_DISTRICT_HEAD_APPROVAL = 11;
    public static int DISTRICT_LEVEL_DD_APPROVAL = 12;
    public static int HEAD_OFFICE_DD_APPROVAL = 13;
    public static int HEAD_OFFICE_AD_APPROVAL = 14;
    public static int HEAD_OFFICE_DIRECTOR_APPROVAL = 15;
    public static int HEAD_OFFICE_SLEC_APPROVAL = 17;
    public static int DMC_LEVEL_DMC_APPROVAL = 16;
    public static int WORK_ORDER_RELEASE = 18;
    public static int WORK_IN_PROCESS_QUOTATION_UPLOAD = 19;
    public static int WORK_IN_PROCESS_INVOICE_UPLOAD = 20;
    public static int WORK_IN_PROCESS_BASEMENT_WORK_INSPECTION = 21;
    public static int WORK_IN_PROCESS_FRAME_WORK_INSPECTION = 22;
    public static int WORK_IN_PROCESS_JOINT_INSPECTION = 23;
    public static int CHECKER_APPROVAL = 24;
    public static int HO_TECH_APPROVAL = 25;
    public static int JD_AD_APPROVAL = 26;
    public static int PAYMENT_RELEASE = 27;


    /**
     * Check Build Version
     *
     * @return
     */
    public static boolean checkVersion() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            //view.setBackgroundResource(R.drawable.ripple_center);
            return true;
        } else {
            return false;
        }
    }


    public static RetrofitService getInstance(FragmentActivity activity) {
        return RetrofitCall.getClient().create(RetrofitService.class);
    }


    public static ViewGroup.LayoutParams setParamsForListView(int position, ArrayList<Integer> items) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        if (position == 0) {
            //First One
            lp.setMargins(8, 8, 8, 3);
        } else if (position == (items.size() - 1)) {
            //Last One Item
            lp.setMargins(8, 3, 8, 8);
        } else {
            //Others
            lp.setMargins(8, 3, 8, 3);
        }

        return lp;
    }

    public static ViewGroup.LayoutParams setParamsForListViewInAll(int position, int itemSize, String view) {

        LinearLayout.LayoutParams lp = null;
        if (view.equals("l")) {
            lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } else if (view.equals("c")) {
            lp = new LinearLayout.LayoutParams(CardView.LayoutParams.MATCH_PARENT, CardView.LayoutParams.WRAP_CONTENT);
        } else {
            lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        }

        if (position == 0) {
            //First One
            lp.setMargins(24, 24, 24, 12);
        } else if (position == (itemSize - 1)) {
            //Last One Item
            lp.setMargins(24, 12, 24, 24);
        } else {
            //Others
            lp.setMargins(24, 12, 24, 12);
        }

        return lp;
    }

    public static ViewGroup.LayoutParams setParamsForGridView(int position, int items) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        if (position == 0) {
            //First One
            lp.setMargins(24, 24, 12, 12);
        } else if (position == 1) {
            //Second One
            lp.setMargins(12, 24, 24, 12);
        } /*else if (position == (items - 1)) {
            //Last One Item
            lp.setMargins(12, 12, 24, 24);
        } else if (position == (items - 2)) {
            //Last Two Item
            lp.setMargins(24, 12, 12, 24);
        } */ else if ((position % 2) == 0) {
            if (position == (items - 2)) {
                lp.setMargins(24, 12, 12, 24);
            } else {
                //3,5,7..etc
                lp.setMargins(24, 12, 12, 12);
            }
        } else {
            //4,6,8..etc
            if (position == (items - 1)) {
                //Last One Item
                lp.setMargins(12, 12, 24, 24);
            } else {
                lp.setMargins(12, 12, 24, 12);
            }
        }

        return lp;
    }


    public static ViewGroup.LayoutParams setParamsForGridViewCons(int position, int items) {
        ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        if (position == 0) {
            //First One
            lp.setMargins(24, 24, 12, 12);
        } else if (position == 1) {
            //Second One
            lp.setMargins(12, 24, 24, 12);
        } /*else if (position == (items - 1)) {
            //Last One Item
            lp.setMargins(12, 12, 24, 24);
        } else if (position == (items - 2)) {
            //Last Two Item
            lp.setMargins(24, 12, 12, 24);
        } */ else if ((position % 2) == 0) {
            if (position == (items - 2)) {
                lp.setMargins(24, 12, 12, 24);
            } else {
                //3,5,7..etc
                lp.setMargins(24, 12, 12, 12);
            }
        } else {
            //4,6,8..etc
            if (position == (items - 1)) {
                //Last One Item
                lp.setMargins(12, 12, 24, 24);
            } else {
                lp.setMargins(12, 12, 24, 12);
            }
        }

        return lp;
    }

    public static ViewGroup.LayoutParams setParamsForGridViewThreeItem(int position, int itemsSize) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        if (position == 0) {
            //First One
            lp.setMargins(24, 24, 12, 12);
        } else if (position == 1) {
            //Second One
            lp.setMargins(12, 24, 12, 12);
        } else if (position == 2) {
            //Third One
            lp.setMargins(12, 24, 24, 12);
        }/* else if (position == (items.size() - 1)) {
            //Last One Item
            lp.setMargins(12, 12, 24, 24);
        } else if (position == (items.size() - 2)) {
            //Last Two Item
            lp.setMargins(12, 12, 12, 24);
        } else if (position == (items.size() - 3)) {
            //Last Third Item
            lp.setMargins(24, 12, 12, 24);
        } else if ((position % 3) == 3) {
            log("sdsddd", "111   " + position);
            //3,6..etc
            lp.setMargins(12, 12, 24, 12);

        } */ else if (position % 3 == 1) {
            //4,7.etc

            lp.setMargins(12, 12, 12, 12);
        } else if (position % 3 == 0) {
            //5..etc

            lp.setMargins(24, 12, 12, 12);
        } else {
            lp.setMargins(12, 12, 24, 12);
        }

        return lp;
    }


    public static String getSignedMsg(String input) {
        try {
            /**************************************************************************************************************
             *                                    TODO                                                                     *
             *IMPORTANT : Signing of the message should be done in the backend to ensure that the keys are not compromised *
             *               and  can be modified on a regular  basis.                                                     *
             ***************************************************************************************************************/


            String privKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDNzBfgbTSNp62zSYmScf9pwyr77QCEjGRvyuvHaRQ9AsPqYvxIwstar4WESLIKN9tUvvzE4xJGRuy7J0sGnehRom93fSWtdck6V+MCATfxUOfrRJCNqPEEcNi1809dcz1qsmUfZegG34p/ARURJ3bHAaVvS3YdX8tAwcnHFGX2QLl9/eX02qAZWbxpBFojsAlzpkF1E6NkaIaIiuymvPN1EmQQmeibeSUJlx6juUgtF2K9eS6hbke0khA2Upy+0nA6PLGDnpVuQnFpXG0UxiojLMdlh/Rv0oCadd2exYdGB2CT3q/NGHtDYSKnLU1xBImvS92nPMHVmO7gauoxKSQfAgMBAAECggEBAKM1BoJ/WLw2jHSxDx9KtOolU4NzU4PK6yQVY6NDXD9+X+0UD0uM4ETNCi/8juW3ooO06zUhd66wNLG/2aontMR487lpUGYeETXp2SgP21PPe/2C5LjTkECbVeIGUZyk9cIWNEgQQ1CgG2/ZZeGy0GnGjnKS/9sPy1tR1DnDnZEKG2jANGeE6H7ouYChFVwP85UEOgiHv2fmidRdfk5KrNw4prVelpznTOthKx/Zi+7q9oGkEie/PmE4hdCO1ucXUtOVxU3QMOBL0QWIhyBlWhTeY6nt7ayX4MnIweRsu4CQlP0LsrihmSfsEcgpIRteEQzhG9TuC1CVHvzI6XH2iQECgYEA6teV0RSCSUO9AB64A9p5wgB5V9+SkaBF3lCPfmJbOboQs5Jl0U5F3Ph7IfW1OzZDtqPThRfy+yV6+XWu3fczG3GP2OFCOrPlesmGXPTvWAWbfowEkiQCkddrRFmypI6QR2ZBItrHXhzPfR9Br8RybBzrLuUv7yho0BGCJ/ZnZ18CgYEA4FadXsVJnJxK65akI8ihGJpu1KgVmrKo6zYfplw3bdzoFPbLRAmuc09J9aQcuAQaCBEfguHeVNJb2CTAv90dx7E3gXHCDbW4SUcyybbTBBG5ZUIz4Ltv4LnLITqFKxiTpiOc+vi8+XFfOvDtqyXfBVVegIbV+xUiaOYPS4ULO0ECgYA0e1VZ0lGDegXk3viUs+B+AIkdoDMrJDw5AJvwzJ5Celh9KPxkGC/4v/cUkcqcnvXm/RmqJr4AblHbKfeYV0Qun+RbvYuFfuqL1DmY0IwkiaxETZo/5phEa3XnYnxP1iRcMHfiCC6B08Jy3edaFnbTvmq4ojNiKQ+zYBZMQ/671QKBgEX8/6+3YRXI9N626pJ3XzrrwzP5FHRk1Ko9AnbGQky2JHmV3Shm1NQIooxOHN+T+AMYRHpyuQhBcIHoRXIWK9pHAYgS03WvgcTqv3+K2B5m4S4kD0dHcsnrbOH6/dzKGBY2+hyaSWqQ4iLjU2KXuBJT5d23Mz7YAxoy3Aa1hSGBAoGAHJgJe7f6PT5BgeDZbC7yw1RcLyH7uFwMaTTZCHcnrsvz1FHbWLI4N5lCd08RBzyygEtIofQXGsxpE6wwyDGlnPHBZXnQsUWIm+eYQ4bl0O5mAunPVU9VWl6P8ynCoBqehbjAyy34b59XJ92gxcdK73ybVbHyemocPU4Fj8sp48o=";
            // Get private key from String
            PrivateKey pk = loadPrivateKey(privKey);

            Signature sign = Signature.getInstance("SHA512withRSA");
            sign.initSign(pk);
            //System.out.println("TEST3");
            sign.update(input.getBytes());
            byte[] hashBytes = sign.sign();
            return bytesToHexString(hashBytes);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        Formatter formatter = new Formatter(sb);
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }
        formatter.close();
        return sb.toString();
    }


    static PrivateKey loadPrivateKey(String key64) {
        byte[] privkeybytes = Base64.decode(key64, Base64.NO_WRAP);
        KeyFactory fact;
        try {
            fact = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(privkeybytes);
            PrivateKey privateKey1 = fact.generatePrivate(privateSpec);
            return privateKey1;
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }


    public static void setMaxLength(int maxLength, EditText editText) {
        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(maxLength);
        editText.setFilters(filterArray);
    }


    public static boolean haveNetworkConnection(FragmentActivity fragmentActivity) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) fragmentActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public static void viewDocumentImages(FragmentActivity applicationContext, String imageUrl, @NotNull String imageType, String from) {
        if (!imageUrl.isEmpty()) {

            Intent intent = new Intent(applicationContext, ViewImage.class);
            intent.putExtra("imageUrl", imageUrl);
            intent.putExtra("imageType", imageType);
            intent.putExtra("from", from);
            applicationContext.startActivity(intent);
            applicationContext.overridePendingTransition(0, 0);
        } else {
            MessageUtils.showToastMessage(applicationContext, "Images not Uploaded");
        }
    }


    public static void logoutDialog(final AppCompatActivity appCompatActivity, final NavigationView navigationView) {
        try {

            final Dialog dialog = new Dialog(appCompatActivity);
            dialog.setContentView(R.layout.dialog_permissions);
            dialog.show();
            CustomTextView mTitleOfDialog = dialog.findViewById(R.id.permission_title);
            CustomTextView permission_message = dialog.findViewById(R.id.permission_message);
            CustomTextView dismiss = dialog.findViewById(R.id.btn_dismiss);
            CustomTextView logout = dialog.findViewById(R.id.btn_grand);

            dialog.getWindow().setWindowAnimations(R.style.UpDownDialogAnim);
            dialog.getWindow().setBackgroundDrawableResource(R.color.dialog_tans);
            dialog.getWindow().setGravity(Gravity.BOTTOM);

            mTitleOfDialog.setText(appCompatActivity.getString(R.string.logout));
            permission_message.setText(appCompatActivity.getString(R.string.sure_to_logout));
            logout.setText("Yes");
            dismiss.setText("No");
            //dialog.setCanceledOnTouchOutside(false);
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    navigationView.getMenu().getItem(0).setChecked(true);
                }
            });
            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                    navigationView.getMenu().getItem(0).setChecked(true);
                    return false;
                }
            });
            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    SessionManager.logoutUser(appCompatActivity);
                }
            });

            dismiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    navigationView.getMenu().getItem(0).setChecked(true);

                }
            });


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public static void callWebURL(FragmentActivity appCompatActivity, String url) {
        log(TAG, "callWebURL: " + url);
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(ContextCompat.getColor(appCompatActivity, R.color.colorPrimary));
        builder.addDefaultShareMenuItem();
        builder.setExitAnimations(appCompatActivity, R.anim.right_to_left_end, R.anim.left_to_right_end);
        builder.setStartAnimations(appCompatActivity, R.anim.left_to_right_start, R.anim.right_to_left_start);
        builder.setSecondaryToolbarColor(ContextCompat.getColor(appCompatActivity, R.color.colorPrimary));

        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(appCompatActivity, Uri.parse(url));
    }


    public static void log(String tag, String msg) {
        Log.d(TAG + "_" + tag, msg);
    }

    /*(1,'Block Level','Preinspection')
(2,'Block Level','HO Approval'),
(3,'Block Level','ADH Approval'),
(4,'','Affidavit'),
(9,'District Level','Superindent Approval'),
(10,'District Level','HO Tech Approval'),
(11,'District Level','District Head Approval'),
(12,'District Level','DD Approval'),
(14,'Head Office Level','AD Approval'),
(15,'Head Office Level','Director Approval'),
(16,'DMC Level','DMC Approval'),
(17,'Head Office Level','SLEC Approval'),
(18,'Work Order','Work Order Release'),
(19,'Work Inprogress','Quotation Upload'),
(20,'Work Inprogress','Invoice Upload'),
(21,'Work Inprogress','Basement Work Inspection'),
(22,'Work Inprogress','Frame Work Inspection'),
(23,'Work Inprogress','Joint Inspection'),
(24,'','Checker Approval'),
(25,'','HO Tech Approval'),
(26,'','JD/AD Approval'),
(27,'','Payment Release')*/
}
