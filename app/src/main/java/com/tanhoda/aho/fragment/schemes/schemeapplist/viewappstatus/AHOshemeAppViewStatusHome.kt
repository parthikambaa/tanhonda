package com.tanhoda.aho.fragment.schemes.schemeapplist.viewappstatus


import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.os.StrictMode
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson

import com.tanhoda.R
import com.tanhoda.aho.AHOActivity
import com.tanhoda.aho.fragment.schemes.schemeapplist.AHOSchemeAppList
import com.tanhoda.aho.fragment.schemes.schemeapplist.viewappstatus.app_details.AHOschemeviewAppdet
import com.tanhoda.aho.fragment.schemes.schemeapplist.viewappstatus.farmer_details.AHOschemeviewFarmerdet
import com.tanhoda.aho.fragment.schemes.schemeapplist.viewappstatus.land_details.AHOschemeviewLANDdet
import com.tanhoda.aho.fragment.schemes.schemeapplist.viewappstatus.viewschemePojo.AHOViewSchemeRES
import com.tanhoda.login_splash_screen.SplashActivity
import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import com.tanhoda.utils.Utils.log
import customs.CustomTextView
import kotlinx.android.synthetic.main.fragment_ahosheme_app_view_status_home.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AHOshemeAppViewStatusHome : Fragment() {

    var txt: CustomTextView? = null
    var txt1: CustomTextView? = null
    var txt2: CustomTextView? = null
    var dialog: Dialog? = null

    companion object {
        var ahoviewschemeRES: AHOViewSchemeRES? = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as AHOActivity).disableNavigationInAdoptionHome("Application Status")
        return inflater.inflate(R.layout.fragment_ahosheme_app_view_status_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)
        val newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())
        txt = CustomTextView(activity, null)
        txt1 = CustomTextView(activity, null)
        txt2 = CustomTextView(activity, null)


        /**
         * add the tabs in tab layout
         */

        aho_scheme_view_tablay?.addTab(aho_scheme_view_tablay?.newTab()!!, 0)
        aho_scheme_view_tablay?.addTab(aho_scheme_view_tablay?.newTab()!!, 1)
        aho_scheme_view_tablay?.addTab(aho_scheme_view_tablay?.newTab()!!, 2)

        initTabs()
        aho_scheme_view_tablay?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {

                when (tab?.position) {
                    0 -> {
                        FragmentCallUtils.passFragmentWithoutBackStatck(
                            activity,
                            AHOschemeviewAppdet(),
                            R.id.aho_scheme_view_container_body
                        )
                    }
                    1 -> {
                        FragmentCallUtils.passFragmentWithoutBackStatck(
                            activity,
                            AHOschemeviewFarmerdet(),
                            R.id.aho_scheme_view_container_body
                        )
                    }
                    2 -> {
                        FragmentCallUtils.passFragmentWithoutBackStatck(
                            activity,
                            AHOschemeviewLANDdet(),
                            R.id.aho_scheme_view_container_body
                        )
                    }
                }
            }
        })

        getSChemeApplViewdata()
    }

    private fun getSChemeApplViewdata() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["user_id"] = SessionManager.getUserId(activity)
            map["application_id"] = AHOSchemeAppList.ahoschemecurentAPP_ID!!
            map["user_scope_id"] = SessionManager.getuserSopeID(activity)

            val service = Utils.getInstance(activity)
            val callback = service.call_post("viewapplication", "Bearer " + SessionManager.getToken(activity), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    log("exceproeproper", "" + t.message)
                    MessageUtils.dismissDialog(dialog)
                    SplashActivity.snackBar = MessageUtils.showSnackBar(activity, aho_scheme_view_snackView, t.message)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    log("exceproeproper", "yes_00" )
                    try {
                        log("exceproeproper", "yes")
                        if (response.isSuccessful) {
                            ahoviewschemeRES = Gson().fromJson(response.body()?.string(), AHOViewSchemeRES::class.java)
                            if (ahoviewschemeRES != null) {
                                log("exceproeproper", "yes_11")
                                if (ahoviewschemeRES?.success!!) {
                                    if (ahoviewschemeRES?.data != null && ahoviewschemeRES?.data?.applications != null) {
                                        log("exceproeproper", "yes_22")
                                        FragmentCallUtils.passFragmentWithoutBackStatck(
                                            activity,
                                            AHOschemeviewAppdet(),
                                            R.id.aho_scheme_view_container_body
                                        )
                                    } else {
                                        SplashActivity.snackBar = MessageUtils.showSnackBar(
                                            activity,
                                            aho_scheme_view_snackView,
                                            ahoviewschemeRES?.message
                                        )
                                    }
                                } else {
                                    SplashActivity.snackBar = MessageUtils.showSnackBar(
                                        activity,
                                        aho_scheme_view_snackView,
                                        ahoviewschemeRES?.message
                                    )
                                }

                            } else {
                                SplashActivity.snackBar = MessageUtils.showSnackBar(
                                    activity,
                                    aho_scheme_view_snackView,
                                    getString(R.string.checkjsonformat)
                                )
                            }

                        } else {
                            SplashActivity.snackBar = MessageUtils.showSnackBar(
                                activity,
                                aho_scheme_view_snackView,
                                MessageUtils.setErrorMessage(response.code())
                            )

                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }

            })


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        MessageUtils.dismissSnackBar(SplashActivity.snackBar)
    }

    @SuppressLint("ResourceType")
    private fun initTabs() {
        try {

            txt?.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            txt?.gravity = Gravity.CENTER
            txt?.setText(getString(R.string.app_details))
            val tab = aho_scheme_view_tablay?.getTabAt(0)
            if (tab != null) {
                tab.setCustomView(txt)
            }
            txt1?.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            txt1?.gravity = Gravity.CENTER
            txt1?.setText(getString(R.string.farmer_details))
            val tab1 = aho_scheme_view_tablay?.getTabAt(1)
            if (tab1 != null) {
                tab1.setCustomView(txt1)
            }

            txt2?.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            txt2?.gravity = Gravity.CENTER
            txt2?.setText(getString(R.string.land_details))
            val tab2 = aho_scheme_view_tablay?.getTabAt(2)
            if (tab2 != null) {
                tab2.setCustomView(txt2)
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}
