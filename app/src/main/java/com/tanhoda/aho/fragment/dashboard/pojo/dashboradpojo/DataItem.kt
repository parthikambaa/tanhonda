package com.tanhoda.aho.fragment.dashboard.pojo.dashboradpojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class DataItem(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("stage_name")
	val stageName: String? = null,

	@field:SerializedName("count")
	val count: Int? = null
)