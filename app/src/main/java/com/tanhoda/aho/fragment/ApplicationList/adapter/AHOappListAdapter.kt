package com.tanhoda.aho.fragment.ApplicationList.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.tanhoda.R
import com.tanhoda.aho.fragment.ApplicationList.pojo.ApplicationsItem
import com.tanhoda.interfaces.OnclickPostionEvent
import customs.CustomTextView

class AHOappListAdapter(val context: Context, val applicationArrayList: ArrayList<ApplicationsItem?>?, val onclickEvent: OnclickPostionEvent) :
    RecyclerView.Adapter<AHOappListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AHOappListAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.adapter_aho_application_list,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return applicationArrayList?.size!!
    }

    override fun onBindViewHolder(holder: AHOappListAdapter.ViewHolder, position: Int) {
        try {
            holder.siNo?.text = (position + 1).toString()
            holder.category?.text = applicationArrayList!![position]?.appcategory?.category
            holder.category?.setSingleLine()
            holder.category?.ellipsize = TextUtils.TruncateAt.MARQUEE
            holder.category?.isSelected = true
            /*holder.componet?.text = applicationArrayList!![position]?.appcomponent?.componentName
            holder.componet?.setSingleLine()
            holder.componet?.ellipsize = TextUtils.TruncateAt.MARQUEE
            holder.componet?.isSelected = true*/
            holder.farmerName?.text = applicationArrayList!![position]?.appfarmer?.name
            holder.farmerName?.setSingleLine()
            holder.farmerName?.ellipsize = TextUtils.TruncateAt.MARQUEE
            holder.farmerName?.isSelected = true
            holder.scheme?.text =applicationArrayList!![position]?.appscheme?.shortName
            holder.scheme?.setSingleLine()
            holder.scheme?.ellipsize = TextUtils.TruncateAt.MARQUEE
            holder.scheme?.isSelected = true
            holder.layoutCompat?.setOnClickListener {
                try {
                    onclickEvent.OnclickPostionEvent("Jack", position.toString())
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val siNo = itemView?.findViewById<CustomTextView>(R.id.txt_sino_aho_app_list_adp)
        val scheme = itemView?.findViewById<CustomTextView>(R.id.txt_sheme_aho_app_list_adp)
        val category = itemView?.findViewById<CustomTextView>(R.id.txt_category_aho_app_list_adp)
        //val componet = itemView?.findViewById<CustomTextView>(R.id.txt_component_aho_app_list_adp)
        val farmerName = itemView?.findViewById<CustomTextView>(R.id.txt_farmer_name_aho_app_list_adp)
        val layoutCompat = itemView?.findViewById<LinearLayout>(R.id.aho_adap_linear)

    }
}