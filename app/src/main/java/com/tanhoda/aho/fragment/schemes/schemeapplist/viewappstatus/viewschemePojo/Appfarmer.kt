package com.tanhoda.aho.fragment.schemes.schemeapplist.viewappstatus.viewschemePojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Appfarmer(

	@field:SerializedName("income")
	val income: String? = null,

	@field:SerializedName("gender")
	val gender: String? = null,

	@field:SerializedName("created_at")
	val createdAt: Any? = null,

	@field:SerializedName("pan_no")
	val panNo: String? = null,

	@field:SerializedName("ration_copy")
	val rationCopy: String? = null,

	@field:SerializedName("farmer_type")
	val farmerType: String? = null,

	@field:SerializedName("userkey")
	val userkey: String? = null,

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("aadhaar_copy")
	val aadhaarCopy: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: Any? = null,

	@field:SerializedName("street")
	val street: String? = null,

	@field:SerializedName("branch_name")
	val branchName: String? = null,

	@field:SerializedName("bank_name")
	val bankName: String? = null,

	@field:SerializedName("social_status")
	val socialStatus: String? = null,

	@field:SerializedName("block")
	val block: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("village")
	val village: String? = null,

	@field:SerializedName("ulavanapp_id")
	val ulavanappId: String? = null,

	@field:SerializedName("aadhaar_id")
	val aadhaarId: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("pincode")
	val pincode: String? = null,

	@field:SerializedName("habitation")
	val habitation: String? = null,

	@field:SerializedName("bank_account_no")
	val bankAccountNo: String? = null,

	@field:SerializedName("profile_pic")
	val profilePic: String? = null,

	@field:SerializedName("active")
	val active: Int? = null,

	@field:SerializedName("panchayat")
	val panchayat: Any? = null,

	@field:SerializedName("gname")
	val gname: String? = null,

	@field:SerializedName("ifsc_code")
	val ifscCode: String? = null,

	@field:SerializedName("qualification")
	val qualification: String? = null,

	@field:SerializedName("hotnet_id")
	val hotnetId: String? = null,

	@field:SerializedName("district")
	val district: String? = null,

	@field:SerializedName("ration_card")
	val rationCard: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("house_no")
	val houseNo: String? = null,

	@field:SerializedName("phone_number")
	val phoneNumber: String? = null,

	@field:SerializedName("mobile_number")
	val mobileNumber: String? = null,

	@field:SerializedName("bank_passbook_copy")
	val bankPassbookCopy: String? = null,

	@field:SerializedName("age")
	val age: String? = null
)