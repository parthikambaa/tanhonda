package com.tanhoda.aho.fragment.details.land_details


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tanhoda.R
import com.tanhoda.aho.fragment.details.AHODetailsHome
import kotlinx.android.synthetic.main.fragment_aholand_details.*

class AHOLandDetails : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_aholand_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

    override fun onResume() {
        super.onResume()
        try {

            setTOFormappData()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setTOFormappData() {
        try {
            val data = AHODetailsHome.ahoappdetRES?.data?.applications?.appland!!
            val appitem = AHODetailsHome.ahoappdetRES?.data?.applications!!
            if (data.serveyNo != null) {
                custxtviw_aho_land_survey_number?.setText(data.serveyNo)
            }
            if (data.sourceOfIrrigation != null) {
                custxtviw_aho_land_survey_irragtion?.setText(data.sourceOfIrrigation)
            }
            if (data.totalArea != null) {
                custxtviw_aho_land_total_area?.setText(data.totalArea)
            }
            if (appitem.village != null) {
                custxtviw_aho_land_village?.setText(appitem.villagename?.village)
            }
            if (appitem.appblock != null) {
                custxtviw_aho_land_block?.setText(appitem.appblock.block)
            }
            if (appitem.appdistrict != null) {
                custxtviw_aho_land_district?.setText(appitem.appdistrict.district)
            }
            if (appitem.appstate != null) {
                custxtviw_aho_land_state?.setText(appitem.appstate.state)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


}
