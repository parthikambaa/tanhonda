package com.tanhoda.aho.fragment.dashboard.pojo.pridashboardPojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class District(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("districtcount")
	val districtcount: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)