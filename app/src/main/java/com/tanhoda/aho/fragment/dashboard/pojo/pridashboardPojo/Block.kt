package com.tanhoda.aho.fragment.dashboard.pojo.pridashboardPojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Block(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("blockcount")
	val blockcount: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)