package com.tanhoda.aho.fragment.details.appdetails.approve.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tanhoda.R
import com.tanhoda.interfaces.OnclickPostionEvent
import customs.CustomCheckBox

class AHOappDetViewDocumetnAdaper(
    val activity: FragmentActivity,
    val documentArrayList: ArrayList<com.tanhoda.ho.fragment.details.viewappstatusPojo.DocumentsItem?>?,
    val onclickPostionEvent: OnclickPostionEvent
) : RecyclerView.Adapter<AHOappDetViewDocumetnAdaper.ViewHolde>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolde {
        return ViewHolde(
            LayoutInflater.from(activity).inflate(R.layout.adapter_aho_app_det_approve_view_list, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return documentArrayList?.size!!
    }

    override fun onBindViewHolder(holder: ViewHolde, position: Int) {
        try {
            if (documentArrayList!![position]?.status.equals("Pending")) {
                holder.documentcheck?.text = documentArrayList!![position]?.documentName
                holder.documentcheck?.isChecked = false
                holder.documentcheck?.visibility = View.VISIBLE

            } else {

                holder.documentcheck?.visibility = View.GONE

            }
            holder.documentcheck?.setOnCheckedChangeListener { buttonView, isChecked ->
                documentArrayList[position]?.check = isChecked
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    class ViewHolde(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var documentcheck = itemView?.findViewById<CustomCheckBox>(R.id.ch_aho_app_det_approve)
    }
}