package com.tanhoda.aho.fragment.details.appdetails.approve.pojo

import com.google.gson.annotations.SerializedName

class AHOApprovalsserverPojo(
    @field:SerializedName("approval_value")
    var approval_value: String? = null,
    @field: SerializedName("approval_name")
    var approval_name: String? = null
)