package com.tanhoda.aho.fragment.details.appdetails


import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.GridLayout
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.tanhoda.R
import com.tanhoda.aho.fragment.dashboard.AHOPriDashbord
import com.tanhoda.aho.fragment.details.AHODetailsHome
import com.tanhoda.aho.fragment.details.AHODetailsHome.Companion.ahoappdetRES
import com.tanhoda.aho.fragment.details.appdetails.Inspections.AHOInspection
import com.tanhoda.aho.fragment.details.appdetails.approve.AHOappApprove
import com.tanhoda.aho.fragment.details.appdetails.approve.pojo.AHOAppDetDocumentServerPojo
import com.tanhoda.aho.fragment.details.appdetails.approve.pojo.AHOApprovalsserverPojo
import com.tanhoda.ho.fragment.details.app_details.adapter.HOQuotationAdapter
import com.tanhoda.login_splash_screen.SplashActivity
import com.tanhoda.utils.*
import com.tanhoda.utils.Utils.log
import customs.CustomTextEditView
import customs.CustomTextView
import kotlinx.android.synthetic.main.dialog_approved_views.*
import kotlinx.android.synthetic.main.dialog_dismiss_btn.*
import kotlinx.android.synthetic.main.dialog_permissions.*
import kotlinx.android.synthetic.main.fragment_ahoapp_details.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File


class AHOAppDetails : Fragment() {

    var popuptitle: CustomTextView? = null
    var popupremarks: CustomTextEditView? = null
    var popupcancel: CustomTextView? = null
    var popupsubmit: CustomTextView? = null
    var popupdialog: Dialog? = null
    var loaddialog: Dialog? = null

    /**affidavit**/
    var filenametextview: CustomTextView? = null
    var uploadaffidavitfile: Uri? = null

    /**quotation*/
    var recylQuoatationfiles: RecyclerView? = null
    var quotationFiles: ArrayList<File>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ahoapp_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loaddialog = Dialog(activity)

        log("sdsdsd", "" + ahoappdetRES?.data?.count + " statusId  " + ahoappdetRES?.data?.approval?.statusId)

        if (ahoappdetRES?.data?.count != 0) {
            aho_app_det_aprve_linear?.visibility = View.VISIBLE
            btn_ho_app_det_aprve_name?.text = ahoappdetRES?.data?.statusName
        } else {
            aho_app_det_aprve_linear?.visibility = View.GONE
        }



        if (ahoappdetRES?.data?.approval?.statusId == 4 || ahoappdetRES?.data?.approval?.statusId == 19) { //4,19 affidavit and quotation
            btn_ho_app_det_reject.visibility = View.GONE
            btn_ho_app_det_onhold.visibility = View.GONE
            btn_ho_app_det_aprve.visibility = View.VISIBLE

        } else {
            btn_ho_app_det_reject.visibility = View.VISIBLE
            btn_ho_app_det_onhold.visibility = View.VISIBLE
            btn_ho_app_det_aprve.visibility = View.VISIBLE
        }

        aho_app_det_aprve_linear.setOnClickListener {

            if (approver_layout_dialog.visibility == View.VISIBLE) {
                approver_layout_dialog.visibility = View.GONE
            } else {
                approver_layout_dialog.visibility = View.VISIBLE
            }
        }
        approver_layout_dialog.setOnClickListener {
            approver_layout_dialog.visibility = View.GONE
        }


        /**
         * set on clickListener
         */

        btn_ho_app_det_aprve?.setOnClickListener {
            try {
                val statusID = ahoappdetRES?.data?.approval?.statusId
                log("statusIDsdsd", "" + statusID)
//                val statusID=25
                when (statusID) {
                    1, 2, 3 -> FragmentCallUtils.passFragmentWithoutAnim(
                        activity,
                        AHOappApprove(), R.id.aho_container_body
                    )
                    4 -> affidafitPopup()

                    19 -> quatationApproved()

                    20, 21, 22, 23, 24 -> FragmentCallUtils.passFragmentWithoutAnim(
                        activity,
                        AHOInspection(),
                        R.id.aho_container_body
                    )


                    else -> FragmentCallUtils.passFragmentWithoutAnim(
                        activity,
                        AHOappApprove(), R.id.aho_container_body
                    )
                }

            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        btn_ho_app_det_reject?.setOnClickListener {
            try {
                remarksDialogdisplay(1)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        btn_ho_app_det_onhold?.setOnClickListener {
            try {
                remarksDialogdisplay(2)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

    }

    private fun quatationApproved() {
        try {

            val quatationDialog = Dialog(activity)
            quatationDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            quatationDialog.setContentView(R.layout.dialog_quataion_files_upload)
            quatationDialog.setCancelable(false)
            quatationDialog.window.setBackgroundDrawableResource(R.color.dialog_trans)
            quatationDialog.show()
            quatationDialog.window.setGravity(Gravity.BOTTOM)
            quatationDialog.window.setWindowAnimations(R.style.UpDownDialogAnim)
            val remarks = quatationDialog.findViewById<CustomTextEditView>(R.id.quatation_remarks)
            recylQuoatationfiles = quatationDialog.findViewById(R.id.recyler_quatation_file_name)


            val btn_cancel = quatationDialog.findViewById<CustomTextView>(R.id.btn_dismiss)
            val btn_approval = quatationDialog.findViewById<CustomTextView>(R.id.btn_grand)
            btn_approval.setText(resources.getString(R.string.upload))
            btn_cancel.setText(resources.getString(R.string.cancel))
            quatationDialog.findViewById<CustomTextView>(R.id.quotation_file_title)?.setOnClickListener {
                try {
                    getruntimepermissionforMedia(8)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            btn_cancel.setOnClickListener {
                try {
                    if (quatationDialog != null && quatationDialog.isShowing) {
                        quatationDialog.dismiss()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            btn_approval.setOnClickListener {
                try {
                    if (remarks.text.isEmpty()) {
                        MessageUtils.showSnackBar(activity, recylQuoatationfiles, "Remarks Can't be Empty")
                    } else {
                        if (quotationFiles?.size == 0) {
                            MessageUtils.showSnackBar(activity, recylQuoatationfiles, "Quotations File Can't Be Empty")
                        } else {
                            try {

                                loaddialog = MessageUtils.showDialog(activity)
                                val hashMap = HashMap<String, Any>()
                                val documentList = ArrayList<AHOAppDetDocumentServerPojo>()
                                val approvalsList = ArrayList<AHOApprovalsserverPojo>()

                                for (item in ahoappdetRES?.data?.documents!!) {
                                    if (item?.check!!) {
                                        log("check", item?.documentName)
                                        val documentserveritem =
                                            AHOAppDetDocumentServerPojo(item.id.toString(), item.documentName)
                                        documentList.add(documentserveritem)
                                    }
                                }


                                for (item in ahoappdetRES?.data?.approvals!!) {
                                    log("check", item?.applicationStatus)
                                    val approvals = AHOApprovalsserverPojo(item?.id.toString(), item?.applicationStatus)
                                    approvalsList.add(approvals)
                                }
                                hashMap["approvals"] = approvalsList
                                hashMap["documents"] = documentList
                                hashMap["user_id"] = SessionManager.getUserId(activity).toInt()
                                hashMap["role"] = SessionManager.getuserSopeID(activity).toInt()
                                hashMap["block"] = SessionManager.getBlockId(activity).toInt()
                                hashMap["district"] = SessionManager.getdistrictID(activity).toInt()
                                hashMap["application_id"] = ahoappdetRES?.data?.applications?.id!!
                                hashMap["approval_id"] = ahoappdetRES?.data?.approval?.id!!
                                hashMap["remarks"] = remarks?.text.toString()

                                val quotationImageParts = ArrayList<MultipartBody.Part>()
                                quotationFiles?.forEachIndexed { index, file ->
                                    val requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file)
                                    quotationImageParts.add(
                                        MultipartBody.Part.createFormData(
                                            "quotation[]",
                                            file.name,
                                            requestBody
                                        )
                                    )
                                }


                                val service = Utils.getInstance(activity)
                                val callback = service.call_postimage(
                                    "Ahoapproval",
                                    "Bearer " + SessionManager.getToken(activity),
                                    hashMap, quotationImageParts

                                )
                                callback.enqueue(object : Callback<ResponseBody> {
                                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                        MessageUtils.dismissDialog(loaddialog)
                                        val msg = MessageUtils.setFailurerMessage(activity, t.message)
                                        MessageUtils.showSnackBar(activity, recylQuoatationfiles, msg)
                                    }

                                    override fun onResponse(
                                        call: Call<ResponseBody>,
                                        response: Response<ResponseBody>
                                    ) {
                                        MessageUtils.dismissDialog(loaddialog)
                                        try {
                                            if (response.isSuccessful) {
                                                if (quatationDialog != null && quatationDialog.isShowing) {
                                                    quatationDialog.dismiss()
                                                }
                                                val obj = JSONObject(response.body()?.string())
                                                val success = obj.getBoolean("success")
                                                val message = obj.getString("message")
                                                if (success) {

                                                    MessageUtils.showToastMessage(activity, message)
                                                    FragmentCallUtils.passFragmentWithoutBackStatck(
                                                        activity,
                                                        AHOPriDashbord(),
                                                        R.id.aho_container_body
                                                    )

                                                } else {

                                                    MessageUtils.showSnackBar(activity, recylQuoatationfiles, message)
                                                }

                                            } else {
                                                val msg = MessageUtils.setErrorMessage(response.code())
                                                MessageUtils.showSnackBar(activity, recylQuoatationfiles, msg)
                                            }

                                        } catch (ex: Exception) {
                                            ex.printStackTrace()
                                        }
                                    }

                                })


                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }

                        }
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    private fun affidafitPopup() {
        try {
            val affidravitDIALOG = Dialog(activity)
            affidravitDIALOG.requestWindowFeature(Window.FEATURE_NO_TITLE)
            affidravitDIALOG.setContentView(R.layout.dialog_aho_affidavitupload)
            affidravitDIALOG.setCancelable(false)
            affidravitDIALOG.window.setBackgroundDrawableResource(R.color.dialog_trans)
            affidravitDIALOG.window.setGravity(Gravity.BOTTOM)
            affidravitDIALOG.show()
            affidravitDIALOG.window.setWindowAnimations(R.style.UpDownDialogAnim)
            /**
             * set onclick Listener
             */

            val remarks = affidravitDIALOG.findViewById<CustomTextEditView>(R.id.aho_affidavit_remarks)
            filenametextview = affidravitDIALOG.findViewById<CustomTextView>(R.id.aho_affidavit_file_name)
            var affidavit_snack_view = affidravitDIALOG.findViewById<CardView>(R.id.affidavit_snack_view)
            val cancel = affidravitDIALOG.findViewById<CustomTextView>(R.id.btn_dismiss)
            val upload = affidravitDIALOG.findViewById<CustomTextView>(R.id.btn_grand)
            upload.text = resources.getString(R.string.upload)
            cancel.text = resources.getString(R.string.cancel)

            cancel.setOnClickListener {
                try {
                    if (affidravitDIALOG != null && affidravitDIALOG.isShowing) {
                        affidravitDIALOG.dismiss()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            filenametextview?.setOnClickListener {
                try {

                    getruntimepermissionforMedia(7)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }


            upload.setOnClickListener {
                try {
                    /*      if (remarks.text.isEmpty()) {
                              MessageUtils.showSnackBar(activity, affidavit_snack_view, "remarks Can't  be Empty")
                          } else {*/
                    if (uploadaffidavitfile != null) {
                        try {
                            loaddialog = MessageUtils.showDialog(activity)

                            val hashMap = HashMap<String, Any>()
                            hashMap["user_id"] = SessionManager.getUserId(activity).toInt()
                            hashMap["user_scope_id"] = SessionManager.getuserSopeID(activity).toInt()
                            hashMap["application_id"] = ahoappdetRES?.data?.applications?.id!!
                            hashMap["block"] = SessionManager.getBlockId(activity).toInt()
                            hashMap["district"] = SessionManager.getdistrictID(activity).toInt()
                            hashMap["approval_id"] = ahoappdetRES?.data?.approval?.id!!
                            hashMap["remarks"] = remarks?.text.toString()

                            val requestBody = RequestBody.create(
                                MediaType.parse("multipart/form-data"),
                                CompressFile.getCompressedImageFile(
                                    File(
                                        PathUtil.getPath(
                                            activity,
                                            uploadaffidavitfile
                                        )
                                    ), activity
                                )
                            )

                            val filepart = MultipartBody.Part.createFormData(
                                "affidavit_image",
                                File(PathUtil.getPath(activity, uploadaffidavitfile)).name,
                                requestBody
                            )

                            val service = Utils.getInstance(activity)
                            val callback = service.call_post(
                                "Affidavitupload",
                                "Bearer " + SessionManager.getToken(activity),
                                hashMap,
                                filepart
                            )
                            callback.enqueue(object : Callback<ResponseBody> {
                                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                    MessageUtils.dismissDialog(loaddialog)
                                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                                    MessageUtils.showSnackBar(activity, affidavit_snack_view, msg)
                                }

                                override fun onResponse(
                                    call: Call<ResponseBody>,
                                    response: Response<ResponseBody>
                                ) {
                                    MessageUtils.dismissDialog(loaddialog)
                                    try {
                                        if (response.isSuccessful) {
                                            val obj = JSONObject(response.body()?.string())
                                            val success = obj.getBoolean("success")
                                            val msg = obj.getString("message")
                                            if (success) {
                                                if (affidravitDIALOG != null && affidravitDIALOG.isShowing) {
                                                    affidravitDIALOG.dismiss()
                                                }
                                                MessageUtils.showToastMessage(activity, msg)
                                                FragmentCallUtils.passFragmentWithoutBackStatck(
                                                    activity,
                                                    AHOPriDashbord(),
                                                    R.id.aho_container_body
                                                )
                                            } else {
                                                MessageUtils.showSnackBar(activity, affidavit_snack_view, msg)
                                            }
                                        } else {

                                            val msg = MessageUtils.setErrorMessage(response.code())
                                            MessageUtils.showSnackBar(activity, affidavit_snack_view, msg)
                                        }
                                    } catch (ex: Exception) {
                                        ex.printStackTrace()
                                    }
                                }

                            })


                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }


                    } else {
                        MessageUtils.showSnackBar(activity, affidavit_snack_view, "Affidavit File Can't Be Empty")
                    }
//                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    private fun getruntimepermissionforMedia(i: Int) {
        try {
            if (
                ContextCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED

            ) {

                val dialog = Dialog(activity)
                dialog.setContentView(R.layout.dialog_permissions)
                dialog.setCancelable(false)
                dialog.window.setBackgroundDrawableResource(R.color.dialog_tans)
                dialog.window.setGravity(Gravity.BOTTOM)
                dialog.show()
                dialog.window.setWindowAnimations(R.style.UpDownDialogAnim)
                dialog.btn_grand.visibility = View.VISIBLE
                dialog.permission_title.text = "Permission Required"
                dialog.permission_message.text = "CAMERA\n" +
                        "The App needs access to the camera to click your land Information\n" +
                        "\n" +
                        "STORAGE\n" +
                        "The App accesses to your document verification"

                dialog.btn_dismiss.setOnClickListener {
                    dialog.dismiss()
//                        token?.cancelPermissionRequest()
                }
                dialog.btn_grand.setOnClickListener {
                    dialog.dismiss()
                    validatePermissions(1, i)
                }

            } else {
                val intent = Intent()
                intent.type = "*/*"
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Files"), i)

            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun validatePermissions(isShow: Int, i: Int) {
        Dexter.withActivity(activity)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (isShow == 1 && report?.areAllPermissionsGranted()!!) {
                        val intent = Intent()
                        intent.type = "*/*"
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                        intent.action = Intent.ACTION_GET_CONTENT
                        startActivityForResult(Intent.createChooser(intent, "Select Files"), i)

                    }

                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?, token: PermissionToken?
                ) {
                    log("permissions", "denied")
                    token?.continuePermissionRequest()
                }

            })
            .check()

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        log("selectFile", "" + requestCode.toString())
        if (requestCode == 7 && resultCode == Activity.RESULT_OK && data != null) {
            log("selectFile", "" + data?.data.toString())
            uploadaffidavitfile = data.data
            filenametextview?.text = File(PathUtil.getPath(activity, uploadaffidavitfile)).name
        } else if (requestCode == 8 && resultCode == Activity.RESULT_OK && data != null) {
            quotationFiles = ArrayList()
            if (data.clipData != null) {
                try {
                    for (i: Int in 0 until data.clipData.itemCount) {
                        val file = CompressFile.getCompressedImageFile(
                            File(
                                PathUtil.getPath(
                                    activity,
                                    data.clipData.getItemAt(i).uri
                                )
                            ), activity
                        )
                        log("select Files ", "" + i + "\t" + file.name)
                        quotationFiles?.add(file)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

            } else {
                val file = CompressFile.getCompressedImageFile(File(PathUtil.getPath(activity, data.data)), activity)
                quotationFiles?.add(file)

            }
            log("select Files11", "" + quotationFiles?.size)
            val adapter = HOQuotationAdapter(activity!!, quotationFiles!!)
            recylQuoatationfiles?.layoutManager = GridLayoutManager(activity, 4, GridLayout.VERTICAL, false)
            recylQuoatationfiles?.adapter = adapter
        }
    }


    private fun remarksDialogdisplay(i: Int) {
        try {
            popupdialog = Dialog(activity)
            popupdialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            popupdialog?.setContentView(R.layout.dialog_remarksfr_reject)
            popupdialog?.setCancelable(false)
            popupdialog?.window?.setBackgroundDrawableResource(R.color.dialog_trans)
            popupdialog?.window?.setGravity(Gravity.BOTTOM)
            popupdialog?.window?.setWindowAnimations(R.style.UpDownDialogAnim)
            popupdialog?.show()
            popuptitle = popupdialog?.findViewById(R.id.aho_app_det_title_diag)
            popupcancel = popupdialog?.findViewById(R.id.btn_dismiss)
            popupsubmit = popupdialog?.findViewById(R.id.btn_grand)
            popupremarks = popupdialog?.findViewById(R.id.aho_app_det_remarks_dig)
            popupsubmit?.text = resources.getString(R.string.submit)
            popupsubmit?.visibility = View.VISIBLE
            if (i == 1) {
                popuptitle?.text = getString(R.string.reject_form)

            } else {
                popuptitle?.text = getString(R.string.onhold_form)
            }
            popupcancel?.setOnClickListener {
                try {
                    if (popupdialog != null && popupdialog?.isShowing!!) {
                        popupdialog?.dismiss()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            popupsubmit?.setOnClickListener {
                try {

                    if (Utils.haveNetworkConnection(activity)) {
                        if (popupremarks?.text.toString().isEmpty()) {
                            SplashActivity.snackBar =
                                MessageUtils.showSnackBar(activity, popuptitle, getString(R.string.remarksvalidmsg))
                        } else {

                            sendtoUpdateREmarksinserver()
                        }
                    } else {
                        SplashActivity.snackBar =
                            MessageUtils.showSnackBar(activity, popuptitle, getString(R.string.check_internet))
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun sendtoUpdateREmarksinserver() {
        try {
            loaddialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["user_id"] = SessionManager.getUserId(activity)
            map["application_id"] = AHODetailsHome?.ahoappdetRES?.data?.applications?.id!!
            if (popuptitle?.text.toString() == getString(R.string.reject_form)) {
                map["submit_button"] = "Reject"
                map["status"] = "Rejected"
            } else {
                map["submit_button"] = "ON Hold"
                map["status"] = "Holded"
            }
            map["remarks"] = popupremarks?.text.toString()
            map["approval_id"] = ahoappdetRES?.data?.approval?.id!!
            val approvalsList = ArrayList<AHOApprovalsserverPojo>()
            for (item in AHODetailsHome.ahoappdetRES?.data?.approvals!!) {
                log("check", item?.applicationStatus)
                val approvals = AHOApprovalsserverPojo(item?.id.toString(), item?.applicationStatus)
                approvalsList.add(approvals)
            }
            map["approval_id"] = approvalsList

            val calinterface = Utils.getInstance(activity)
            val callback =
                calinterface?.call_post("apprejectonhold", "Bearer " + SessionManager.getToken(activity), map)
            callback?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(loaddialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    MessageUtils.showSnackBar(activity, popuptitle, msg)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(loaddialog)

                    try {
                        if (response.isSuccessful) {
                            val obj = JSONObject(response.body()?.string())
                            val success = obj.getBoolean("success")
                            val message = obj.getString("message")
                            if (success) {
                                if (popupdialog != null && popupdialog?.isShowing!!) {
                                    popupdialog?.dismiss()
                                }
                                MessageUtils.showToastMessage(activity, message)
                            } else {
                                MessageUtils.showSnackBar(activity, popuptitle, message)
                            }
                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            MessageUtils.showSnackBar(activity, popuptitle, msg)
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }


                }

            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        try {
            setTOFormappData()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setTOFormappData() {
        try {
//            val data = ahoappListRES?.data?.applications!![ahocurrentclickposition!!.toInt()]!!
            val data = ahoappdetRES?.data?.applications!!

            custxtviw_aho_app_det_scheme?.setText(data.appscheme?.schemeName!!)
            custxtviw_aho_app_det_category?.setText(data.appcategory?.category!!)
            custxtviw_aho_app_det_component?.setText(data.appcomponent?.componentName!!)
            if (data.id != null) {
                custxtviw_aho_app_det_app_id?.setText(data.applicationId.toString())
            }

            if (data.applicationStatus != null) {
                custxtviw_aho_app_det_app_status?.setText(data.applicationStatus)
            }
            if (data.waitingFor != null) {
                custxtviw_aho_app_det_waitingfor?.setText(data.waitingFor)
            }

            /**
             * Proposal Details
             */

            if (data.areaProposed != null) {
                custxtviw_aho_app_det_area_proposed?.setText(data.areaProposed.toString())
            }
            if (data.proposedCrop != null) {
                custxtviw_aho_app_det_proposed_crop?.setText(data.proposedCrop.toString())
            }
            if (data.estimateCost != null) {
                custxtviw_aho_app_det_estimated_amt?.setText(data.estimateCost.toString())
            }
            if (data.govtSubsidy != null) {
                custxtviw_aho_app_det_whetherany?.setText(data.govtSubsidy.toString())
            }
            if (data.relevant != null) {
                custxtviw_aho_app_det_remarks?.setText(data.relevant)
            }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


}
