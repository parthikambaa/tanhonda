package com.tanhoda.aho.fragment.schemes


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.gson.Gson

import com.tanhoda.R
import com.tanhoda.aho.AHOActivity
import com.tanhoda.aho.fragment.schemes.adapter.AHOSchemeAdpter
import com.tanhoda.aho.fragment.schemes.pojo.schemepojo.AHOSchemeRES
import com.tanhoda.aho.fragment.schemes.schemeapplist.AHOSchemeAppList
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.login_splash_screen.SplashActivity
import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_ahoschemes.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AHOSchemes : Fragment(),OnclickPostionEvent {


    var dialog:Dialog?=null
    var schemeRES:AHOSchemeRES?=null
    companion object {
        var ahoschemeid=""
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as AHOActivity).disableNavigationInAdoptionHome("Schemes")
        return inflater.inflate(R.layout.fragment_ahoschemes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog =Dialog(activity)
        /**
         * getschemeList
         */
        when {
            Utils.haveNetworkConnection(activity)->{
                try {
                    getschemeList()
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
            else ->{
                aho_scheme_empty_list?.visibility =View.VISIBLE
                aho_scheme_empty_list?.text =getString(R.string.check_internet)
                MessageUtils.showSnackBarAction(activity,aho_scheme_snack_view,getString(R.string.check_internet))
            }
        }

    }

    private fun getschemeList() {
        try{
            dialog =MessageUtils.showDialog(activity)
            val map = HashMap<String,Any>()
            map["user_id"] = SessionManager.getUserId(activity)
//            map["user_id"] = 8
            map["district"] =SessionManager.getdistrictID(activity)
            val service = Utils.getInstance(activity)
            val callback =service.call_post("schemes","Bearer "+SessionManager.getToken(activity),map)
            callback.enqueue(object : Callback<ResponseBody>{
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg =MessageUtils.setFailurerMessage(activity,t.message)
                    MessageUtils.showSnackBar(activity,aho_scheme_snack_view,msg)
                    aho_scheme_empty_list?.visibility =View.VISIBLE
                    aho_scheme_empty_list?.text =msg
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try{
                        if(response.isSuccessful){
                            schemeRES =Gson().fromJson(response.body()?.string(),AHOSchemeRES::class.java)
                            if(schemeRES!=null){
                                if(schemeRES?.success!!){
                                    if(schemeRES?.data!=null && schemeRES?.data?.schemes?.size!=0){
                                        recy_aho_scheme_list?.visibility =View.VISIBLE
                                        aho_scheme_list_title_linear?.visibility =View.VISIBLE
                                        aho_scheme_empty_list?.visibility =View.GONE
                                        setToAdapter()
                                    }else{
                                        aho_scheme_empty_list?.visibility =View.VISIBLE
                                        aho_scheme_empty_list?.text = schemeRES?.message
                                        recy_aho_scheme_list?.visibility =View.GONE
                                        aho_scheme_list_title_linear?.visibility =View.GONE
                                    }

                                }else{
                                    MessageUtils.showSnackBar(activity,aho_scheme_snack_view,schemeRES?.message)
                                }
                            }else{
                                MessageUtils.showSnackBar(activity,aho_scheme_snack_view,getString(R.string.checkjsonformat))
                            }

                        }else {
                            val  msg = MessageUtils.setErrorMessage(response.code())
                            aho_scheme_empty_list?.visibility =View.VISIBLE
                            aho_scheme_empty_list?.text =msg
                            MessageUtils.showSnackBar(activity,aho_scheme_snack_view,msg)
                        }

                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

            })

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setToAdapter() {
        try {
            val ahoschemeadpter = AHOSchemeAdpter(activity!!,schemeRES?.data?.schemes,this)
            recy_aho_scheme_list?.layoutManager = LinearLayoutManager(activity,LinearLayout.VERTICAL,false)
            recy_aho_scheme_list?.adapter = ahoschemeadpter

        }catch (ex:Exception){ex.printStackTrace()}
    }
    override fun OnclickPostionEvent(name: String, postion: String) {
        try {
            ahoschemeid = schemeRES?.data?.schemes!![postion.toInt()]?.id.toString()
            FragmentCallUtils.passFragmentWithoutAnim(activity, AHOSchemeAppList(),R.id.aho_container_body)
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        MessageUtils.dismissSnackBar(SplashActivity.snackBar)
    }
}
