package com.tanhoda.aho.fragment.details.appdetails.approve

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.tanhoda.R
import com.tanhoda.aho.AHOActivity
import com.tanhoda.aho.fragment.dashboard.AHOPriDashbord
import com.tanhoda.aho.fragment.details.AHODetailsHome
import com.tanhoda.aho.fragment.details.appdetails.approve.adapter.AHOappDetViewDocumetnAdaper
import com.tanhoda.aho.fragment.details.appdetails.approve.adapter.AHOapprovedynamicAdpter
import com.tanhoda.aho.fragment.details.appdetails.approve.pojo.AHOAcompayingofficedynamic
import com.tanhoda.aho.fragment.details.appdetails.approve.pojo.AHOAppDetDocumentServerPojo
import com.tanhoda.aho.fragment.details.appdetails.approve.pojo.AHOApprovalsserverPojo
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.utils.*
import com.tanhoda.utils.Utils.log
import kotlinx.android.synthetic.main.aho_home_fragment.*
import kotlinx.android.synthetic.main.bottom_cancel_approve.*
import kotlinx.android.synthetic.main.dialog_dismiss_btn.*
import kotlinx.android.synthetic.main.dialog_permissions.*
import kotlinx.android.synthetic.main.dialog_pic_img.*
import kotlinx.android.synthetic.main.item_dynamic.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class AHOappApprove : Fragment(), View.OnClickListener, OnclickPostionEvent {


    var GALLERY: Int = 0
    var CAMERA: Int = 1
    var IMAGE_DIRECTORY: String = "/TanHoda/Land"
    var path: String = ""
    var TAG = "AHO_FRAGMENT"
    var extraofficerArrayList = ArrayList<AHOAcompayingofficedynamic>()
    var extraofficeradapter: AHOapprovedynamicAdpter? = null
    var dialog:Dialog?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //validatePermissions(0)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        (activity as AHOActivity).disableNavigationInAdoptionHome(getString(R.string.approve))
        return inflater.inflate(R.layout.aho_home_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
            dialog = Dialog(activity)
        btn_cancel.setOnClickListener(this)
        btn_approval.setOnClickListener(this)
        aho_app_img_select.setOnClickListener(this)

        ins_add_more?.setOnClickListener {
            try {
                val officername = edt_comer_name?.text.toString()
                val department = edt_dept_name?.text.toString()
                val designation = edt_comer_designation?.text.toString()
                val remarks = edt_comer_remark?.text.toString()
                if (validateextraofficerdata(officername, department, designation, remarks)) {
                    val officeritem = AHOAcompayingofficedynamic(officername, department, designation, remarks)
                    extraofficerArrayList.add(officeritem)
                    aho_approve_dynamic_view?.layoutManager =
                            LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
                    extraofficeradapter = AHOapprovedynamicAdpter(activity!!, extraofficerArrayList, this)
                    aho_approve_dynamic_view?.adapter = extraofficeradapter
                    setCardViewempty()
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }


        /**
         * getDynamicApprovelDocuments
         *
         */
        if (AHODetailsHome.ahoappdetRES?.data?.approval?.statusId == 2 || AHODetailsHome.ahoappdetRES?.data?.approval?.statusId == 1 ||
            AHODetailsHome.ahoappdetRES?.data?.approval?.statusId == 3
        ) {
            setToadapter()
        }

        if (AHODetailsHome.ahoappdetRES?.data?.inspectionimage == 0) {
            aho_app_land_img_layout?.visibility = View.VISIBLE
        } else {
            aho_app_land_img_layout?.visibility = View.GONE
        }


    }

    private fun setToadapter() {
        try {
            val adapter = AHOappDetViewDocumetnAdaper(activity!!, AHODetailsHome.ahoappdetRES?.data?.documents, this)
            recyl_aho_app_det_approvelist?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            recyl_aho_app_det_approvelist?.adapter = adapter

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setCardViewempty() {
        try {
            edt_comer_name?.setText("")
            edt_comer_designation?.setText("")
            edt_comer_remark?.setText("")
            edt_dept_name?.setText("")
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun validateextraofficerdata(
        officername: String,
        department: String,
        designation: String,
        remarks: String
    ): Boolean {
        try {
            if (officername.isEmpty()) {
                MessageUtils.showSnackBar(activity, aho_app_approv_snackview, getString(R.string.enter_ooficername))
                return false
            } else if (department.isEmpty()) {
                MessageUtils.showSnackBar(activity, aho_app_approv_snackview, getString(R.string.enter_department))
                return false
            } else if (designation.isEmpty()) {
                MessageUtils.showSnackBar(activity, aho_app_approv_snackview, getString(R.string.enter_designation))
                return false
            } else if (remarks.isEmpty()) {
                MessageUtils.showSnackBar(activity, aho_app_approv_snackview, getString(R.string.enter_remark))
                return false
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return true
    }


    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.btn_cancel -> {
                    activity!!.onBackPressed()
                }

                R.id.btn_approval -> {

                    if(aho_app_land_img_layout?.visibility==View.VISIBLE){
                        if(path.isEmpty()){
                            MessageUtils.showSnackBar(activity,aho_app_approv_snackview,getString(R.string.select_image))
                        }else{
                            uploadTOserver()

                        }
                    } else{ 
                        uploadTOserver()
                    }


                }
                R.id.aho_app_img_select -> {
                    if (
                        ContextCompat.checkSelfPermission(
                            activity!!,
                            Manifest.permission.CAMERA
                        ) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(
                            activity!!,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(
                            activity!!,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ) != PackageManager.PERMISSION_GRANTED

                    ) {

                        val dialog = Dialog(activity)
                        dialog.setContentView(R.layout.dialog_permissions)
                        dialog.setCancelable(false)
                        dialog.window.setBackgroundDrawableResource(R.color.dialog_tans)
                        dialog.window.setGravity(Gravity.BOTTOM)
                        dialog.show()
                        dialog.window.setWindowAnimations(R.style.UpDownDialogAnim)
                        dialog.btn_grand.visibility = View.VISIBLE
                        dialog.permission_title.text = "Permission Required"
                        dialog.permission_message.text = "CAMERA\n" +
                                "The App needs access to the camera to click your land Information\n" +
                                "\n" +
                                "STORAGE\n" +
                                "The App accesses to your document verification"

                        dialog.btn_dismiss.setOnClickListener {
                            dialog.dismiss()
//                        token?.cancelPermissionRequest()
                        }
                        dialog.btn_grand.setOnClickListener {
                            dialog.dismiss()
                            validatePermissions(1)
                        }

                    } else {
                        showImagePickDialog()
                    }

                }
            }
        }
    }

    private fun uploadTOserver() {
        try{



            dialog =MessageUtils.showDialog(activity)
            val strRemarks = edt_remarks.text.toString().trim()
            val documentList=ArrayList<AHOAppDetDocumentServerPojo>()
            val approvalsList=ArrayList<AHOApprovalsserverPojo>()
            val approvalsmap =HashMap<String,Any>()
            val documentsmap =HashMap<String,Any>()

            for(item in AHODetailsHome.ahoappdetRES?.data?.documents!!){
                if(item?.check!!){
                    log("check",item?.documentName)
                    val documentserveritem = AHOAppDetDocumentServerPojo(item.id.toString(),item.documentName)
                    documentList.add(documentserveritem)
                }
            }
            documentsmap.put("document", documentList)

            for(item in AHODetailsHome.ahoappdetRES?.data?.approvals!!){
                log("check",item?.applicationStatus)
                val approvals = AHOApprovalsserverPojo(item?.id.toString(),item?.applicationStatus)
                approvalsList.add(approvals)
            }

            approvalsmap["approval"] = approvalsList

            val hashMap = HashMap<String, Any>()
            log("USER_ID",""+SessionManager.getUserId(activity).toInt())
            hashMap["user_id"] = SessionManager.getUserId(activity).toInt()
            hashMap["role"] = SessionManager.getuserSopeID(activity).toInt()
            hashMap["block"] = SessionManager.getBlockId(activity).toInt()
            hashMap["district"] =SessionManager.getdistrictID(activity).toInt()
            hashMap["application_id"] =AHODetailsHome.ahoappdetRES?.data?.applications?.id!!
            hashMap["approvals"] =approvalsList
            hashMap["documents"] =documentList


            val file = CompressFile.getCompressedImageFile(File(path),activity)

            var landimagePart :MultipartBody.Part ?=null
            if(file!=null ){
                val fileSize = file.length()/1024
                log("FileSize",""+fileSize)
                if(fileSize <=300 ){
                    val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
                    // MultipartBody.Part is used to send also the actual file name
                    landimagePart = MultipartBody.Part.createFormData("land_image", file.getName(), requestFile)

                }else{
                    MessageUtils.showSnackBar(
                        activity, aho_app_approv_snackview,(fileSize/1024).toString()+"Too Large"
                    )
                }


            }else{
                val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "")
                // MultipartBody.Part is used to send also the actual file name
                landimagePart = MultipartBody.Part.createFormData("land_image", "", requestFile)

            }
            // add another part within the multipart request

            hashMap["remarks"] = strRemarks

            log("hashMapsd", "" + hashMap)
            val service =Utils.getInstance(activity)
            val callback = service.call_post("Ahoapproval","Bearer "+SessionManager.getToken(activity),hashMap,landimagePart!!)

            callback.enqueue(object: Callback<ResponseBody>{
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val  msg =MessageUtils.setFailurerMessage(activity,t.message)
                    MessageUtils.showSnackBar(activity,aho_app_approv_snackview,msg)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try{
                        if(response.isSuccessful){

                            val obj =JSONObject(response.body()?.string())
                            val success =obj.getBoolean("success")
                            val message = obj.getString("message")
                            if(success){

                                MessageUtils.showToastMessage(activity,message)
                                FragmentCallUtils.passFragmentWithoutBackStatck(activity,AHOPriDashbord(),R.id.aho_container_body)

                            }else{

                                MessageUtils.showSnackBar(activity,aho_app_approv_snackview,message)
                            }

                        }else{
                            val msg =MessageUtils.setErrorMessage(response.code())
                            MessageUtils.showSnackBar(activity,aho_app_approv_snackview,msg)
                        }
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

            })

        } catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun showImagePickDialog() {

        val dialog = Dialog(activity)
        dialog.setContentView(R.layout.dialog_pic_img)
        dialog.setCancelable(false)
        dialog.window.setBackgroundDrawableResource(R.color.dialog_tans)
        dialog.window.setGravity(Gravity.BOTTOM)
        dialog.show()
        dialog.window.setWindowAnimations(R.style.UpDownDialogAnim)
        dialog.btn_dismiss.setOnClickListener {
            dialogDismiss(dialog)
        }

        dialog.gallery.setOnClickListener {
            dialogDismiss(dialog)

            choosePhotoFromGallary()
        }
        dialog.camera.setOnClickListener {
            dialogDismiss(dialog)
            takePhotoFromCamera()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_CANCELED) {
            aho_app_lay_file_name.visibility = View.GONE
            return
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data.data
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(activity!!.getContentResolver(), contentURI)
                    path = saveImage(bitmap)
                    log("pathsdsd", "" + path)
                    aho_app_txt_img_file_name.text = File(path).name
                    Toast.makeText(activity, "Image Saved!", Toast.LENGTH_SHORT).show()
                    aho_app_img_land.setImageBitmap(bitmap)
                    aho_app_lay_file_name.visibility = View.VISIBLE
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(activity, "Failed!", Toast.LENGTH_SHORT).show()
                }

            }

        } else if (requestCode == CAMERA) {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            aho_app_img_land.setImageBitmap(thumbnail)

            path = saveImage(thumbnail)
            log("pathCamera", path)
            aho_app_txt_img_file_name.text = File(path).name
            aho_app_lay_file_name.visibility = View.VISIBLE
            Toast.makeText(activity, "Image Saved!", Toast.LENGTH_SHORT).show()
        }
    }

    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            "" + Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY
        )
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }

        try {
            var pathsFileName = "" + Calendar.getInstance().getTimeInMillis() + ".jpg"
            val f = File(wallpaperDirectory, pathsFileName)
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                activity,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            log("TAG", "File Saved::---&gt;" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }

    fun choosePhotoFromGallary() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }


    private fun validatePermissions(isShow: Int) {
        log(TAG, "" + isShow)
        Dexter.withActivity(activity)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (isShow == 1 && report?.areAllPermissionsGranted()!!) {
                        showImagePickDialog()
                    }

                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?, token: PermissionToken?
                ) {
                    log("permissions", "denied")
                    token?.continuePermissionRequest()
                }

            })
            .check()

    }

    private fun showPermissionDalog(token: PermissionToken?) {

        AlertDialog.Builder(activity)
            .setTitle(
                "storage_permission_rationale_title"
            )
            .setMessage(
                "storage_permition_rationale_message"
            )
            .setNegativeButton(
                android.R.string.cancel
            ) { dialog, _ ->
                dialog.dismiss()
                token?.cancelPermissionRequest()
            }
            .setPositiveButton(
                android.R.string.ok
            ) { dialog, _ ->
                dialog.dismiss()
                token?.continuePermissionRequest()
            }
            .setOnDismissListener {
                token?.cancelPermissionRequest()
            }
            .show()
    }

    fun dialogDismiss(dialog: Dialog) {
        if (dialog != null) {
            if (dialog.isShowing) {
                dialog.dismiss()
            }
        }

    }

    override fun OnclickPostionEvent(name: String, postion: String) {

    }

}