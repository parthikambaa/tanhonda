package com.tanhoda.aho.fragment.details.appdetails.Inspections.pojo

import com.google.gson.annotations.SerializedName

class AHOInspectWithPojo (
    @field:SerializedName("name")
    var name:String?=null,
    @field:SerializedName("designation")
    var designation:String?=null,
    @field:SerializedName("remarks")
    var remarks:String?=null
)