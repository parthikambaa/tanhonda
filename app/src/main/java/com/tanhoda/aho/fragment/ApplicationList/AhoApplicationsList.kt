package com.tanhoda.aho.fragment.ApplicationList


import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.google.gson.Gson
import com.tanhoda.R
import com.tanhoda.aho.AHOActivity
import com.tanhoda.aho.fragment.ApplicationList.adapter.AHOappListAdapter
import com.tanhoda.aho.fragment.ApplicationList.pojo.AHOApplicationRES
import com.tanhoda.aho.fragment.dashboard.AhoDashborad
import com.tanhoda.aho.fragment.details.AHODetailsHome
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.login_splash_screen.SplashActivity.Companion.snackBar
import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import com.tanhoda.utils.Utils.log
import kotlinx.android.synthetic.main.fragment_ahoapplications_list.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AhoApplicationsList : Fragment(), OnclickPostionEvent {


    companion object {
        var ahoappListRES: AHOApplicationRES? = null
        var ahocurrentclickposition:String?=null
        var  ahocurrentAPPID=""
    }

    var ahOappListAdapter: AHOappListAdapter? = null
    val TAG = "AHOAPPLICATIONLIST"
    var dialog: Dialog? = null
    var snackViewAHOUserList: RelativeLayout? = null
    var snackbar: Snackbar? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as AHOActivity).disableNavigationInAdoptionHome(getString(R.string.applicationlist))
        return inflater.inflate(R.layout.fragment_ahoapplications_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)
        snackViewAHOUserList = view.findViewById(R.id.rel_lay_aho_app_list_snackview)

//        FragmentCallUtils.passFragment(activity, AHODetailsHome(), R.id.aho_container_body)//Testing Purpose

        /*
         * get AHO Application List*/

        if (Utils.haveNetworkConnection(activity)) {
            getApplicationList()
        } else {
            aho_app_empty_list?.visibility =View.VISIBLE
            aho_app_empty_list?.text =getString(R.string.check_internet)
            snackBar=    MessageUtils.showSnackBarAction(activity, snackViewAHOUserList, getString(R.string.check_internet))
        }


    }

    private fun getApplicationList() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["userid"] = SessionManager.getUserId(activity)
            map["role"] =SessionManager.getuserSopeID(activity)
            map["status"] =AhoDashborad.currentstatusname
            map["block"] =SessionManager.getBlockId(activity)
            map["district"]=SessionManager.getdistrictID(activity)
            val callinterface = Utils.getInstance(activity)
            val callback = callinterface.call_post("applicationlist","Bearer "+SessionManager.getToken(activity), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t!!.message)
                    snackbar = MessageUtils.showSnackBar(activity, snackViewAHOUserList, msg)
                    aho_app_empty_list?.visibility = View.VISIBLE
                    aho_app_empty_list?.text =msg
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {

                            ahoappListRES = Gson().fromJson(response.body()?.string(), AHOApplicationRES::class.java)
                            if (ahoappListRES != null) {
                                if (ahoappListRES?.success!!) {
                                    if (ahoappListRES?.data != null) {
                                        log("AHOLIST",""+ahoappListRES?.data?.applications?.size)
                                        if (ahoappListRES?.data?.applications != null && ahoappListRES?.data?.applications?.size != 0) {
                                            setToADapter()
                                            aho_app_empty_list?.visibility = View.GONE
                                            recy_aho_app_list?.visibility = View.VISIBLE
                                            aho_app_list_title_linear?.visibility = View.VISIBLE
                                        } else {
                                            aho_app_list_title_linear?.visibility = View.GONE
                                            recy_aho_app_list?.visibility = View.GONE
                                            aho_app_empty_list?.visibility = View.VISIBLE
                                            aho_app_empty_list?.text = ahoappListRES?.message
                                        }

                                    } else {
                                        snackBar =    MessageUtils.showSnackBar(
                                            activity,
                                            snackViewAHOUserList,
                                            ahoappListRES?.message
                                        )
                                    }
                                } else {
                                    snackBar =  MessageUtils.showSnackBar(
                                        activity,
                                        snackViewAHOUserList,
                                        ahoappListRES?.message
                                    )
                                }
                            } else {
                                snackBar =  MessageUtils.showSnackBar(
                                    activity,
                                    snackViewAHOUserList,
                                    getString(R.string.checkjsonformat)
                                )
                            }

                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            aho_app_empty_list?.visibility = View.VISIBLE
                            aho_app_empty_list?.text =msg
                            snackBar = MessageUtils.showSnackBar(activity, snackViewAHOUserList, msg)
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }

                }

            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setToADapter() {
        try {
            recy_aho_app_list?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            ahOappListAdapter = AHOappListAdapter(activity!!, ahoappListRES?.data?.applications, this)
            recy_aho_app_list?.adapter = ahOappListAdapter
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {
        try {
            log(TAG, "position " + name)
            MessageUtils.dismissSnackBar(snackBar)
            ahocurrentclickposition=postion
            ahocurrentAPPID = ahoappListRES?.data?.applications!![postion.toInt()]?.id.toString()
            FragmentCallUtils.passFragment(activity, AHODetailsHome(), R.id.aho_container_body)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        MessageUtils.dismissSnackBar(snackBar)
    }
}

