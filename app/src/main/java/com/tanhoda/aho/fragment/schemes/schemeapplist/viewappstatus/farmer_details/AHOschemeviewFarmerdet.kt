package com.tanhoda.aho.fragment.schemes.schemeapplist.viewappstatus.farmer_details


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tanhoda.R
import com.tanhoda.aho.AHOActivity
import com.tanhoda.aho.fragment.schemes.schemeapplist.viewappstatus.AHOshemeAppViewStatusHome
import kotlinx.android.synthetic.main.fragment_ahoschemeview_farmerdet.*

class AHOschemeviewFarmerdet : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as AHOActivity).disableNavigationInAdoptionHome("Farmer Details")
        return inflater.inflate(R.layout.fragment_ahoschemeview_farmerdet, container, false)
    }

    override fun onResume() {
        super.onResume()
        setDataToform()
    }

    private fun setDataToform() {
        try{

            val data =AHOshemeAppViewStatusHome?.ahoviewschemeRES?.data?.applications?.appfarmer!!

            if(data.name!=null){
                custxtviw_aho_scheme_far_name.setText(data.name)
            }
            if(data.gname!=null){
                custxtviw_aho_scheme_far_farthername.setText(data.gname)
            }
            if(data.farmerType!=null){
                custxtviw_aho_scheme_far_fartype.setText(data.farmerType)
            }
            if(data.socialStatus!=null){
                custxtviw_aho_scheme_far_social.setText(data.socialStatus)
            }
            if(data.houseNo!=null &&  data.street!=null && data.habitation!=null && data.pincode!=null){
                custxtviw_aho_scheme_far_address.setText(data.houseNo+"\n"+data.street+"\n"+data.habitation+"\n"+data.pincode)
            }
            if(data.mobileNumber!=null){
                custxtviw_aho_scheme_far_mobile.setText(data.mobileNumber)
            }
            if(data.gender!=null){
                custxtviw_aho_scheme_far_gender.setText(data.gender)
            }
            if(data.age!=null){
                custxtviw_aho_scheme_far_age.setText(data.age)
            }
            if(data.income!=null){
                custxtviw_aho_scheme_far_income.setText(data.income)
            }
            if(data.aadhaarId!=null){
                custxtviw_aho_scheme_far_aadhar_no.setText(data.aadhaarId)
            }
            if(data.bankAccountNo!=null){
                custxtviw_aho_scheme_far_account_no.setText(data.bankAccountNo)
            }
            if(data.ifscCode!=null){
                custxtviw_aho_scheme_far_ifsc_code.setText(data.ifscCode)
            }
            if(data.bankName!=null){
                custxtviw_aho_scheme_far_bankname.setText(data.bankName)
            }


        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

}
