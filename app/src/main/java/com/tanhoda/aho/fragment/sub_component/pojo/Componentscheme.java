
package com.tanhoda.aho.fragment.sub_component.pojo;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Componentscheme {

    @SerializedName("active")
    private Long mActive;
    @SerializedName("available_in")
    private String mAvailableIn;
    @SerializedName("create_date")
    private String mCreateDate;
    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("id")
    private Long mId;
    @SerializedName("scheme_image")
    private String mSchemeImage;
    @SerializedName("scheme_name")
    private String mSchemeName;
    @SerializedName("scheme_type")
    private String mSchemeType;
    @SerializedName("short_description")
    private String mShortDescription;
    @SerializedName("short_name")
    private String mShortName;
    @SerializedName("updated_at")
    private String mUpdatedAt;

    public Long getActive() {
        return mActive;
    }

    public void setActive(Long active) {
        mActive = active;
    }

    public String getAvailableIn() {
        return mAvailableIn;
    }

    public void setAvailableIn(String availableIn) {
        mAvailableIn = availableIn;
    }

    public String getCreateDate() {
        return mCreateDate;
    }

    public void setCreateDate(String createDate) {
        mCreateDate = createDate;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getSchemeImage() {
        return mSchemeImage;
    }

    public void setSchemeImage(String schemeImage) {
        mSchemeImage = schemeImage;
    }

    public String getSchemeName() {
        return mSchemeName;
    }

    public void setSchemeName(String schemeName) {
        mSchemeName = schemeName;
    }

    public String getSchemeType() {
        return mSchemeType;
    }

    public void setSchemeType(String schemeType) {
        mSchemeType = schemeType;
    }

    public String getShortDescription() {
        return mShortDescription;
    }

    public void setShortDescription(String shortDescription) {
        mShortDescription = shortDescription;
    }

    public String getShortName() {
        return mShortName;
    }

    public void setShortName(String shortName) {
        mShortName = shortName;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

}
