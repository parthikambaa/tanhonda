package com.tanhoda.aho.fragment.schemes.schemeapplist.viewappstatus.app_details


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tanhoda.R
import com.tanhoda.aho.AHOActivity
import com.tanhoda.aho.fragment.schemes.schemeapplist.AHOSchemeAppList
import com.tanhoda.aho.fragment.schemes.schemeapplist.viewappstatus.AHOshemeAppViewStatusHome
import com.tanhoda.aho.fragment.schemes.schemeapplist.viewappstatus.viewschemePojo.AHOViewSchemeRES
import kotlinx.android.synthetic.main.fragment_ahoapp_details.*
import kotlinx.android.synthetic.main.fragment_ahoschemeview_appdet.*


class AHOschemeviewAppdet : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as AHOActivity).disableNavigationInAdoptionHome("application Details")
        return inflater.inflate(R.layout.fragment_ahoschemeview_appdet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setdatatoform()
    }

    private fun setdatatoform() {
        try{
            val data  =AHOshemeAppViewStatusHome.ahoviewschemeRES?.data?.applications!!
            custxtviw_aho_scheme_app_det_scheme?.setText(data.appscheme?.schemeName!!)
            custxtviw_aho_scheme_app_det_category?.setText(data.appcategory?.category!!)
            custxtviw_aho_scheme_app_det_component?.setText(data.appcomponent?.componentName!!)
            if (data.applicationId != null) {
                custxtviw_aho_scheme_app_det_app_id?.setText(data.applicationId.toString())
            }

            if (data.applicationStatus != null) {
                custxtviw_aho_scheme_app_det_app_status?.setText(data.applicationStatus)
            }
            if (data.waitingFor != null) {
                custxtviw_aho_scheme_app_det_waitingfor?.setText(data.waitingFor)
            }

            /**
             * Proposal Details
             */

            if (data.areaProposed != null) {
                custxtviw_aho_scheme_app_det_area_proposed?.setText(data.areaProposed.toString())
            }
            if (data.proposedCrop != null) {
                custxtviw_aho_scheme_app_det_proposed_crop?.setText(data.proposedCrop.toString())
            }
            if (data.estimateCost != null) {
                custxtviw_aho_scheme_app_det_estimated_amt?.setText(data.estimateCost.toString())
            }
            if (data.govtSubsidy != null) {
                custxtviw_aho_scheme_app_det_whetherany?.setText(data.govtSubsidy.toString())
            }
            if (data.relevant != null) {
                custxtviw_aho_scheme_app_det_remarks?.setText(data.relevant)
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }


}
