package com.tanhoda.aho.fragment.dashboard.pojo

import com.google.gson.annotations.SerializedName

class AHOappStarusPojo(
    @field:SerializedName("image")
    var image:String,
    @field:SerializedName("status_name")
    var statusName:String,
    @field:SerializedName("status_no")
    var status_no:String

)