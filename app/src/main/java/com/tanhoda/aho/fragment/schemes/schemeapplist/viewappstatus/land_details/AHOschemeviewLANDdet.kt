package com.tanhoda.aho.fragment.schemes.schemeapplist.viewappstatus.land_details


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.tanhoda.R
import com.tanhoda.aho.AHOActivity


class AHOschemeviewLANDdet : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as AHOActivity).disableNavigationInAdoptionHome("Land Details")
        return inflater.inflate(R.layout.fragment_ahoschemeview_landdet, container, false)
    }


}
