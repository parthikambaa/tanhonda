package com.tanhoda.aho.fragment.details.appdetails.approve.pojo

import com.google.gson.annotations.SerializedName

class AHOAppDetDocumentServerPojo(
    @field:SerializedName("document_value")
    var document_value:String?=null,
    @field:SerializedName("document_name")
    var document_name:String?=null
)