package com.tanhoda.aho.fragment.schemes.schemeapplist.viewappstatus.viewschemePojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("documents")
	val documents: ArrayList<Any?>? = null,

	@field:SerializedName("statusupdates")
	val statusupdates: ArrayList<Any?>? = null,

	@field:SerializedName("inspectionimages")
	val inspectionimages: ArrayList<Any?>? = null,

	@field:SerializedName("approval")
	val approval: Any? = null,

	@field:SerializedName("inspectionimage")
	val inspectionimage: Int? = null,

	@field:SerializedName("status_name")
	val statusName: String? = null,

	@field:SerializedName("approvals")
	val approvals: ArrayList<Any?>? = null,

	@field:SerializedName("count")
	val count: Int? = null,

	@field:SerializedName("checkstatus")
	val checkstatus: Int? = null,

	@field:SerializedName("approvalid")
	val approvalid: Approvalid? = null,

	@field:SerializedName("applications")
	val applications: Applications? = null
)