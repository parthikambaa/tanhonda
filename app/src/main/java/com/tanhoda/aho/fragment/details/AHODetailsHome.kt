package com.tanhoda.aho.fragment.details


import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.os.StrictMode
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.tanhoda.R
import com.tanhoda.aho.AHOActivity
import com.tanhoda.aho.fragment.ApplicationList.AhoApplicationsList
import com.tanhoda.aho.fragment.details.appdetails.AHOAppDetails
import com.tanhoda.aho.fragment.details.farmar_details.AHOFarmerDetails
import com.tanhoda.aho.fragment.details.land_details.AHOLandDetails
import com.tanhoda.aho.fragment.details.tapadaptr.AhoTapadapter
import com.tanhoda.aho.fragment.schemes.schemeapplist.AHOSchemeAppList
import com.tanhoda.ho.fragment.details.land_details.land_view_documents.HoLandViewDocuments
import com.tanhoda.ho.fragment.details.land_details.land_view_status.HOAppLandViewStatus
import com.tanhoda.ho.fragment.details.viewappstatusPojo.HOappviewRES
import com.tanhoda.ho.fragment.details.viewappstatusPojo.StatusupdatesItem
import com.tanhoda.login_splash_screen.SplashActivity
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import customs.CustomTextView
import kotlinx.android.synthetic.main.dialog_view_status_document.*
import kotlinx.android.synthetic.main.fragment_ahodetails_home.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AHODetailsHome : Fragment() {

    var ahotapArrayList: ArrayList<String>? = null
    var ahoTapadapter: AhoTapadapter? = null
    var typeface: Typeface? = null
    var txt: CustomTextView? = null
    var txt1: CustomTextView? = null
    var txt2: CustomTextView? = null
    var dialog: Dialog? = null

    companion object {
        var ahoappdetRES: HOappviewRES? = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ahodetails_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())
        dialog = Dialog(activity)
        txt = CustomTextView(activity, null)
        txt1 = CustomTextView(activity, null)
        txt2 = CustomTextView(activity, null)

        ahotapArrayList = ArrayList()
        ahotapArrayList?.add(getString(R.string.app_details))
        ahotapArrayList?.add(getString(R.string.land_details))
        ahotapArrayList?.add(getString(R.string.farmer_details))

        aho_app_det_viewpager?.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(aho_app_det_taplay))
        aho_app_det_taplay?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                try {
                    when (tab?.position) {
                        0 -> (activity as AHOActivity).disableNavigationInAdoptionHome(getString(R.string.app_details))
                        1 -> (activity as AHOActivity).disableNavigationInAdoptionHome(getString(R.string.land_details))
                        2 -> (activity as AHOActivity).disableNavigationInAdoptionHome(getString(R.string.farmer_details))
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        })



        when {
            Utils.haveNetworkConnection(activity) -> {
                getViewApplication()
            }
            else -> {
                aho_details_home_empty_List?.visibility = View.VISIBLE
                aho_details_home_empty_List?.text = getString(R.string.check_internet)
                SplashActivity.snackBar = MessageUtils.showSnackBarAction(
                    activity,
                    aho_app_det_view_snack_view,
                    getString(R.string.check_internet)
                )
            }
        }


        /*  if (ahoappdetRES?.data?.statusupdates?.size != 0) {
              aho_app_land_view_status?.visibility = View.VISIBLE
          } else {
              aho_app_land_view_status?.visibility = View.GONE
          }

          if (ahoappdetRES?.data?.documents?.size != 0) {
              aho_app_land_view_documents?.visibility = View.VISIBLE
          } else {
              aho_app_land_view_documents?.visibility = View.GONE
          }*/

        /**
         * set on click Listester
         */
        status_category.visibility = View.GONE

        view_status_fab_aho?.setOnClickListener {
            if (status_category?.visibility == View.GONE) {
                status_category?.visibility = View.VISIBLE
            } else {
                status_category?.visibility = View.GONE
            }
        }
        status_category?.setOnClickListener {
            status_category?.visibility = View.GONE
        }

        ho_land_view_status_btn?.setOnClickListener {

            try {
                if (ahoappdetRES?.data?.statusupdates?.size != 0) {
                    // FragmentCallUtils.passFragmentWithoutAnim(activity, AHOappLandViewStatus(), R.id.aho_container_body)
                    status_category?.visibility = View.GONE
                    val intent = Intent(this.context, HOAppLandViewStatus::class.java);
                    val bundle = Bundle()
                    bundle.putSerializable("details", ahoappdetRES)
                    intent.putExtras(bundle)

                    startActivity(intent)
                } else {
                    SplashActivity.snackBar =
                        MessageUtils.showSnackBar(activity, snack_view, getString(R.string.no_data_found))
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }
        ho_land_view_document_btn?.setOnClickListener {
            try {
                if (ahoappdetRES?.data?.documents?.size != 0) {
                    /* FragmentCallUtils.passFragmentWithoutAnim(
                         activity,
                         AHOAppLandViewDocuments(),
                         R.id.aho_container_body
                     )*/
                    status_category?.visibility = View.GONE
                    val intent = Intent(this.context, HoLandViewDocuments::class.java)
                    val bundle = Bundle()
                    bundle.putSerializable("details", ahoappdetRES)
                    intent.putExtras(bundle)
                    startActivity(intent)

                } else {
                    SplashActivity.snackBar =
                        MessageUtils.showSnackBar(activity, snack_view, getString(R.string.no_data_found))
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

        ho_land_view_pdf_btn.setOnClickListener {
            try {

                if (ahoappdetRES != null) {
                    status_category?.visibility = View.GONE
                    Utils.callWebURL(activity, Utils.PDF_LOADER + (ahoappdetRES?.data?.applications?.id))
                } else {
                    SplashActivity.snackBar =
                        MessageUtils.showSnackBar(activity, snack_view, getString(R.string.no_data_found))
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    private fun getViewApplication() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["user_id"] = SessionManager.getUserId(activity)
            if (!AHOSchemeAppList.ahoschemecurentAPP_ID.isEmpty()) {
                map["application_id"] = AHOSchemeAppList.ahoschemecurentAPP_ID
            } else {
                map["application_id"] =
                    AhoApplicationsList.ahoappListRES?.data?.applications!![AhoApplicationsList.ahocurrentclickposition!!.toInt()]?.id!!
            }

            map["user_scope_id"] = SessionManager.getuserSopeID(activity)
            val service = Utils.getInstance(activity)
            val callback = service.call_post("viewapplication", "Bearer " + SessionManager.getToken(activity), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                    MessageUtils.dismissDialog(dialog)

                    try {
                        if (response.isSuccessful) {
                            ahoappdetRES = Gson().fromJson(response.body()?.string(), HOappviewRES::class.java)
                            if (ahoappdetRES != null) {
                                if (ahoappdetRES?.success!!) {
                                    if (ahoappdetRES?.data != null) {

                                        /*         if(ahoappdetRES?.data?.statusupdates?.size!=0){
                                                     aho_app_land_view_status?.visibility =View.VISIBLE
                                                 }else{
                                                     aho_app_land_view_status?.visibility =View.GONE
                                                 }

                                                 if(ahoappdetRES?.data?.documents?.size!=0){
                                                     aho_app_land_view_documents?.visibility =View.VISIBLE
                                                 }else{
                                                     aho_app_land_view_documents?.visibility =View.GONE
                                                 }
         */

                                        val item = StatusupdatesItem(
                                            1, 1, "", "New Application", "", 0,
                                            ahoappdetRES?.data?.applications?.createdAt, 0, null,
                                            "", "Approved"
                                        )
                                        ahoappdetRES?.data?.statusupdates?.add(0, item)
                                        setupViewPager()

                                        /* if (ahoappdetRES?.data?.statusupdates?.size != 0 || ahoappdetRES?.data?.documents?.size != 0) {
                                             status_category.visibility = View.VISIBLE
                                         } else {
                                             status_category.visibility = View.GONE
                                         }*/
                                    } else {
                                        aho_details_home_empty_List?.visibility = View.VISIBLE
                                        SplashActivity.snackBar = MessageUtils.showSnackBar(
                                            activity,
                                            aho_app_det_view_snack_view,
                                            ahoappdetRES?.message
                                        )
                                    }
                                } else {
                                    SplashActivity.snackBar = MessageUtils.showSnackBar(
                                        activity,
                                        aho_app_det_view_snack_view,
                                        ahoappdetRES?.message
                                    )
                                }

                            } else {
                                SplashActivity.snackBar = MessageUtils.showSnackBar(
                                    activity,
                                    aho_app_det_view_snack_view,
                                    getString(R.string.checkjsonformat)
                                )
                            }


                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            aho_details_home_empty_List?.visibility = View.VISIBLE
                            aho_details_home_empty_List?.text = msg
                            SplashActivity.snackBar =
                                MessageUtils.showSnackBar(activity, aho_app_det_view_snack_view, msg)

                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }

                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)

                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    SplashActivity.snackBar = MessageUtils.showSnackBar(activity, aho_app_det_view_snack_view, msg)
                    aho_details_home_empty_List?.visibility = View.VISIBLE
                    aho_details_home_empty_List?.text = msg

                }

            })
        } catch (ex: Exception) {
            ex.printStackTrace()
            MessageUtils.dismissDialog(dialog)
        }
    }

    private fun setupViewPager() {
        try {
            ahoTapadapter = AhoTapadapter(childFragmentManager)
            ahoTapadapter!!.addFragment(AHOAppDetails(), ahotapArrayList?.get(0)!!)
            ahoTapadapter!!.addFragment(AHOLandDetails(), ahotapArrayList?.get(1)!!)
            ahoTapadapter!!.addFragment(AHOFarmerDetails(), ahotapArrayList?.get(2)!!)
            aho_app_det_viewpager?.adapter = ahoTapadapter
            aho_app_det_taplay?.setupWithViewPager(aho_app_det_viewpager)
            initTabs()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        MessageUtils.dismissSnackBar(SplashActivity.snackBar)
    }

    @SuppressLint("ResourceType")
    private fun initTabs() {
        try {

            txt?.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            txt?.gravity = Gravity.CENTER
            txt?.setText(ahotapArrayList?.get(0))
            val tab = aho_app_det_taplay?.getTabAt(0)
            if (tab != null) {
                tab.setCustomView(txt)
                tab.setText(ahoTapadapter!!.getPageTitle(0))
            }
            txt1?.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            txt1?.gravity = Gravity.CENTER
            txt1?.setText(ahotapArrayList?.get(1))
            val tab1 = aho_app_det_taplay?.getTabAt(1)
            if (tab1 != null) {
                tab1.setCustomView(txt1)
                tab1.setText(ahoTapadapter!!.getPageTitle(1))
            }

            txt2?.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            txt2?.gravity = Gravity.CENTER
            txt2?.setText(ahotapArrayList?.get(2))
            val tab2 = aho_app_det_taplay?.getTabAt(2)
            if (tab2 != null) {
                tab2.setCustomView(txt2)
                tab2.setText(ahoTapadapter!!.getPageTitle(2))
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


}
