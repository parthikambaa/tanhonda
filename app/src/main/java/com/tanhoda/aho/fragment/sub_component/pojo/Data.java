
package com.tanhoda.aho.fragment.sub_component.pojo;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("subcomponent")
    private ArrayList<Subcomponent> mSubcomponent;

    public ArrayList<Subcomponent> getSubcomponent() {
        return mSubcomponent;
    }

    public void setSubcomponent(ArrayList<Subcomponent> subcomponent) {
        mSubcomponent = subcomponent;
    }

}
