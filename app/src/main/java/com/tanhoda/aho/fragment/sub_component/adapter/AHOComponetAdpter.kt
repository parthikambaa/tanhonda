package com.tanhoda.aho.fragment.sub_component.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.tanhoda.R
import com.tanhoda.aho.fragment.sub_component.pojo.Subcomponent
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.utils.ImageUtils
import com.tanhoda.utils.Utils
import customs.CustomTextView

class AHOComponetAdpter(
    val context: Context,
    val componetnArrayList: MutableList<Subcomponent>,
    val onclickPostionEvent: OnclickPostionEvent
) : RecyclerView.Adapter<AHOComponetAdpter.Viewholader>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholader {
        return Viewholader(LayoutInflater.from(context).inflate(R.layout.adapter_aho_component_list, parent, false))
    }

    override fun getItemCount(): Int {
        return componetnArrayList.size!!
    }

    override fun onBindViewHolder(holder: Viewholader, position: Int) {

        try {
            if (componetnArrayList[position].maxLimit != null) {
                holder.max_limit_txt!!.text = componetnArrayList[position]?.maxLimit + " "
                holder.max_limit_txt.visibility = View.VISIBLE
            } else {
                holder.max_limit_txt!!.visibility = View.GONE
            }
            if (componetnArrayList[position].minLimit != null) {
                holder.min_limit_txt!!.visibility = View.VISIBLE
                holder.min_limit_txt.text = componetnArrayList[position]?.minLimit + " "
            } else {
                holder.min_limit_txt!!.visibility = View.GONE
            }

            ImageUtils.displayImageFromUrl(
                context,
                Utils.IMG_COMPONENT_URL + componetnArrayList[position]?.image,
                holder.image
            )
            holder.component?.text = componetnArrayList[position].componentscheme?.schemeName
            holder.siNo?.text = (position + 1).toString()
            holder.subsidyamt?.text = componetnArrayList[position].componentName
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    class Viewholader(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val siNo = itemView?.findViewById<CustomTextView>(R.id.txt_sino_aho_component_app_list_adp)
        val component = itemView?.findViewById<CustomTextView>(R.id.txt_component_aho_component_app_list_adp)
        val subsidyamt = itemView?.findViewById<CustomTextView>(R.id.txt_subsidy_aho_component_app_list_adp)
        val image = itemView?.findViewById<ImageView>(R.id.txt_availble_aho_component_app_list_adp)
        val linear = itemView?.findViewById<LinearLayout>(R.id.aho_component_adap_linear)
        val max_limit_txt = itemView?.findViewById<CustomTextView>(R.id.max_limit_txt)
        val min_limit_txt = itemView?.findViewById<CustomTextView>(R.id.min_limit_txt)
    }

}