package com.tanhoda.aho.fragment.details.appdetails.approve.pojo

import com.google.gson.annotations.SerializedName

class AHOAcompayingofficedynamic(
    @field:SerializedName("officer_name")
    var officerName:String?=null,
    @field:SerializedName("department")
    var department:String?=null,
    @field:SerializedName("designation")
    var designation:String?=null,
    @field:SerializedName("remarks")
    var remarks:String?=null
)