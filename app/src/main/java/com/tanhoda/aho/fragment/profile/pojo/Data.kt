package com.tanhoda.aho.fragment.profile.pojo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("users")
	val users: Users? = null
)