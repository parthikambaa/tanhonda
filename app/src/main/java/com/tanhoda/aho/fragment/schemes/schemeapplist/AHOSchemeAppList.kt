package com.tanhoda.aho.fragment.schemes.schemeapplist


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.gson.Gson
import com.tanhoda.R
import com.tanhoda.aho.AHOActivity
import com.tanhoda.aho.fragment.details.AHODetailsHome
import com.tanhoda.aho.fragment.schemes.AHOSchemes
import com.tanhoda.aho.fragment.schemes.schemeapplist.adapter.AHoschemeApplistAdapter
import com.tanhoda.aho.fragment.schemes.schemeapplist.pojo.AHOschemeApplistRES
import com.tanhoda.interfaces.OnclickPostionEvent
import com.tanhoda.login_splash_screen.SplashActivity
import com.tanhoda.utils.FragmentCallUtils
import com.tanhoda.utils.MessageUtils
import com.tanhoda.utils.SessionManager
import com.tanhoda.utils.Utils
import kotlinx.android.synthetic.main.fragment_ahoscheme_app_list.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AHOSchemeAppList : Fragment(), OnclickPostionEvent {

    var dialog: Dialog? = null
    var schemeAppRES: AHOschemeApplistRES? = null

    companion object {
        var ahoschemecurentAPP_ID = ""
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as AHOActivity).disableNavigationInAdoptionHome(getString(R.string.scheme_app_List))
        return inflater.inflate(R.layout.fragment_ahoscheme_app_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)

        /**
         * getting the stages AppList
         */

        when {
            Utils.haveNetworkConnection(activity) -> {
                getttingschemApplist()
            }
            else -> {
                aho_schemeapp_empty_list?.visibility = View.VISIBLE
                aho_schemeapp_empty_list?.text = getString(R.string.check_internet)
                MessageUtils.showSnackBarAction(
                    activity,
                    aho_schemeapp_list_snack_view,
                    getString(R.string.check_internet)
                )

            }
        }

    }

    private fun getttingschemApplist() {
        try {
            dialog = MessageUtils.showDialog(activity)
            val map = HashMap<String, Any>()
            map["user_id"] = SessionManager.getUserId(activity)
//            map["user_id"] = 8
            map["scheme_id"] = AHOSchemes.ahoschemeid
            val service = Utils.getInstance(activity)
            val callback =
                service.call_post("schemeapplicationlist", "Bearer " + SessionManager.getToken(activity), map)
            callback.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    val msg = MessageUtils.setFailurerMessage(activity, t.message)
                    MessageUtils.showSnackBar(activity, aho_schemeapp_list_snack_view, msg)
                    aho_schemeapp_empty_list?.visibility = View.VISIBLE
                    aho_schemeapp_empty_list?.text = msg
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try {
                        if (response.isSuccessful) {
                            schemeAppRES = Gson().fromJson(response.body()?.string(), AHOschemeApplistRES::class.java)
                            if (schemeAppRES != null) {
                                if (schemeAppRES?.success!!) {
                                    if (schemeAppRES?.data != null && schemeAppRES?.data?.applications?.size != 0) {
                                        setToadpter()
                                        recy_aho_schemeapp_list?.visibility = View.VISIBLE
                                        aho_schemeapp_empty_list?.visibility = View.GONE
                                        //aho_schemeapp_list_title_linear?.visibility = View.GONE
                                    } else {
                                        //aho_schemeapp_list_title_linear?.visibility = View.GONE
                                        recy_aho_schemeapp_list?.visibility = View.GONE
                                        aho_schemeapp_empty_list?.text = schemeAppRES?.message
                                        aho_schemeapp_empty_list?.visibility = View.VISIBLE
                                    }
                                } else {
                                    MessageUtils.showSnackBar(
                                        activity,
                                        aho_schemeapp_list_snack_view,
                                        schemeAppRES?.message
                                    )
                                }

                            } else {
                                MessageUtils.showSnackBar(
                                    activity,
                                    aho_schemeapp_list_snack_view,
                                    getString(R.string.checkjsonformat)
                                )
                            }

                        } else {
                            val msg = MessageUtils.setErrorMessage(response.code())
                            aho_schemeapp_empty_list?.visibility = View.VISIBLE
                            aho_schemeapp_empty_list?.text = msg
                            MessageUtils.showSnackBar(activity, aho_schemeapp_list_snack_view, msg)
                        }

                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            })

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun setToadpter() {
        try {
            val schemAppListADpter = AHoschemeApplistAdapter(activity!!, schemeAppRES?.data?.applications, this)
            recy_aho_schemeapp_list?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            recy_aho_schemeapp_list?.adapter = schemAppListADpter

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun OnclickPostionEvent(name: String, postion: String) {
        try {
            ahoschemecurentAPP_ID = schemeAppRES?.data?.applications!![postion.toInt()]?.id.toString()
            FragmentCallUtils.passFragmentWithoutAnim(activity, AHODetailsHome(), R.id.aho_container_body)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        MessageUtils.dismissSnackBar(SplashActivity.snackBar)
    }

}
